import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { CommonService, GlobalDataService } from '@app/_services';
import { debounceTime, map, distinctUntilChanged, filter } from "rxjs/operators";
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription, fromEvent } from 'rxjs';
import { ActivityService } from '@app/_services/activity.service';
import { TranslateService } from '@ngx-translate/core';

@Component({ 
    selector: 'activities-page',
    templateUrl: 'activities.page.html' 
})
export class ActivitiesPage implements OnInit, OnDestroy {
    @ViewChild('activitiesFilterSearchTerm', {static: false, read: ElementRef}) activitiesFilterSearchTerm: ElementRef;  
    private subscription: Subscription;
    private navigationSubscription: Subscription;  
    public isSearching: boolean = false;
    public selected_filter = {
        date_mode: 0,
        assigned_to: '',
        searchTerm: ''
      };  

    public view_mode: string = 'page';
    public def_take: number = 15;
    public def_skip: number = 0;
    public def_sort = [];
    public search_placeholder: string = 'Key | Summary | Description';

    public grid_params = {
      def_take: this.def_take,
      def_skip: this.def_skip,
      def_sort: this.def_sort
    }
    public status_list = [];
    public done_status: number = 3;
    public assigned_list = [];
    public loaded: boolean = false;
    

    constructor(
        private activityService: ActivityService,
        public globalDataService: GlobalDataService,
        private commonService: CommonService,
        private router: Router, private actRoute: ActivatedRoute, private location: Location,
        private translateService:TranslateService
    ) {
      this.translateService.get(['ACTIVITY.SEARCH_PLACEHOLDER']).subscribe((data:any)=> {
        this.search_placeholder = data['ACTIVITY.SEARCH_PLACEHOLDER'];
      });      
    }

    ngAfterViewInit() {    
      this.subscription = fromEvent(this.activitiesFilterSearchTerm.nativeElement, 'keyup').pipe(
          // get value
          map((event: any) => {
          return event.target.value;
          })
          // if character length greater then 2
          ,filter(res => res.length >= 0)
          // Time in milliseconds between key events
          ,debounceTime(450)        
          // If previous query is diffent from current   
          ,distinctUntilChanged()
          // subscription for response
          ).subscribe((text: string) => {
          this.isSearching = true;
          this.selected_filter.searchTerm = text;
          this.changeFilter(true);
          });    
  }    

    ngOnInit() {
        this.navigationSubscription = this.actRoute.params.subscribe(params => {

          this.activityService.tableSetting()
          .subscribe(
              results => {
                this.status_list = results['status_list'];
                this.done_status = results['done_status'];
                this.assigned_list = results['assigned_list'];
                
                this.loaded = true;

                for (let key in this.selected_filter) {               
  
                  if (params.hasOwnProperty(key) && params[key]) {
                    let val = params[key];
                    if(key == 'source_id') {
                      val = parseInt(params[key]);
                    }
                    this.selected_filter[key] = val;
                  }
                }  
                
                for (let key in this.grid_params) {               
  
                  if (params.hasOwnProperty(key) && params[key]) {
                    let val = parseInt(params[key]);
                    
                    this.grid_params[key] = val;
                    this[key] = val
                  }
                }
                
                if (params.hasOwnProperty('dir') && (params['dir'] == 'asc' || params['dir'] == 'desc') && params.hasOwnProperty('field') && params['field']) {
                  //    this.grid_params.def_sort.push({dir: params['dir'], field: params['field']});
                      this.def_sort.push({dir: params['dir'], field: params['field']});
                }                  
              });                

          });        

    }

    ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }

    public setActivityDate(date_mode) {
        this.selected_filter.date_mode = date_mode;
        this.changeFilter();
    }

    public changeAssigned(event) {
      this.selected_filter.assigned_to = event;
      this.changeFilter();      
    }

    changeFilter(def_state?) {
        if(this.view_mode == 'page') {
          if(def_state) { 
            this.grid_params.def_skip = 0;
          }
          let params = {}    
          for (let key in this.selected_filter) {
            let current_param = this.selected_filter[key];
            if(current_param) {
              params[key] = current_param;
            }
          }

          for (let key in this.grid_params) {
            let current_param = this.grid_params[key];
            if(current_param) {
              if(key == 'def_sort') {
                if(current_param.length) {
                    params['dir'] = current_param[0].dir;
                    params['field'] = current_param[0].field;
                }
              } else {              
                params[key] = current_param;
              }
            }
          }        

          let new_url = this.router.serializeUrl(this.router.createUrlTree(['/activities', params]));          
          this.location.replaceState(new_url);    
        }
      }  

    onButtonActivityClick() {
        let setting = {
            from_module: 'page',
            from_mode: 'page'
        };
        this.commonService.notifyOther({action: 'newActivityDialog', afterSubmitAction: 'loadActivities', def_type: '1', setting: setting});
    }

    stateGrid(event) {
        // console.log(event);
        this.grid_params.def_skip = event.skip;
        this.grid_params.def_take = event.take;

        if(event.sort && event.sort[0] && event.sort[0].dir) {
          this.grid_params.def_sort = [];
          this.grid_params.def_sort.push(event.sort[0]);
        } else {
            this.grid_params.def_sort = [];
        } 
        this.changeFilter(false);
      }      


}