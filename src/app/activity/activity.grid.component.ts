import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { CategoriesService } from '@app/_services/northwind.service';
import { CommonService } from '@app/_services/common.service';
import { ActivityService } from '@app/_services/activity.service';
import { State } from '@progress/kendo-data-query';
import { GlobalDataService } from '@app/_services/globaldata.service';
import {
    GridDataResult,
    RowClassArgs,
    DataStateChangeEvent,
    GridComponent
} from '@progress/kendo-angular-grid';


@Component({ 
    selector: 'activity-grid',
    templateUrl: 'activity.grid.component.html',
    providers: [CategoriesService]
})

export class ActivityGridComponent implements OnInit, OnDestroy, OnChanges {
    @ViewChild('activityGridInstance', {static: false}) activityGridInstance: GridComponent;    
    private subscription: Subscription;
    @Input() mode: string = "page";
    @Input() date_mode: number = 0;
    @Input() release_id: number = 0;
    @Input() types = "";
    @Input() def_take: number = 15;
    @Input() def_skip: number = 0;      
    @Input() def_sort = []; 
    @Input() status_list = [];    
    @Input() searchTerm: string = ''; 
    @Input() done_status: number = 3;
    @Input() assigned_to: string = '';    
    @Input() statusReadonly: boolean = false; 
    @Input() columns: string[] = [];
    @Output()emitState: EventEmitter<any> = new EventEmitter<any>();
    loadingIndicator = true;
    private controller: string = 'activity/table';

    public take_list: number[] = [5, 10, 15, 25, 50, 100, 150]; 
    
    public filters = {};
    public view: Observable<GridDataResult>;

    public state: State = {
        skip: 0,
        take: this.def_take,
        sort: this.def_sort
    };

    constructor(private globalDataService: GlobalDataService, private activityService: ActivityService, private commonService: CommonService, private service: CategoriesService) {
       
    }

    rowCallback(context: RowClassArgs) {
        //const isDone = context.dataItem.is_done;
        const isWarn = context.dataItem.warn;
        //const isSelected = context.dataItem.id == this.selectedRowId
        return {
            //activity_done: isDone,
            //activity_not_done: !isDone,
            warn_row: isWarn
            //,selected_activity_row: isSelected
          };
        }    

    ngOnInit() {       
        
        if(this.def_take > 150 ) {
            this.def_take = 50;
        }
        this.state = {
            skip: this.def_skip,
            take: this.def_take,
            sort: this.def_sort
        }         

        this.loadActivities();

        this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('action')) {
              if(res.action == 'loadActivities') {
                this.loadActivities();
              } else if(res.action == 'loadTasks') {
                if(this.types=='1') {
                    this.loadActivities();
                }
              } else if(res.action == 'loadCallsAndMeetings') {
                if(this.types=='2,3') {
                    this.loadActivities();
                }
              }
            }
          });        
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnChanges(changes: SimpleChanges) {
        if((changes["types"] && !changes["types"].isFirstChange()) ||
            (changes["date_mode"] && !changes["date_mode"].isFirstChange()) ||
            (changes["searchTerm"] && !changes["searchTerm"].isFirstChange()) ||
            (changes["assigned_to"] && !changes["assigned_to"].isFirstChange())
            
        )
        {
            this.state.skip = 0;
            this.loadActivities();
        }        

    }    


    selectActivityStatus(event, act_id: number): void {
        /*console.log(event);
        console.log(act_id);*/
        const params = {
            status_id: event
        };

        const setting = {
            from_mode: this.mode,
            from_module: 'grid'
        };

        if(params && act_id) {
            this.activityService.autoSave(act_id, params, setting)
                  .subscribe(
                      results => {     
                        if(this.mode == 'widget' && event == this.done_status) {                    
                            const obj = this.activityGridInstance.data['data'].find(
                                row => row.id === act_id);
                        
                            if(obj){
                                obj['status_id'] = event;
                            }
                        }
                        
                        this.loadActivities();             
                      }
                  );
        }        
    }

    onCellClick(event): void {
        if(((event.columnIndex == 0 || event.columnIndex == 1 || event.columnIndex == 2) && this.mode == 'widget') ||
            (this.mode == 'page' && (event.columnIndex >= 0 && event.columnIndex <= 6))
        ) {
            const selectedRowId = event.dataItem.id;
            const current_act_after = this.globalDataService.getActivityAfterSubmitAction(this.types);
            const setting = {
                from_module: 'grid',
                from_mode: this.mode
            };

            this.commonService.notifyOther({action: 'editActivityDialog', afterSubmitAction: current_act_after, act_id: selectedRowId, setting: setting});
        }
      }

    private loadActivities(): void {
        this.filters['types'] = this.types;
        this.filters['mode'] = this.mode;    
        this.filters['date_mode'] = this.date_mode;   
        this.filters['assigned_to'] = this.assigned_to;
        this.filters['search_term'] = this.searchTerm;

        if(this.release_id) {
            this.filters['release_id'] = this.release_id;
        }
        
        this.page();
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.loadingIndicator = true;
        this.state = state;
        this.emitState.emit(this.state);
        this.service.query(state, this.controller, this.filters);
      }
    
    page() {
        this.loadingIndicator = true;
        this.view = this.service;
        this.service.query(this.state, this.controller, this.filters);
    }

  }