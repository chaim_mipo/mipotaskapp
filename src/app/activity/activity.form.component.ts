import { Component, OnInit, Input, ViewChild, QueryList, ViewChildren, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';
import { CommonService } from '@app/_services/common.service';
import { GlobalDataService } from '@app/_services/globaldata.service';
import { ActivityService } from '@app/_services/activity.service';
import { RemoveEvent, FileState, SelectEvent, SuccessEvent, UploadComponent } from '@progress/kendo-angular-upload';
import {  UploadInterceptor } from '@app/_helpers';
import { IntlService } from '@progress/kendo-angular-intl';
import { environment } from '@environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { DialogService, DialogRef, DialogCloseResult, DialogContentBase } from '@progress/kendo-angular-dialog';
import { DropDownListComponent } from '@progress/kendo-angular-dropdowns';
import { ComboBoxComponent } from '@progress/kendo-angular-dropdowns';
import { Lightbox } from 'ngx-lightbox';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';
import { NotificationService } from '@progress/kendo-angular-notification';
import { trigger, animate, style, transition, state } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';
import "froala-editor/js/plugins.pkgd.min.js";
import * as _ from 'lodash';
// import Tribute from "tributejs";
const Tribute = require("tributejs");
const FroalaEditor = require("froala-editor");

@Component({
  selector: 'activity-form',
  templateUrl: 'activity.form.component.html',
  providers: [{
      provide: NG_VALUE_ACCESSOR,
      useExisting: UploadInterceptor,
      multi: true
    }
  ],
  animations: [
    trigger('EnterLeave', [
        state('flyIn', style({ transform: 'translateX(0)' })),
        transition(':enter', [
          style({ transform: 'translateX(-100%)' }),
          animate('0.15s 300ms ease-in')
        ]),
        transition(':leave', [
          animate('0.15s ease-out', style({ transform: 'translateX(-100%)' }))
        ])
      ])
  ] 
})

export class ActivityFormComponent extends DialogContentBase implements OnInit, OnDestroy {
    @ViewChild('myUpload', {static: false}) myUpload: UploadComponent;
    @ViewChild('textAreaComment',{static: false}) textAreaComment: any;
    @ViewChild('activitySummary',{static: false}) activitySummary: any;
    @ViewChild('taskDropDownInstance', {static: false}) taskDropDownInstance: DropDownListComponent;
    
    @ViewChild('commentsEnd',{static: false}) commentsEnd: any;
    @ViewChild('historyStart',{static: false}) historyStart: any;
    @ViewChildren('myAutoConnectModels') myAutoConnectModels: QueryList<ComboBoxComponent>;
    private subscription: Subscription;
    @Input() public my_parent: any;
    @Input() public this_dialog: DialogRef;
    @Input() act_id : number = 0;
    @Input() title: string = 'Add Activity';
    @Input() def_type: string = '1';
    @Input() afterSubmitAction: string = '';
    @Input() board_id: number = 0;
    @Input() model: string = '';
    @Input() model_id: number = 0;
    @Input() is_template: number = 0;
    @Input() from_module: string = 'grid';
    @Input() from_mode: string = 'page';
    @Input() from_component: string = '';
    @Input() models_connect = [];

    public apiKey: string = 'nQE2uD1A1G2H1C2C1lFa1f1PVWEd2Fa1XHTHh1THMMb1NCg1tA2B2C2E1C5F1D1F1I4B11==';   
    public fix_title: string = 'Add_Activity';
    
    public def_priority: number = 2;
    public current_priority: number = this.def_priority;
    public current_type: number = 1;
    public show_type_control: boolean = false;
    public show_priority_control: boolean = false;

    public newCommentFroala: any;
    public activityFormSubmitted: boolean = false;
    public activityCommentSubmitted: boolean = false;
    public ready: boolean = false;
    public saving: boolean = false;    
    public dialog_form_content_add_class: string = '';
    public newCommentContent: string = '';
    public myName: string = 'Def User';
    public myFirstName: string = '';
    public myLastName: string = '';
    public myColor: string = '';
    public myPhoto: string = 'assets/images/user_def.png';
    public myId: number;
    public act_key: string = '';
    public dialog_form_content_mode_class: string = 'addMode';

    public all_activities = [];
    public comments = [];
    public log = [];
    public comment_history_tab: number = 1;
    public comment_history_col: number = 7;
    public is_rtl: number = 0;
    public defaultTopic = {title: "Select Topic", id: null};
    private checkListData = [];
    public editorOptionDefDirection: string = 'auto';

    activityForm: FormGroup;
    status_list = [];
    time_list = [];
    start_hours = [];
    all_tags = [];
    assignee_list = [];
    priority_list = [];
    assignee_list_source = [];
    topic_list= [];
    default_assignee;
    form_data = {};
    public buttomNewCommentLine: boolean = false;

    connect_data_1 = [];
    connect_data_2 = [];
    connect_data_3 = [];
    connect_data_4 = [];
    connect_data_5 = [];
    connect_data_6 = [];
    connect_data_7 = [];
    connect_data_8 = [];
    connect_data_9 = [];
    connect_model_list = [];

    public activityFiles: Array<any>;
    uploadSaveUrl: string = 'saveUrl'; 
    uploadRemoveUrl: string = 'removeUrl';  
    upload_files_count: number = 0; 
    removeField : string = 'myFiles';
    uploadField : string = 'files';
    removeMethod: string = 'POST';
    withCredentials: boolean = false;
    requestHeader = new HttpHeaders({
        'Authorization': "Bearer " + localStorage.getItem(environment.userKey)['access_token']
        });     
    
    activityFormErrors = {
        from_date : '',
        from_time : '',
        to_time : '',            
        summary :  '',
        type: '',
        description : '',
        assigned_to : '',
        watchers: '',
        myUpload : '',
        tags: ''
      };
            
    validationMessages = {};    

    private initItemData: activityItemData = {
        from_date: null,
        from_time: '',
        to_time: '',
        duration: '',
        summary: '',
        type: this.def_type,
        priority: this.def_priority,
        description: '',
        assigned_to: '',
        watchers: '',
        tags: '',
        status_id: 1,
        topic_id: '',
        sprint_id: '',
        release_id: '',
        desc_tags: [],
        parent_id: ''       
    };

    public type_list: {id: number, name: string, icon?: string, color?: string}[] = [
        {id: 1, name: 'Task', icon: 'k-i-clock', color: '4BADE8' },
        {id: 2, name: 'Bug', icon: 'k-i-close-circle', color: 'E5493A'},
        {id: 3, name: 'Feature', icon: 'k-i-check-circle', color: '9759E4' }
      ];

    public sprint_list: {id: number, name: string}[] = [];  
    public release_list: {id: number, name: string}[] = [];

    constructor(
        private dialogService: DialogService,
        private globalDataService: GlobalDataService, 
        private translateService:TranslateService,
        private intl: IntlService, 
        private fb: FormBuilder, 
        private commonService: CommonService, 
        private lightbox: Lightbox,
        private cdr: ChangeDetectorRef,
        private notificationService: NotificationService,
        private activityService: ActivityService,
        public dialog: DialogRef) {
            super(dialog);
            this.current_type = parseInt(this.def_type);
            
            this.translateService.get(['GENERAL.REQUIRED_FIELD', 'GENERAL.MAX_LENGTH_180', 'BOARD.SELECT_TOPIC']).subscribe((data:any)=> {

                this.validationMessages = {
                    'summary': {
                        'required': data['GENERAL.REQUIRED_FIELD'],
                        'maxlength': data['GENERAL.MAX_LENGTH_180']
                    },
                    'assigned_to': {
                        'required': data['GENERAL.REQUIRED_FIELD']
                    }
                };


                this.defaultTopic = {title: data['BOARD.SELECT_TOPIC'], id: null};
            });

            if(environment.lang == 'he') {
                this.editorOptionDefDirection = 'Rtl';
            }

        }

    handleEditorBlur(event:any): void {
        this.autoSave('description', this.activityForm.controls['description'].value);
    }


    connectFilterChange(event: string, id: number): void {
        const data_var_key = 'connect_data_' + id;
        const cur_combox_id = id - 1;
        let cur_combox = this.myAutoConnectModels.toArray()[cur_combox_id];
        cur_combox.loading = true;

        const params = 'mode=' +  id + '&term=' + event;

        this.activityService.getComboList(params)
        .subscribe(
            results => {
                cur_combox.loading = false;
                if(results['success']) {
                    this[data_var_key] = results['items'];
                }
            }
        );        
    }

    connectValueChange(event: any, id: number): void {        
        if(this.act_id) {            
            const val = event ? event['value'] : '';
            const params = {
                mode: id,
                val: val
            }

            const setting = {
                connect_model: 'change'
            };
            this.saving = true;
            this.activityService.autoSave(this.act_id, params, setting)
                .subscribe(
                    results => {                          
                        setTimeout(() => {   
                        this.saving = false;
                        }, 150);    

                        if(results['success']) {

                        }
                });


        }
    }

    selectEventHandler(e: SelectEvent) {
     
    } 

    removeEventHandler(e: RemoveEvent): void {
        e.data = {
          uid: e.files[0].uid,
          name: e.files[0].name
        };
      }    

    errorEventHandler(e: ErrorEvent): void {

        if(e['response'] && e['response'].body && e['response'].body.data && e['response'].body.data.message) {
            //this.alertService.error(e.['response'].body.data.message);
        }
    }

    successEventHandler(e: SuccessEvent): void { 
        if(e.operation == 'upload' && e.response && e.response.body && e.response.body.data && e.response.body.data && e.response.body.data.item) {
          this.upload_files_count = this.upload_files_count - 1;
          e.files[0].name = e.response.body.data.item.name;
          e.files[0]['created'] = e.response.body.data.item.created;
          e.files[0]['old_name'] = e.response.body.data.item.old_name;
        }
      }  

    assigneeClientFilter(value: string): void {
        this.assignee_list = this.assignee_list_source.filter((s) => s.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }

    public setActLinkToClip(): void {
        let link = environment.appDomain + '#/boards/' + this.board_id;        
        link += this.act_key ? '/' + this.act_key : ';actId=' + this.act_id;
        HelpersFuncs.copyMessage(link);

        this.notificationService.show({
            content: 'Coped to clipboard',
            cssClass: 'button-notification',
            animation: { type: 'fade', duration: 200 },
            position: { horizontal: 'center', vertical: 'top' },
            type: { style: 'success', icon: true },
            hideAfter: 1800
        });       
    }    

    getFileUrl(name: string, state: number): string {
        let url = 'assets/images/skype_mask_.gif';
        if(this.upload_files_count == 0 || state == 1 || state == 3) {
            url = environment.domain + environment.uploadDir + name;  
        }
        return url;
    }    

    getFileType(type: string, name: string, f): string { 
        type = type.toLowerCase();
        let html = '<span class="k-file-extension">' + type + '</span>';
    //    console.log('getFileType: name = ' + name + '; state = ' + f.state);
        if ((type == 'jpg' || type == 'png' || type == 'jpeg') && (f.state == 1 || f.state == 3)) {
            html = '<img class="upload_image_thumb" src="' + this.getFileUrl(name, f.state) + '" />';
        }
        return html;
    }    

    showButton(state: FileState): boolean {
        return (state === FileState.Uploaded || state === FileState.Initial) ? true : false;
    }  
    
    public download(name: string): void {
        let link = this.getFileUrl(name, 1);
        window.open(link, '_blank');
    } 
    
    public viewImage(name: string): void{
        const link = this.getFileUrl(name, 1);
        let albums = [];        

        const album = {
           src: link,
           caption: '',
           thumb: link
        };
   
        albums.push(album);        
        this.lightbox.open(albums, 0);
    }

    public remove(upload, uid: string): void {
        upload.removeFilesByUid(uid);
    }    

    public onCommentHistoryTabSelect(event: any): void {        
        this.comment_history_tab = event.index + 1;

        if(this.comment_history_tab == 1) {
            this.comment_history_col = 7;
            this.dialog_form_content_add_class = '';
        } else if(this.comment_history_tab == 2) {
            this.comment_history_col = 7;
            this.getHistory();
        } else if(this.comment_history_tab == 3) {
            this.dialog_form_content_add_class = '';
            this.comment_history_col = 11;
        } else if(this.comment_history_tab == 4) {
            this.comment_history_col = 7;
        }
    }

    getHistory(): void {
        const params = {
            act_id: this.act_id
        }

        this.activityService.getHistory(params)
        .subscribe(
            results => {
                this.log = results['log'];
            }
        );
    }


    public ngOnInit() {         
        let that = this;
        const params = {
            id: this.act_id,
            board_id: this.board_id,
            model: this.model,
            model_id: this.model_id,
            models_connect: this.models_connect
        }
  
        if(this.is_template) {
            this.default_assignee = { name: "בחר אחראי/ת", id: null };
        } else {
            if(this.act_id) {
                this.dialog_form_content_mode_class = 'editMode';
                this.default_assignee = false;
            } else {
                this.default_assignee = { name: "בחר אחראי/ת", id: null };
            }
        }

        this.initItemData.from_date = this.is_template ? '' : this.intl.formatDate(new Date(), 'dd/MM/yyyy');

        this.activityService.getItem(params)
        .subscribe(   
            results => {
                this.time_list = results['time_list'];
                this.start_hours = results['start_hours'];
                this.all_tags = results['all_tags'];
                this.assignee_list_source = results['assignee_list'];
                this.priority_list = results['priority_list'];
                this.topic_list = results['topic_list'];
                this.sprint_list = results['sprint_list'];
                this.release_list = results['release_list'];                
                this.all_activities = results['all_activities'];
                
                this.connect_model_list = results['connect_model_list'];
                if(this.connect_model_list && this.connect_model_list.length) {
                    this.connect_model_list.forEach(function(obj) { 
                        that['connect_data_' + obj['id']] = obj['data'];
                    });
                }

                if(this.assignee_list_source && this.assignee_list_source.length) {
                    this.assignee_list = this.assignee_list_source.slice();
                }

                if(results['user']) {
                    this.myName = results['user']['my_name'];
                    this.myFirstName = results['user']['my_first_name'];
                    this.myLastName = results['user']['my_last_name'];
                    this.myPhoto = results['user']['my_photo'];
                    this.myId = results['user']['my_id'];
                    this.myColor = results['user']['my_color'];
                }

                this.is_rtl = results['is_rtl'];
                if(this.is_rtl) {
                    this.editorOptionDefDirection = 'Rtl';
                }
                this.board_id = results['board_id'];
                
                if(this.act_id) {

                 //   this.editor_init['inline'] = true;

                    if(results['form_data_success']) {

                        if(results['act_key']) {
                            this.title = results['act_key'];
                            this.fix_title = results['act_key'].replace(/\s/g, '');
                            this.act_key = results['act_key'];
                        } else {
                            this.title = 'Edit Activity';
                            this.fix_title = 'Edit_Activity';
                        }                        

                        this.form_data = results['form_data'];
                        this.current_priority = this.form_data['priority'];

                        this.status_list = results['status_list'];

                        this.comments = results['comments'];
                        this.comments.forEach(function(obj) { obj.edit_mode = 0; obj.more_mode = 0; obj.loading = 0;});
                        
                        if(results['file_list']) {
                            this.activityFiles = results['file_list'];
                            this.activityForm.controls['myUpload'].setValue(this.activityFiles);
                        }

                        for (var key in results['form_data']) {
                            var attrName = key;
                            var attrValue = results['form_data'][key];

                            if (this.activityForm.controls[attrName]) {
                                if(attrName == 'from_date' && attrValue) {
                                    attrValue = new Date(attrValue);
                                } else if((attrName == 'watchers') && attrValue) {
                                    var tmp = attrValue.split(",").map(Number);
                                    attrValue = tmp;
                                }

                                this.activityForm.controls[attrName].setValue(attrValue);                                
                                if(_.has(this.initItemData, attrName)){
                                    if(attrName == 'from_date' && attrValue) {
                                        attrValue = this.intl.formatDate(attrValue, 'dd/MM/yyyy');
                                    }
                                    this.initItemData[attrName] = attrValue;
                                }
                            } else if(attrName == 'desc_tags') {
                                if(_.has(this.initItemData, attrName)){
                                    this.initItemData[attrName] = attrValue;
                                }
                            }
                        }
//console.log(this.initItemData);
                        this.current_type = parseInt(this.activityForm.controls['type'].value);
                    } else {
                        if(results['action']) {
                            if(results['action'] == 'close') {
                                this.this_dialog.close();

                                this.notificationService.show({
                                    content: results['mess'],
                                    cssClass: 'button-notification',
                                    animation: { type: 'fade', duration: 200 },
                                    position: { horizontal: 'center', vertical: 'top' },
                                    type: { style: 'error', icon: true },
                                    hideAfter: 2800
                                });                                 
                            }
                        }
                    }
                } else {
                    this.activityForm.controls['from_time'].setValue(this.start_hours['from_hour']);
                    this.activityForm.controls['to_time'].setValue(this.start_hours['to_hour']);  
                    if(this.myId) {
                        this.activityForm.controls['assigned_to'].setValue(this.myId);                    
                    }
                }

                this.ready = true;
                this.addStartFocus();
            },
            () => {

               this.this_dialog.close();
            }
        );
        
        if (this.act_id) {
            this.uploadSaveUrl = environment.apiUrl + '/activity/upload-image?act_id=' + this.act_id;
            this.uploadRemoveUrl = environment.apiUrl + '/activity/delete-image?act_id=' + this.act_id;                
        } else {
            this.uploadSaveUrl = 'saveUrl'; 
            this.uploadRemoveUrl = 'removeUrl'; 
        }

        this.createForm();  
        

        FroalaEditor.DefineIcon("chDirRtl", { NAME: "info", SVG_KEY: "alignLeft" });
        FroalaEditor.DefineIcon("chDirLtr", { NAME: "info", SVG_KEY: "alignRight" });
        FroalaEditor.RegisterCommand("chDirLtr", {
          title: "Change Direction",
          focus: true,
          undo: true,
          refreshAfterCallback: true,
          callback: function() {
            var editor = this;
            this.html.set('<div class="fro-rtl">'+this.html.get().replace('class="fro-ltr"',"")+'</div>');
            /*editor.destroy();
            editor = null;
            if(this.$oel[0].getAttribute("name")=="description")
                editor = new FroalaEditor("textarea#froala-"+that.title,that.editorOptions('Rtl','description'));
            else if(this.$oel[0].getAttribute("name")=="comment_new"){
                editor = new FroalaEditor("textarea#froala-comment-new",that.editorOptions('Rtl','comment_new'));
                that.newCommentFroala = editor;
            }
            else
                editor = new FroalaEditor("textarea#"+this.$oel[0].getAttribute("id"),that.editorOptions('Rtl','comment_edit'));*/
          }
        });
        FroalaEditor.RegisterShortcut(39, "chDirLtr", false, 'Right', true, true);
        FroalaEditor.RegisterCommand("chDirRtl", {
          title: "Change Direction",
          focus: true,
          undo: true,
          refreshAfterCallback: true,
          callback: function() {
            var editor = this;
            this.html.set('<div class="fro-ltr">'+this.html.get().replace('class="fro-rtl"',"")+'</div>');
            /*editor.destroy();
            editor = null;
            if(this.$oel[0].getAttribute("name")=="description")
                editor = new FroalaEditor("textarea#froala-"+that.title,that.editorOptions('Ltr','description'));
            else if(this.$oel[0].getAttribute("name")=="comment_new"){
                editor = new FroalaEditor("textarea#froala-comment-new",that.editorOptions('Ltr','comment_new'));
                that.newCommentFroala = editor;
            }
            else
                editor = new FroalaEditor("textarea#"+this.$oel[0].getAttribute("id"),that.editorOptions('Ltr','comment_edit'));*/
          }
        });
        FroalaEditor.RegisterShortcut(37, "chDirRtl", false, 'Left', true, true);
    }

    public tributeOptions = function(editor,assignee_list) {
        var tribute = new Tribute({
            values: assignee_list,
            selectTemplate: function(item) {
            return (
                '<span class="fr-deletable fr-tribute" data-user="'+item.original.value+'">' +
                item.original.key +
                "</a></span>"
            );
            }
        });

        //var editor = this;
        tribute.attach(editor.el);

        editor.events.on(
            "keydown",
            function(e) {
                if (e.which === FroalaEditor.KEYCODE.ENTER && tribute.isActive) {
                    return false;
                }
            },
            true
        );
    }

    public editorOptions(direction:string,ed_type:string) {
        let that = this;
        let env_key = localStorage.getItem(environment.userKey);
        env_key = typeof env_key["access_token"] != "undefined" ? env_key["access_token"] : JSON.parse(env_key)["access_token"];
        return {
            //toolbarButtons: ["bold", "italic", '|', "chDirRtl", "chDirLtr", '|', 'textColor', 'backgroundColor', '|', 'insertLink', '|', 'formatOL', 'formatUL'],
            toolbarButtons: {
                'custom': {'buttons': ["bold", "italic", '|', (direction=="Rtl" ? "chDirLtr" : "chDirRtl"), (direction=="Rtl" ? "chDirRtl" : "chDirLtr"), '|', /*'insertLink',*/ 'textColor', 'backgroundColor', '|', 'insertImage'], 'buttonsVisible': 7}
               // ,'moreText': {'buttons': ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', 'textColor', 'backgroundColor', 'inlineClass', 'inlineStyle', 'clearFormatting'], 'buttonsVisible': 0},
            //    'moreParagraph': {'buttons': ['alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'alignJustify', 'formatOL', 'formatUL', 'paragraphFormat', 'paragraphStyle', 'lineHeight', 'outdent', 'indent', 'quote'], 'buttonsVisible': 0},
            /*    'moreRich': {'buttons': ['insertLink', 'insertImage', 'insertVideo', 'insertTable', 'emoticons', 'fontAwesome', 'specialCharacters', 'embedly', 'insertFile', 'insertHR'], 'buttonsVisible': 0},
                'moreMisc': {'buttons': ['undo', 'redo', 'fullscreen', 'print', 'getPDF', 'spellChecker', 'selectAll', 'html', 'help'], 'buttonsVisible': 0}*/
            },
        //    pluginsEnabled: ['image', 'link', 'align', 'file'],
            direction: direction.toLowerCase(),
            autofocus: true,
            key: this.apiKey,
            imageUploadURL: environment.domain+'app/editor/upload-editor-image?act_id='+that.act_id,
            fileUploadURL: environment.domain+'app/activity/upload-image?act_id='+that.act_id,
            imageUploadParam: 'files',
            requestHeaders: {'Authorization' : 'Bearer ' + env_key},
            events: {
                'image.beforeRemove' : function(img) {
                    that.activityService.removeEditorImage({img:img[0].getAttribute('src')}).subscribe(
                        results => {                            
                        }
                    );
                },
                destroy: function(){
                    //console.log("DESTROY!",this)
                    //console.log("DESTROY!",ed_type,this.html.get());
                    if (ed_type=="comment_new"){
                        that.newCommentContent = this.html.get();
                    }
                },
                initialized: function(){
                    if(this.$oel[0].getAttribute("name")=="comment_new"){
                        that.newCommentFroala = this;
                        this.html.set(that.newCommentContent);
                    }
                    that.tributeOptions(this,that.assignee_list.map(function(item){return {key:item.name,value:item.id};}));
                },
                blur: function(){
                    if(ed_type=="description"){
                        that.activityForm.controls['desc_tags'] = new FormControl(Array.from(document.querySelector("textarea#froala-"+that.fix_title).parentNode.querySelectorAll(".fr-tribute")).map(elem => {return elem.getAttribute("data-user");}));
                        if(that.act_id) {
                            that.autoSave('description', this.html.get());
                            that.autoSave('desc_tags', that.activityForm.controls['desc_tags'].value);
                        }
                    }
                    else if (ed_type=="comment_new"){
                        that.newCommentContent = this.html.get();
                    }
                    else{
                        let this_id = this.$oel[0].getAttribute("name").substr(13);
                        var obj = that.comments.find(
                            message => message.id === parseInt(this_id));
                        obj.text = this.html.get();
                    }
                }
            }
        }
    };

    selectFromTime(event: any): void {    
        const from_time = event.split(":");
        const from_hour = parseInt(from_time[0]);

        const hour = from_hour == 23 ? 0 : from_hour + 1;    
        const to_hour = hour < 10 ? String('0'+hour) : String(hour);
        const to_time = to_hour+':'+ from_time[1];

        this.activityForm.controls['to_time'].setValue(to_time);
    }    

    onValueChanged(data?: any) {
        if (!this.activityForm) { return; }
        const form = this.activityForm;
        for (const field in this.activityFormErrors) {
          this.activityFormErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.activityFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      }    

    closeDialog(): void {
        if(this.this_dialog) {
            this.this_dialog.close();
        }
    }

    public selectPriority(event:any, button:{id: number, name: string}): void {
        if(event) {
            this.current_priority = button.id;
            if(this.act_id) {
                this.autoSave('priority', this.current_priority)
            }
        }
    }

    public selectType(event:any, button:{id: number, name: string}): void { 
        if(event) {
            this.current_type = button.id;
            if(this.act_id) {
                this.autoSave('type', this.current_type)
            }
        }
    }    

    public checklistData(event: any): void {
        this.checkListData = event;
    }

    onActivityFormSubmit(): void {
        this.activityFormSubmitted = true;
        const formData = new FormData();
        
        let files = this.activityForm.value['myUpload']
        for (let file in files) {
            formData.append('files[]', files[file]['rawFile']);
         }      

        formData.append('RealtorActivity[id]', String(this.act_id));
        formData.append('RealtorActivity[board_id]', String(this.board_id));
        formData.append('RealtorActivity[model]', this.model);
        formData.append('RealtorActivity[model_id]', String(this.model_id));
        formData.append('RealtorActivity[is_template]', String(this.is_template));        
        formData.append('RealtorActivity[from_module]', String(this.from_module));
        formData.append('RealtorActivity[from_mode]', String(this.from_mode));
        formData.append('RealtorActivity[models_connect]', JSON.stringify(this.models_connect));
        formData.append('RealtorActivity[type]', String(this.current_type));
        formData.append('RealtorActivity[priority]', String(this.current_priority));

        const desc_tags = this.activityForm.controls['desc_tags'].value;
        for (let i = 0; i < desc_tags.length; i++) {
            formData.append('RealtorActivity[desc_tags][]', desc_tags[i]);
        }

        Object.keys(this.activityForm.controls).forEach(key => {     
            if(key != 'myUpload' && key != 'type' && key != 'priority' && key != 'desc_tags') {   
                let val = this.activityForm.controls[key].value;
                if(key == 'from_date') {
                    val = this.intl.formatDate(val, 'dd/MM/yyyy');
                } else if (Object.prototype.toString.call( val ) === '[object Array]' ) {
                    val = val.join(',');
                    if (val == 'null' || val == 'undefined' || val == 'NaN') {
                        val = '';
                    }
                }
                formData.append('RealtorActivity[' + key + ']', val);
            }
        });

        let current_act_after = this.globalDataService.getActivityAfterSubmitAction(this.current_type);
  


        this.myAutoConnectModels.forEach(function(obj, i) { 
            formData.append('ConnectModel[' + (i + 1) + ']', obj.value);
        });

        
        this.checkListData.forEach(function(obj, i) {
            var is_done = obj['is_done'] ? 1 : 0;
            formData.append('CheckList[' + (i + 1) + '][title]', obj.title);
            formData.append('CheckList[' + (i + 1) + '][is_done]', String(is_done));
        });
  
        this.activityService.saveItem(formData)
        .subscribe(
            results => {
                this.activityFormSubmitted = false;
                if(results['success']) {
                    if(this.afterSubmitAction) {    
                        if(this.from_module == 'board') {
                            this.commonService.notifyOther({action: this.afterSubmitAction, new_item: results['new_item']});
                        } else if(this.from_module == 'grid') {             
                            this.commonService.notifyOther({action: current_act_after});
                        } else {
                            this.commonService.notifyOther({action: this.afterSubmitAction, item: results['item']});
                        }
                    }
                }
                this.closeDialog();
            });

    }

    createForm(): void {
        this.activityForm = this.fb.group({
            from_date : this.is_template ? '' : new Date(),
            from_time : '',
            to_time : '',
            duration: '',            
            summary : ['', Validators.compose([Validators.maxLength(180), Validators.required])],
            type: this.def_type,
            priority: this.def_priority,
            description: '',
            assigned_to: this.is_template ? '' : ['', Validators.required ],
            watchers: '',
            myUpload: [this.activityFiles],
            tags: '',
            status_id: 1,
            topic_id: '',
            sprint_id: '',
            release_id: '',
            parent_id: ''
        });
    
        this.subscription = this.activityForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
  
        this.onValueChanged();    

      }
    
      public changeTopic(event: {}): void {  
          this.autoSave('topic_id', event['topic_id']);
      }

    ngOnDestroy() {

        if(this.subscription) {
            this.subscription.unsubscribe()
        }
    }    
    
    public filterTasks(term: string): void {
        this.taskDropDownInstance.data = this.all_activities.filter((s) => 
            (s.summary.toLowerCase().indexOf(term.toLowerCase()) !== -1 || s.prefix.toLowerCase().indexOf(term.toLowerCase()) !== -1)
        );
    }     

      public autoSave(field: any, event: any): void {
//console.log(field);console.log(event);
        if(field == 'type') {
            this.current_type = parseInt(event);
        }

        if((this.activityForm.controls[field] && this.activityForm.controls[field].valid) && this.act_id) {
          let params = {};
          let val: any;

          if(field == 'type' || field == 'priority') {
            params[field] = encodeURIComponent(event);
            val = params[field];
          } else if(field == 'from_date') {
            params[field] = this.intl.formatDate(event, 'dd/MM/yyyy');
          } else if (field == 'search_city_ids' || field == 'search_hood_ids' || field == 'topic_id' || field == 'sprint_id' || field == 'release_id' || field == 'description' || field == 'desc_tags') {

            if((field == 'sprint_id' || field == 'release_id')  && (typeof event === 'undefined' || event === null )) {
                event = '';
            }            

            params[field] = event;
          } else {
            val = this.activityForm.controls[field].value;
            if( Object.prototype.toString.call( val ) === '[object Array]' ) {
              val = val.join(',');
            } else {
            //  val = encodeURIComponent(val);
              if(val == 'true') {
                val = 1;
              } else if(val == 'false') {
                val = 0;
              } else if(val == 'null') {
                val = '';
              }
            }
    
            params[field] = val;
          }

        let sendToServer: boolean = true;

        if(_.has(this.initItemData, field)){                                        
            const prev_val = this.initItemData[field];

            if(_.isArray(params[field])) {
                if(! _.xor(prev_val, params[field]).length) {
                    sendToServer = false;
                }
            } else {
                if(params[field] == prev_val) {
                    sendToServer = false;
                }
            }
        } 

 //       console.log(this.form_data[field] + '  |  ' + field + '  =  ' + val);   
          if(params && this.act_id && sendToServer) {//console.log(params);
            const setting = {
                from_module: this.from_module,
                from_mode: this.from_mode
            };
            this.saving = true;
            this.activityService.autoSave(this.act_id, params, setting)
                  .subscribe(
                      results => {   

                        if(_.has(this.initItemData, field)){                             
                           this.initItemData[field] = params[field];
                        }                        
                                                
                          setTimeout(() => {                              
                            this.saving = false;
                           
                            if (!this.cdr['destroyed']) {
                                this.cdr.detectChanges();   
                            }
                          }, 1400);    

                          if(results['success']) {
                            if(this.from_module == 'board') {
                                if(results['afterUpdate']) {
                                    this.commonService.notifyOther({action: results['afterUpdate']});
                                }
                            } else if(this.from_module == 'grid') {
                                if(field == 'type') {
                                    this.commonService.notifyOther({action: 'loadCallsAndMeetings'});
                                    this.commonService.notifyOther({action: 'loadTasks'});
                                } else {
                                    if(results['afterUpdate']) { 
                                        if(this.from_component == 'time_grid') {
                                            this.commonService.notifyOther({action: 'loadTime'});
                                        } else {                                        
                                            this.commonService.notifyOther({action: results['afterUpdate']});
                                        }

                                    } else {                                    
                                        let current_act_after = this.globalDataService.getActivityAfterSubmitAction(this.activityForm.controls['type'].value);
                                        this.commonService.notifyOther({action: current_act_after});    
                                    }
                                }
                            } else if(this.from_module == 'log') {
                                this.commonService.notifyOther({action: this.afterSubmitAction, item: results['item']});
                            }

                            if(results['modified']) {
                                this.form_data['modified'] = results['modified'];
                            }
                          }
                      }
                  );
          }
    
    
        }
      }      

      addCommentFocus(): void {
          this.dialog_form_content_add_class = 'dialog_form_content_comment_focus';
          this.newCommentFroala = new FroalaEditor("textarea#froala-comment-new",this.editorOptions('Ltr','comment_new'));
          this.commentsEnd.nativeElement.scrollIntoView();
          setTimeout(() => {   
            this.commentsEnd.nativeElement.scrollIntoView();
          }, 0);
      }

      addStartFocus(): void {
        if(!this.act_id) {
            setTimeout(() => {   
            this.activitySummary.nativeElement.focus();
        //    this.activitySummary.nativeElement.scrollIntoView();
            }, 400);         
        }                      
    }      

      cancelNewComment(): void {
        this.dialog_form_content_add_class = '';
        this.activityCommentSubmitted = false;
        this.newCommentContent = '';
        setTimeout(() => {   
            this.commentsEnd.nativeElement.scrollIntoView();
        }, 0);
      }

      addNewComment(): void {
        this.activityCommentSubmitted = true;
        //let text = this.newCommentContent.replace(/(<([^>]+)>)/ig,"").replace(/(?:\r\n|\r|\n)/g, '<br />');
        let text = this.newCommentContent;
        let tags = Array.from(document.querySelector("textarea#froala-comment-new").parentNode.querySelectorAll(".fr-tribute")).map(elem => {return elem.getAttribute("data-user");});
        this.activityService.addComment(this.act_id, text, tags)
        .subscribe(
            results => {
                if(results['success']) {
                    if(results['item']) {
                        setTimeout(() => {
                            results['item']['edit_mode'] = 0;
                            results['item']['more_mode'] = 0;
                            results['item']['loading'] = 0;      
                            results['item']['state'] = 'active';                      
                            this.comments.push(results['item']);
                            this.activityCommentSubmitted = false;
                            this.dialog_form_content_add_class = '';
                            
                            setTimeout(() => {   
                                this.newCommentFroala.html.set('');
                                this.newCommentContent = '';
                                this.commentsEnd.nativeElement.scrollIntoView();
                            }, 0);             
                        }, 450);                            
                    }
                }
            }
        );
   
      }


      editComment(id: number, i?: number): void {

        const obj = this.comments.find(
            message => message.id === id); 
          if (obj.edit_mode == 0) {   
            obj.edit_mode = 1; 
            obj.prev_text = obj.text;   
          } else {
            obj.edit_mode = 0;   
            obj.text = obj.prev_text;   
          }    
      }

      br2nl(str: string): string {
        return str.replace(/<br\s*\/?>/mg,"\n");
      }    
      
      cancelEditComment(id: number): void {
        const obj = this.comments.find(
          message => message.id === id); 
         obj.edit_mode = 0;
         obj.text = obj.prev_text; 
      }  
      
      removeComment(id: number): void {

        this.translateService.get(['ACTIVITY.DELETE_COMMENT', 'GENERAL']).subscribe((data:any)=> {

                const dialog: DialogRef = this.dialogService.open({
                    title: data['ACTIVITY.DELETE_COMMENT'],
                    content: data['GENERAL']['ARE_YOU_SURE'],
                    actions: [                                
                        { text: data['GENERAL']['NO'] },
                        { text: data['GENERAL']['YES'], primary: true }
                    ],
                    width: 350,
                    height: 200,
                    minWidth: 250
                });

                dialog.result.subscribe((result) => {
                    if (result instanceof DialogCloseResult) {

                    } else {                            
                        if(result['text'] == data['GENERAL']['YES']) {

                            const obj = this.comments.find(
                                message => message.id === id);                
                
                            obj.loading = 1;
                        
                            const params = {
                                comment_id: id,
                                act_id: this.act_id             
                            }

                            this.activityService.removeComment(params)
                                .subscribe(
                                    results => {
                                    obj.edit_mode = 0;
                                        obj.loading = 0;
                                        if(results['success']) {
                                            obj.loading = 0;
                                            this.deleteNote(obj);
                                        }
                                    }
                                );      
                                

                        }
                    }

                });
        });
      }

      deleteNote(note) {
        const index: number = this.comments.indexOf(note);
        if (index !== -1) {
            this.comments.splice(index, 1);
        }        
      }

      saveComment(id: number): void {
        const obj = this.comments.find(
            message => message.id === id); 
            const text = obj.text.trim();
            const tags = Array.from(document.querySelector("textarea#froala_comment_edit_"+id).parentNode.querySelectorAll(".fr-tribute")).map(elem => {return elem.getAttribute("data-user");});
        if(text) {
            //text = text.replace(/(<([^>]+)>)/ig,"").replace(/(?:\r\n|\r|\n)/g, '<br />');

            obj.loading = 1;
      
            const params = {
              text: text,
              tags: tags,
              comment_id: id,
              act_id: this.act_id             
            }
            this.activityService.saveComment(params)
              .subscribe(
                  results => {
                    obj.edit_mode = 0;
                      obj.loading = 0;
                      if(results['success']) {
                        obj.text = text;
                      }
                  }
              );      
          }            
      }
}

interface activityItemData {
    from_date: string,
    from_time : string,
    to_time : string,
    duration: string,            
    summary : string,
    type: string,
    priority: number,
    description: string,
    assigned_to: string,
    watchers: string,
    tags: string,
    status_id: number,
    topic_id: string,
    sprint_id: string,
    release_id: string,  
    desc_tags: string[],
    parent_id: string 
}