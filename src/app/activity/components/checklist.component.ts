import { Component, OnInit, Input, Output, OnDestroy, ViewChild, EventEmitter } from '@angular/core';
import { ChecklistItem } from './checklist.item';
import { ProgressBarAnimation } from '@progress/kendo-angular-progressbar';
import { ChecklistService } from '@app/_services/checklist.service';
import { TextAreaDirective } from '@progress/kendo-angular-inputs';


@Component({ 
  selector: 'checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.scss']
})
export class ChecklistComponent implements OnInit, OnDestroy {
  @ViewChild('textAreaItem',{static: false}) textAreaItem: any;
  @ViewChild('editTextArea',{static: false}) editTextArea: any;
  @Input() parent_id: number = 0;
  @Output() emitData: EventEmitter<any> = new EventEmitter<any>();

  public data: Array<ChecklistItem> = [];
  public counter: number = 0;
  public newItemTitle: string = '';
  public show_add_control: boolean = false;
  public savingNew: boolean = false;

  public editing_id: number = 0;

  public progressBarAnimation: ProgressBarAnimation = {
    duration: 500
  };

  public progressBarValue = 0;

  public  progressBarLabel = {
    visible: false,
    position: 'start',
    format: 'percent'
 };  

  public progressStyles= {    
    background: '#5cb85c'
  }

  public constructor(private checklistService: ChecklistService){
      this.counter = this.data.length;
  }

  public showControl() {

    if(this.editing_id) {
        let obj2 = this.data.find(
            item2 => item2.id === this.editing_id); 
            
        if(obj2) {
            obj2.is_edit = false;
        }
    }

    this.show_add_control = true;
    setTimeout(() => {  
        this.textAreaItem.nativeElement.focus();
    }, 0);  

  }

  public hideControl() {
    this.closeAddControl();    
  }

  public toggleCheckbox(id: number) {
      if(this.parent_id) {
        this.changeItemAttr('is_done', id);
      } else {
        this.emitData.emit(this.data);
        this.calcIsDone();
      }
  }

  public startEditItem(id: number) {
    let obj = this.data.find(
        item => item.id === id);  
    
    if(obj) {

        if(this.editing_id && this.editing_id != id) {
            let obj2 = this.data.find(
                item2 => item2.id === this.editing_id); 
                
            if(obj2) {
                obj2.is_edit = false;
            }
        }

        this.editing_id = id
        setTimeout(() => {  
            this.editTextArea.nativeElement.focus();
        }, 0);

        obj.is_edit = true;
    }      
  }

  public updateItem(id: number) {
    if(this.editing_id && this.editing_id == id) {
        let obj = this.data.find(
            item => item.id === id);  
        
        if(obj && obj.title.trim()) {

            if(this.parent_id) {
                this.autoSave(id, 'title', obj.title.trim(), obj);
            } else {
                obj.is_edit = false;
                this.editing_id = 0;  
                this.emitData.emit(this.data);  
            }

        }         
    }
}

  public cancelEditItem(id: number) {
    let obj = this.data.find(
        item => item.id === id);  
    
    if(obj) {
        obj.is_edit = false;
        this.editing_id = 0;
    }      
  }

  private changeItemAttr(attr: string, id: number) {
    let obj = this.data.find(
        item => item.id === id);  
    
    if(obj) {
        this.autoSave(id, attr, obj[attr], obj);
    }
  }

  private autoSave(id: number, field: string, value: any, obj: ChecklistItem) {
    if(value == 'true' || value == true) {
        value = 1;
    } else if(value == 'false' || !value || value == false) {
        value = 0;
    }   
    obj.saving = true;
    this.checklistService.autoSave(this.parent_id, id, field, value)
    .subscribe(
        results => {
            if(results['success']) {
                obj.saving = false;
                if(field == 'is_done') {
                    this.calcIsDone();
                } else if(field == 'title') {
                    obj.is_edit = false;
                    this.editing_id = 0;   
                }
            }
        },
        error => {

        }        
    );

  }  


  public addToList() {

    if(this.newItemTitle) {
        let model = new ChecklistItem();
        model.title = this.newItemTitle;
        model.is_done = false;
        
        if(this.parent_id) {
            model.id = 0
        } else {
            this.counter++;
            model.id = this.counter;
        }

        if(this.parent_id) {
            
            let params = model;
            params['parent_id'] = this.parent_id;
            this.savingNew = true;
            this.checklistService.saveItem(params)
            .subscribe(
                results => {
                    this.savingNew = false;
                    if(results['success']) {
                        this.addItemToList(results['item']);
                    }
                },
                error => {
                    this.savingNew = false;
                }
            );

        } else {
            this.addItemToList(model);
        }

    }

  }

  public removeItem(id: number) {
    let obj = this.data.find(
        item => item.id === id);  
    
    if(obj) {
        if(this.parent_id) {
            this.checklistService.removeItem(this.parent_id, id)
            .subscribe(
                results => {
                    if(results['success']) {
                        this.deleteFromList(obj);
                    }
                },
                error => {

                }
            );

            
        } else {
            this.deleteFromList(obj);
        }
    }
     
  }

  private deleteFromList(obj: ChecklistItem) {
    const index: number = this.data.indexOf(obj); 
    if (index !== -1) {
        this.data.splice(index, 1);
        this.calcIsDone();
        this.emitData.emit(this.data);
    }           
  }

  private calcIsDone() {
    let all = this.data.length;
    let checked = 0;
    for(let i:number = 0; i < all; i++) {
        let item = this.data[i];
        if(item.is_done) {
            checked = checked + 1;
        }
    }      

    this.progressBarValue = all ? Math.round((checked/all)*100) : 0;
  }

  private addItemToList(model: ChecklistItem) {
    this.data.push(model);
    this.emitData.emit(this.data);
    this.closeAddControl();
    this.calcIsDone();
  }

  private closeAddControl() {
    this.show_add_control = false;
    this.newItemTitle = '';      
  }



  private loadItems() {
    if(this.parent_id) {
        this.checklistService.getItems(this.parent_id)
        .subscribe(
            results => {
                if(results['success']) {
                    if(results['items'] && results['items'].length) {
                        results['items'].forEach((item: ChecklistItem) => {
                            item.is_edit = false;
                            item.saving = false;
                            this.data.push(item);
                        });

                        this.calcIsDone();
                    }
                }
            },
            error => {

            }
        );

    }      
  }

  public ngOnInit() {
    this.loadItems();
  }
  

  public ngOnDestroy() {

  }

  public br2nl(str: any) {
    return str.toString().replace(/<br\s*\/?>/mg,"\n");
  }   
}
