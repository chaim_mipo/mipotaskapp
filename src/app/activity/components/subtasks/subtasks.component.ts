import { Component, OnInit, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { ActivityService } from '@app/_services/activity.service';


@Component({ 
  selector: 'subtasks',
  templateUrl: './subtasks.component.html',
  styleUrls: ['./subtasks.component.scss']
})
export class SubtasksComponent implements OnInit, OnDestroy {
  @Input() parent_id: number = 0;
  @Input() board_id: number = 0;
  @Input() status_list: {id: number, name: string}[] = [];
  @Output() emitData: EventEmitter<any> = new EventEmitter<any>();

  public items: subtaskItem[] = [];
  public loading: boolean = false;

  public constructor(
      private activityService: ActivityService
    ){

  }


  private loadSubtasks(): void {
    if(this.parent_id) {
        this.loading = true;
        this.activityService.getSubtasks(this.parent_id)
        .subscribe(
            (results) => {
                this.loading = false;
                if(results['success']) {
                   this.items = results['items'];

                   if(this.items.length) {
                       this.items.forEach(item => {
                        if(this.status_list.length) {
                            item.status = this.status_list.find(status => status.id == item.status_id).name
                        }

                        item.link = '/boards/' + this.board_id + '/' + item.prefix;
                       });
                   }
                }
            },
            () => {
                this.loading = false;
            }
        );

    }      
  }

  public ngOnInit() {
    this.loadSubtasks();
  }
  

  public ngOnDestroy() {

  }

 
}

interface subtaskItem {
    id?: number,
    prefix?: string,
    summary?: string,
    user_name?: string,
    status_id?: number,
    status?: string,
    user_color?: string,
    link?: string
}