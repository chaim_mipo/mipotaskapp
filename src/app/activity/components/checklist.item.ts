export class ChecklistItem {
    id: number;
    title: string;
    is_done: boolean;
    is_edit: boolean = false;
    saving: boolean = false;
}