import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { Align } from '@progress/kendo-angular-popup';
import { ActivityService } from '@app/_services/activity.service';

@Component({ 
  selector: 'activity-topic',
  templateUrl: './activity.topic.component.html'
})
export class ActivityTopicComponent implements OnInit, OnDestroy {

  @ViewChild('anchor', {static: false}) public anchor: ElementRef;
  @ViewChild('popup', {static: false, read: ElementRef }) public popup: ElementRef;    
  @Input() topicList = [];
  @Input() selectedId: number = 0;
  @Output() emitAction: EventEmitter<any> = new EventEmitter<any>();
  public def_title: string = 'Add Topic';
  public title: string = this.def_title;
  public show: boolean = false;
  public anchorAlign: Align = { horizontal: "left", vertical: "bottom" };
  public popupAlign: Align = { horizontal: "left", vertical: "top" };
  public saving:boolean = false;

  public constructor(private activityService: ActivityService){}

  public ngOnInit() {
      if(this.selectedId && this.topicList && this.topicList.length) {
        this.buildTitle();
      }
  }

  private buildTitle() {
    for(let i=0; i < this.topicList.length; i++) {
        if(this.topicList[i].id == this.selectedId) {
            this.title = this.topicList[i].title;
        }
    }
  }

  public removeTopic() {
      this.save(0);
  }

  public selectTopic(topic_id: number) {
    this.save(topic_id);
  }

  
  private save(topic_id: number) {
    this.saving = true;
    this.emitAction.emit({topic_id: topic_id});  
    setTimeout(() => {   
        this.saving = false;
        this.selectedId = topic_id;
        if(this.selectedId) {
            this.buildTitle();
        } else {
            this.title = this.def_title;
        }
        this.onToggle();
    }, 350);      
    
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
      if (event.keyCode === 27) {
          this.onToggle(false);
      }
  }

  @HostListener('document:click', ['$event'])
  public documentClick(event: any): void {
      if (!this.contains(event.target)) {
        this.onToggle(false);
      }
  }

  private contains(target: any): boolean {
    return this.anchor.nativeElement.contains(target) ||
        (this.popup ? this.popup.nativeElement.contains(target): false);
  }  

  public onToggle(show?: boolean): void {
    this.show = show !== undefined ? show : !this.show;
  }


  public ngOnDestroy() {

  }
 
}
