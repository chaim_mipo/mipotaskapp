import { Component, OnInit, OnDestroy, Input, ViewChild, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { State, process, DataResult } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent, GridComponent, GroupableSettings } from '@progress/kendo-angular-grid';
import { CategoriesService } from '@app/_services/northwind.service';
import { CommonService, BoardService, TimeService, GlobalDataService} from '@app/_services/index';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NotificationService } from '@progress/kendo-angular-notification';
import { IntlService } from '@progress/kendo-angular-intl';
import { TimeTask } from './timetask.model';
import { DropDownListComponent } from '@progress/kendo-angular-dropdowns';
import { DialogService, DialogRef, DialogCloseResult } from '@progress/kendo-angular-dialog';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';

@Component({
    selector: 'time-grid',
    providers: [CategoriesService],
    templateUrl: './time.grid.component.html'
  })

  export class TimeGridComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private navigationSubscription: Subscription; 


    public isNew: boolean;

    @ViewChild('timeGridInstance', {static: false}) timeGridInstance: GridComponent;
    @ViewChild('taskDropDownInstance', {static: false}) taskDropDownInstance: DropDownListComponent;
    @Input() mode: string = "page";
    @Input() types: string = "";
    @Input() def_take: number = 10;
    @Input() def_skip: number = 0;  
    @Input() def_sort = [];     
    @Input() controller: string = 'time/table';     
    @Input() filters = {}; 
    @Input() run_filter: boolean = false;
    @Input() hide_command_column: boolean = false;
    @Input() hide_pagination: boolean = false;
    @Input() hide_checkbox_column: boolean = false;
    @Input() from_page: string = 'time-report';
    @Input() grid_action_type: number = 0;
    @Input() run_grid_action: boolean = false;
    @Input() scrollable: boolean | string = 'none';


    @Input() board_id: number = 0;
    @Input() act_id: number = 0;
    @Input() exployee_list = [];
    @Input() board_list = [];
    private source_board_list = [];
    private board_list2 = [];

    public task_list = [];
    public source_task_list = [];

    @Output()emitState: EventEmitter<any> = new EventEmitter<any>();  
    @Output()emitLoaded: EventEmitter<boolean> = new EventEmitter();  

    public hours_list = [];
    public sumHours: number = 0;

    public mySelectionKey: string = 'id';
    public mySelection: any = [];    
    public showMoreOptions = {};//: Array<boolean> = new Array<boolean>();
    public view: Observable<GridDataResult>;

    public state: State = {
        skip: this.def_skip,
        take: this.def_take,
        sort: this.def_sort
    };
    //private controller = 'property/table';
    public take_list: number[] = [5, 10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1500];   
    
    public formGroup: FormGroup;
    private editedRowIndex: number;    
    public groupableSettings: GroupableSettings = {
        enabled: false,
        showFooter: true
    };   

    public groupable: boolean = false; 
    public gridData: DataResult;
    public allCount: number;
    public loadedData: any[];
    public selectedGroupBy: string = '';
    public excelFN: string = '';

    public constructor(
        private notificationService: NotificationService,
        private service: CategoriesService, 
        private commonService: CommonService,
        private formBuilder: FormBuilder,
        public intl: IntlService,
        private boardService: BoardService,
        private timeService: TimeService,
        private dialogService: DialogService,
        public globalDataService: GlobalDataService
    ) {
        this.fillHours();
     }  
     
     getPaggebleSetting(): void {
        let ret: any;
        if(this.from_page != 'time-report') {
        //  ret = true;
          ret = {
            buttonCount: 0,
            info: true,
            type: 'numeric',
            pageSizes: false,
            previousNext: false
          }
        } else {
          ret = { pageSizes: this.take_list };
        }
      
        return ret;
      }     

    task(dataItem:any): string {
        let ret: string = '';

        const row = this.timeGridInstance.data['data'].find(({ id }) => id === dataItem.id);
        if(row && row.task_list) {
            let obj = row.task_list.find(
                t => t.id === row.act_id); 
                
            if(obj) {
                ret = obj.summary;
            }                
        }

        return ret;
    }

    public getCurrentDate(): string {
        const now = new Date();
        return now.getDate() + '_' + (now.getMonth() + 1) + '_' + now.getFullYear() + '_' + now.getHours() + '_' + now.getMinutes() + '_' + now.getSeconds();
    }

    public exportToExcel(grid: GridComponent): void {
        this.excelFN = 'time_report_' + HelpersFuncs.getCurrentDate() + '.xlsx';
        grid.saveAsExcel();
    }

    public groupByFieldSelect(val: string): void {
     //   console.log(val);
        if(val) {
            this.groupable = true;
            this.state.group = [{field: val, aggregates: [{field: 'duration', aggregate: 'sum'}]}];
             
         //   this.page();
        } else {
            this.state.group = [];
            this.groupable = false;
        }

        const state: State = JSON.parse(JSON.stringify(this.state));
        state.skip = 0;

        this.gridData = process(this.loadedData, state);
        this.gridData.total = this.allCount;   
    }

    public board(id: number): any {
        return this.board_list2.find(x => x.id === id);
    }


    private fillHours(): void {
        for(let i=0; i <= 10; i+=0.25) {
            const item = {
                value: i,
                text: this.intl.formatNumber(i, 'n2')
            };

            this.hours_list.push(item);
        }
    }

    public filterTasks(term: string): void {
        this.taskDropDownInstance.data = this.source_task_list.filter((s) => 
            (s.summary.toLowerCase().indexOf(term.toLowerCase()) !== -1 || s.prefix.toLowerCase().indexOf(term.toLowerCase()) !== -1)
        );
    }   
    
    public filterBoards(term: string): void {
        this.board_list = this.source_board_list.filter((s) => 
            s.name.toLowerCase().indexOf(term.toLowerCase()) !== -1
        );
    }       

    public selectBoard(event: any, formGroup: any, dataItem:any, isNew: boolean): void {
        this.taskDropDownInstance.loading = true;
//console.log(dataItem);
        const params = {
            id: event,
            only_act: 1,
            filters: {}
          };
      
        this.boardService.getBoard(params)
        .subscribe(
            results => {
                if(results['success']) {
                    this.taskDropDownInstance.data = results['activities']; 
                    formGroup.controls['act_id'].setValue(0);
                    this.taskDropDownInstance.reset();      
                    formGroup.valid;             
                    formGroup.dirty;
                    
                    this.source_task_list = results['activities'];
                  //  formGroup.removeControl('task_list');
                  //  formGroup.addControl('task_list', new FormControl(this.formBuilder.array(results['activities']), []));
                    if(isNew) {
                        this.task_list = results['activities'];
                    } else {
                        let row = this.timeGridInstance.data['data'].find(({ id }) => id === dataItem.id);
                        if(row) {
                            row['task_list'] = results['activities'];
                        }
                    }
                  //  formGroup.controls['task_list'].setValue(results['activities']);
                }
                this.taskDropDownInstance.loading = false;
            },
            error => {
                this.taskDropDownInstance.loading = false;
            }
        );

    }

    public addHandler({ sender }): void {
        this.closeEditor(sender);

        let model = new TimeTask();
        let currentDate = new Date();
        if(this.filters['date']) {
            let date_parts = this.filters['date'].split('/');                    
            currentDate = new Date(date_parts[2], date_parts[1] - 1, date_parts[0]);
        }

        this.source_task_list = [];

        model.due_date = currentDate;
        model.board_id = this.board_id;
        model.act_id = this.act_id;

        sender.addRow(this.createFormGroup(model));

    //    sender.addRow(this.formGroup);
    }    

    public createFormGroup(dataItem: any): FormGroup {

   //     console.log('createFormGroup -> dataItem');
   //     console.log(dataItem);

        let form =  this.formBuilder.group({
            'id': dataItem.id,
         //   'board_id': [dataItem.board_id, Validators.required],
       //     'act_id': [dataItem.act_id, Validators.required],
            'due_date': [dataItem.due_date, Validators.required],
            'duration': [dataItem.duration, [Validators.required, Validators.min(0.25)]],
            'description': dataItem.description,
       //     'task_list': this.formBuilder.array(dataItem.task_list)
            
            /*,
            'UnitsInStock': [dataItem.UnitsInStock, Validators.compose([Validators.required, Validators.pattern('^[0-9]{1,3}')])],
            'Discontinued': dataItem.Discontinued*/
        });


        if(!this.board_id) {
            form.addControl('board_id', new FormControl(dataItem.board_id, [Validators.required, Validators.min(1)]));
            form.controls["board_id"].updateValueAndValidity();
        }

        if(!this.act_id) {
            form.addControl('act_id', new FormControl(dataItem.act_id));
            //form.addControl('act_id', new FormControl(dataItem.act_id, [Validators.required, Validators.min(1)]));
            form.controls["act_id"].updateValueAndValidity();

            form.addControl('task_list', new FormControl(this.formBuilder.array(dataItem.task_list)));
            form.controls["task_list"].updateValueAndValidity();            
        }

      //  form.addControl('employee', new FormControl(dataItem.employee, [Validators.required, Validators.min(1)]));

        return form;

    }

    public cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }): void {
        if (!isEdited) {

            if(dataItem['due_date']){
                if(typeof dataItem['due_date'].getMonth !== 'function') {
                    let date_parts =dataItem['due_date'].split('/');                    
                    var mydate = new Date(date_parts[2], date_parts[1] - 1, date_parts[0]);

                    dataItem['due_date'] = mydate;//this.intl.parseDate(mydate);

                }
            }

            sender.editCell(rowIndex, columnIndex, this.createFormGroup(dataItem));
        }
    }



    public editHandler({ sender, rowIndex, dataItem }): void {
        this.closeEditor(sender);

        if(dataItem['due_date']){
            if(typeof dataItem['due_date'].getMonth !== 'function') {

                let date_parts =dataItem['due_date'].split('/');
                var mydate = new Date(date_parts[2], date_parts[1] - 1, date_parts[0]);
                dataItem['due_date'] = mydate;//this.intl.parseDate(mydate);
            }
        }    

        if(dataItem['task_list']) {
            this.source_task_list = dataItem['task_list'];
        } else {
            this.source_task_list = [];
        }
        
        if(dataItem['description']) {
            dataItem['description'] = this.br2nl(dataItem['description']);
        }
//console.log('editHandler -> dataItem');
//console.log(dataItem);
        this.formGroup = this.createFormGroup(dataItem);

        this.editedRowIndex = rowIndex;

        sender.editRow(rowIndex, this.formGroup);
    }    


    public cellCloseHandler(args: any): void {
        const { formGroup, dataItem } = args;
//console.log('cellCloseHandler');
        if (!formGroup.valid) {
       //     console.log('cellCloseHandler -> not Valid');
             // prevent closing the edited cell if there are invalid values.
            args.preventDefault();
        } else if (formGroup.dirty) {
       //     console.log('cellCloseHandler -> Valid');
         //   this.editService.assignValues(dataItem, formGroup.value);
        //    this.editService.update(dataItem);
        }
    }


    private closeEditor(grid, rowIndex = this.editedRowIndex): void {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

    public cancelHandler({ sender, rowIndex }): void {
        sender.closeRow(rowIndex);
    }    

    public saveHandler({ sender, rowIndex, formGroup, isNew }): void { // console.log(formGroup.value);
        if (formGroup.valid) {
            const params = formGroup.value;

            //let params = Object.assign({}, formGroup.value);

            params['due_date'] = this.intl.formatDate(params['due_date'], 'dd/MM/yyyy');

            if(this.board_id){
                params['board_id'] = this.board_id;
            }

            if(this.act_id){
                params['act_id'] = this.act_id;
            } 
            
            if(params['task_list']) {
                delete params['task_list'];
            }

      //      console.log(params);            

            this.timeService.saveTimeItem(params).subscribe(
                results => {
                    if(results['success']) {
                        
                    //    console.log(this.task_list);
                        let item = formGroup.value;
                        item['id'] = results['id'];

                    //    this.save(item, isNew );
                        sender.closeRow(rowIndex);
                        this.page();
                    }
                    
                },
                () => {
                    
                }
            );


        //    sender.closeRow(rowIndex);

        } else {
            formGroup.dirty;
        }
    }    

    public removeHandler(event:any): void {      
        const dialog: DialogRef = this.dialogService.open({
            title: 'Remove Hours',
            content: 'Are you sure?',
            height: 170,
            minWidth: 350,
            actions: [
                { text: 'Cancel', primary: false },
                { text: 'OK', primary: true }
            ]
        });
    
        dialog.result.subscribe((result) => {
            if (result instanceof DialogCloseResult) {
           //     console.log("close");
            } else {
               if (result['primary']) {


                this.timeService.removeTimeItem(event.dataItem.id)
                .subscribe(
                    results => {
                        if(results['success']) {
                            this.page();
                        }
                    });     
            
                }
            }
        });


    }

    public save(item: any, isNew: boolean): void {
        if (isNew) {
       
            item['task_list'] = this.task_list;
            this.timeGridInstance.data['data'].push(item);
        } else {
            /*let obj = this.timeGridInstance.data['data'].find(
                row => row.id === item['id']);  
             */   
 // console.log(item);          
            Object.assign(
                this.timeGridInstance.data['data'].find(({ id }) => id === item.id),
                item
            );

        }
    }

    onCellClick(event: any): void {
        if(event.dataItem.act_source_id && (event.column.field == 'act_id' || event.column.field == 'act_key') && event.dataItem.board_sid != 4){
            const current_act_after = this.globalDataService.getActivityAfterSubmitAction(this.types);
            const setting = {
                from_module: 'grid',
                from_mode: this.mode,
                from_component: 'time_grid'
            }
            this.commonService.notifyOther({action: 'editActivityDialog', afterSubmitAction: current_act_after, act_id: event.dataItem.act_source_id, setting: setting});
        }
      }

    ngOnChanges(changes: SimpleChanges) {//console.log(changes);
        if((changes["run_filter"] && !changes["run_filter"].isFirstChange())             
            /*||
            (changes["source_id"] && !changes["source_id"].isFirstChange()) ||
            (changes["status_ids"] && !changes["status_ids"].isFirstChange())*/
        ) {
            this.state.skip = 0;
         /*   this.selectedGroupBy = '';
            this.groupable = false;*/
            this.page();
        }        
        if (changes['run_grid_action']) {

        }
    } 


    public ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();     
        }
    }

    public ngOnInit() {   
        
        this.source_board_list = this.board_list.slice();
        this.board_list2 = this.board_list.slice();

        this.filters['from_page'] = this.from_page;
        this.filters['board_id'] = this.board_id;
        this.filters['act_id'] = this.act_id;

      /*  if(this.def_take > 150 ) {
            this.def_take = 50;
        }*/
        this.state = {
            skip: this.def_skip,
            take: this.def_take,
            sort: this.def_sort
        }     
                
        this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('action')) {
                if (res.action == 'hideIndicator' + this.controller) {
                    if(this.from_page != 'time-report') {
                        setTimeout(() => {
                            this.sumHours = 0;
                            this.timeGridInstance.data['data'].forEach((row) => {
                                this.sumHours += row['duration'];
                            });  
                        }, 150);                   
                    }
                }  else if(res.action == 'loadTime') {
                    this.page();
                }
               
            } 
        });           

        this.page();
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
    /*    this.selectedGroupBy = '';
        this.groupable = false;     */   
        this.emitState.emit(this.state);
        this.service.query(state, this.controller, this.filters);
    }    

    page(): void {
        this.view = this.service; 
        this.view.subscribe(res => {
            this.gridData = res;
            if(res && res.data) {
                this.loadedData = res.data;
                this.allCount = res.total;
                this.groupByFieldSelect(this.selectedGroupBy);
            }
           // console.log(this.gridData);
        });

        this.service.query(this.state, this.controller, this.filters);
    }    

    public showSuccessNotification(): void {
        this.notificationService.show({
            content: 'הפעולה בוצעה בהצלחה!',
            cssClass: 'button-notification',
            animation: { type: 'slide', duration: 400 },
            position: { horizontal: 'center', vertical: 'bottom' },
            type: { style: 'success', icon: true },
            closable: true
        });
    }

    br2nl(str:any): string {
        return str.replace(/<br\s*\/?>/mg,"\n");
    }      
    
  }