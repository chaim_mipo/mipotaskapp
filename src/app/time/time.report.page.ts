import { Component, OnInit, OnDestroy } from '@angular/core';
import { BoardService, GlobalDataService, TimeService } from '@app/_services';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IntlService } from '@progress/kendo-angular-intl';
import { parseDate } from '@telerik/kendo-intl';


@Component({
  selector: 'time-report-page',
  templateUrl: './time.report.page.html'
})
export class TimeReportPage implements OnInit, OnDestroy {
    public pageLoaded: boolean = false;
    public date_format: string = "dd/MM/yyyy";
    public run_filter: boolean = false;
    public customer_list = [];
    public board_list = [];
    public employee_list = [];
    public selected_filter = {
      searchTerm: '',
      from_date: this.intl.formatDate(new Date(), this.date_format),
      to_date: this.intl.formatDate(new Date(), this.date_format),
      cust_ids: '',
      board_ids: '',
      employee_ids: ''
    };  

    public form_filter = {
      searchTerm: '',
      from_date: new Date(),
      to_date: new Date(),
      cust_ids: '',
      board_ids: '',
      employee_ids: ''
    };     

    public def_take: number = 25;
    public def_skip: number = 0;
    public def_sort = [];


    public grid_params = {
      def_take: this.def_take,
      def_skip: this.def_skip,
      def_sort: this.def_sort
    }
    public filterForm: FormGroup;
    public open_time_stat: boolean = false;

    public search_placeholder: string = 'Task Key';
    public filtering: boolean = false;
    public stat_loaded: boolean = false;
    public stat_rows = [];
    public stat_total: number = 0;
    private subscription: Subscription;
    private navigationSubscription: Subscription;    

    constructor(private router: Router, 
      private location: Location,
      private actRoute: ActivatedRoute,
      private boardService: BoardService,
      private fb: FormBuilder,
      private intl: IntlService,
      public globalDataService: GlobalDataService,
      private timeService: TimeService
    ) { }

    toggleStat(): void {
      this.open_time_stat = !this.open_time_stat;

      if(this.open_time_stat) {
        this.stat_loaded = false;

        let cloneFilters = Object.assign({}, this.selected_filter);
        cloneFilters['from_page'] = 'stat';

        let queryString = Object.keys(cloneFilters).map(key => key + '=' + cloneFilters[key]).join('&');
        this.timeService.getStat(queryString)
        .subscribe(
            results => {
              if(results['data']) {

                this.stat_rows = results['data']['rows'];
                this.stat_total = results['data']['total'];
              }
              this.stat_loaded = true;
            },
            () => {
              this.stat_loaded = true;
            }
        );        
      }

    }

    createForm(): void { 
      this.filterForm = this.fb.group({
          searchTerm : this.form_filter.hasOwnProperty('searchTerm') && this.form_filter['searchTerm'] ? this.form_filter['searchTerm'] : '',
          from_date : this.form_filter.hasOwnProperty('from_date') && this.form_filter['from_date'] ? this.form_filter['from_date'] : '',
          to_date : this.form_filter.hasOwnProperty('to_date') && this.form_filter['to_date'] ? this.form_filter['to_date'] : '',
          cust_ids: this.form_filter.hasOwnProperty('cust_ids') && this.form_filter['cust_ids'] ? this.form_filter['cust_ids'] : '',
          board_ids: this.form_filter.hasOwnProperty('board_ids') && this.form_filter['board_ids'] ? this.form_filter['board_ids'] : '',
          employee_ids: this.form_filter.hasOwnProperty('employee_ids') && this.form_filter['employee_ids'] ? this.form_filter['employee_ids'] : ''          
      });

    }

    public resetFilter(): void {
      this.filterForm.reset();
      this.runFilters();
    }

    public runFilters(): void {
      let data = this.filterForm.value;  

      if(data['from_date']) {
        if(typeof data['from_date'].getMonth === 'function') {
          data['from_date'] = this.intl.formatDate(new Date(data['from_date']), this.date_format);
        }
      } else {
        data['from_date'] = '';
      }


      if(data['to_date']) {
        if(typeof data['to_date'].getMonth === 'function') {
          data['to_date'] = this.intl.formatDate(new Date(data['to_date']), this.date_format);
        }
      } else {
        data['to_date'] = '';
      }


      this.filterPage(data);
    }

    ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }  
        
    ngOnInit() {
        this.navigationSubscription = this.actRoute.params.subscribe(params => {

            for (let key in this.selected_filter) {               

              if (params.hasOwnProperty(key) && params[key]) {
                  let val = params[key];
                  let val_ = params[key];
                  if(key == 'from_date' || key == 'to_date') {
                    val = parseDate(params[key], this.date_format); // new Date(params[key]);    
                    val_ = this.intl.formatDate(new Date(val), this.date_format);                    
                  }
                  this.selected_filter[key] = val_;
                  this.form_filter[key] = val;                  
              }
            }  
    
            for (let key in this.grid_params) {               

            if (params.hasOwnProperty(key) && params[key]) {
                let val = parseInt(params[key]);
                
                this.grid_params[key] = val;
                this[key] = val
            }
            }
            
            if (params.hasOwnProperty('dir') && (params['dir'] == 'asc' || params['dir'] == 'desc') && params.hasOwnProperty('field') && params['field']) {
            //    this.grid_params.def_sort.push({dir: params['dir'], field: params['field']});
                this.def_sort.push({dir: params['dir'], field: params['field']});
            }                         
        });


      const params = {
          is_template: 0,
          status_id: 4,
          addParams: 'getEmployees'           
      };

      this.boardService.getBoardTemplates(params)
      .subscribe(
          results => {
            if(results['success']) {
              this.board_list = results['board_templates'];
              this.employee_list = results['employee_list'];
              this.customer_list = results['customer_list'];
              
            }
            this.pageLoaded = true;
          },
          () => {
            this.pageLoaded = true;
          }
      );

      this.createForm();
    }
  
      stateGrid(event: any): void {
        this.grid_params.def_skip = event.skip;
        this.grid_params.def_take = event.take;

        if(event.sort && event.sort[0] && event.sort[0].dir) {
          this.grid_params.def_sort = [];
          this.grid_params.def_sort.push(event.sort[0]);
        } else {
            this.grid_params.def_sort = [];
        }        

        this.filterPage(this.selected_filter, true);
      }
       
      
     /* filtered(event: string){//console.log('filtered');
        this.filtering = false;
      }*/

      filterPage(data: any, new_state?:boolean): void {

          let params = HelpersFuncs.filterPage(this.grid_params, this.selected_filter, data, new_state);
          if(!new_state) {
            this.run_filter = !this.run_filter;
            this.filtering = true;
          }

          let new_url = this.router.serializeUrl(this.router.createUrlTree(['/time-report', params]));          
          this.location.replaceState(new_url);             
      }

    public addHandler() {

    }

    public addTime() {

    }
}