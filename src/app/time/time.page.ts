import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService, BoardService } from '@app/_services';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';
import { FormBuilder } from '@angular/forms';
import { IntlService } from '@progress/kendo-angular-intl';
import { parseDate } from '@telerik/kendo-intl';
import { addDays } from '@progress/kendo-date-math';


@Component({
  selector: 'time-page',
  templateUrl: './time.page.html'
})
export class TimePage implements OnInit, OnDestroy {
    public pageLoaded: boolean = false;
    public run_filter = false;
    public board_list = [];
    public filterForm;
    public date_format = "dd/MM/yyyy";

    public selected_filter = {
      date: this.intl.formatDate(new Date(), this.date_format)
    };  

    public form_filter = {
      date: new Date()
    };      

    public def_take = 1000;
    public def_skip = 0;
    public def_sort = [];

    public grid_params = {
      def_take: this.def_take,
      def_skip: this.def_skip,
      def_sort: this.def_sort
    }

    public filtering = false;
    private subscription: Subscription;
    private navigationSubscription: Subscription;    

    constructor(private router: Router, 
      private location: Location,
      private actRoute: ActivatedRoute,
      private boardService: BoardService,
      private fb: FormBuilder,
      private intl: IntlService
    ) { }


    public resetFilter() {
      this.filterForm.reset();
      this.runFilters();
    }

    public runFilters() {
      let data = this.filterForm.value;  

      if(data['date']) {
        if(typeof data['date'].getMonth === 'function') {
          data['date'] = this.intl.formatDate(new Date(data['date']), this.date_format);
        }
      } else {
        data['date'] = '';
      }

      this.filterPage(data);
    }

    public clickNav(mode: number) { 
      let value;
      if(mode == 1 || mode == -1) {
        value = this.form_filter.date;
        value = addDays(value, mode);
      } else {
        value = new Date();
      }

      this.form_filter.date = value;
      this.dateChange(value, 'date');
    }
 

    dateChange(event:any, field:any) { 
      let data = {};
      let val = event ? this.intl.formatDate(event, this.date_format) : '';

      if(val) {
        data[field] = val;
      }

      this.selected_filter[field] = val;
      this.filterPage(data);
  }    

    ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }  
        
    ngOnInit() {
        this.navigationSubscription = this.actRoute.params.subscribe(params => {

            for (let key in this.selected_filter) {               

              if (params.hasOwnProperty(key) && params[key]) {

                  let val = params[key];
                  let val_ = params[key];
                  if(key == 'date' ) {
                    val = parseDate(params[key], this.date_format); // new Date(params[key]);    
                    val_ = this.intl.formatDate(new Date(val), this.date_format);                    
                  }
                  this.selected_filter[key] = val_;
                  this.form_filter[key] = val                  
              }
            }  
            
            for (let key in this.grid_params) {               

            if (params.hasOwnProperty(key) && params[key]) {
                let val = parseInt(params[key]);
                
                this.grid_params[key] = val;
                this[key] = val
            }
            }
            
            if (params.hasOwnProperty('dir') && (params['dir'] == 'asc' || params['dir'] == 'desc') && params.hasOwnProperty('field') && params['field']) {
            //    this.grid_params.def_sort.push({dir: params['dir'], field: params['field']});
                this.def_sort.push({dir: params['dir'], field: params['field']});
            }                         
        });


      let params = {
          is_template: 0,
          status_id: 4           
      };

      this.boardService.getBoardTemplates(params)
      .subscribe(
          results => {
            if(results['success']) {
              this.board_list = results['board_templates'];
            }
            this.pageLoaded = true;
          },
          error => {
            this.pageLoaded = true;
          }
      );

    }
  
      stateGrid(event: any) {
        this.grid_params.def_skip = event.skip;
        this.grid_params.def_take = event.take;

        if(event.sort && event.sort[0] && event.sort[0].dir) {
          this.grid_params.def_sort = [];
          this.grid_params.def_sort.push(event.sort[0]);
        } else {
            this.grid_params.def_sort = [];
        }        

        this.filterPage(this.selected_filter, true);
      }
       
      
      filtered(event){//console.log('filtered');
        this.filtering = false;
      }

      filterPage(data: any, new_state?:boolean) {

          let params = HelpersFuncs.filterPage(this.grid_params, this.selected_filter, data, new_state);

          if(!new_state) {
            this.run_filter = !this.run_filter;
            this.filtering = true;
          }

          let new_url = this.router.serializeUrl(this.router.createUrlTree(['/time', params]));          
          this.location.replaceState(new_url);             
      }

    public addHandler() {

    }

    public addTime() {

    }
}