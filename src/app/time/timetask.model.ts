export class TimeTask {
    public id: number;
    public board_id: number = 0;
    public act_id: number = 0;
    public due_date: Date;
    public duration: number = 0;
    public description = '';
    public employee: number = 0;
    public task_list: any = [];
}