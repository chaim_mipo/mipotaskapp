import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'] // app.component.scss | app.component.rtl.scss
})
export class AppComponent implements OnInit  {
  
  constructor(translate: TranslateService) {
    translate.addLangs(['en','he']);
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang(environment.lang);
     // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use(environment.lang);
  }

 
  public ngOnInit() {


  }
 
}
