﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService, ActivityService } from '@app/_services';

@Component({ 
    selector: 'home-page',
    templateUrl: 'home.component.html' 
})
export class HomeComponent implements OnInit, OnDestroy {

    public status_list = [];
    public show_meetings_and_talks: boolean = false;

    constructor(
        private commonService: CommonService,
        private activityService: ActivityService
    ) {

    }

    ngOnInit() {
        this.activityService.tableSetting()
        .subscribe(
            results => {
                this.status_list = results['status_list'];
            });
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
    }

    onButtonActivityClick(afterSubmitAction, def_type) {
        let setting = {
            from_module: 'grid',
            from_mode: 'widget'
        };
        this.commonService.notifyOther({action: 'newActivityDialog', afterSubmitAction: afterSubmitAction, def_type: def_type, setting: setting});
    }
}