import { Component, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService, CommonService, SiteService, GlobalDataService } from '@app/_services';
import { User } from '@app/_models';
import { DialogService, DialogCloseResult } from '@progress/kendo-angular-dialog';
import { environment } from '@environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { MessageContentComponent } from '@app/_components/message-content/message.content.component';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { LocationStrategy } from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { RoutingState } from '@app/_models/routing.state';
import { ActivityFormComponent } from '../activity/activity.form.component';
import { Location } from '@angular/common';
import { DeviceDetectorService } from 'ngx-device-detector';
import { TranslateService } from '@ngx-translate/core';
import { clientVersion } from '@app/_helpers/global.params';

@Component({
  selector: 'main-page',
  templateUrl: './main.page.html'
})
export class MainPage implements OnInit, OnDestroy  {
  @ViewChild('siteGlobalSearch', {static: false}) siteGlobalSearch;
  private subscription: Subscription;   
  private navigationSubscription: Subscription;  
  currentUser: User = null;
  activityDialog: any;
  protected serverInt;
  protected serverIntTime = 1000*60*2; //60  
  protected intCounter = 0;
  public user_name;
  public is_manager = 0;
  public is_zakiyan = 0;   
  public showSavingMask: boolean = false;
  public fullScreenMask: boolean = false; 
  messageBoxDialog;  
  moduleTitle = ''; 
  public moduleManualTitle: boolean = false;
  public showMenu: boolean = false;
  public showBread: boolean = true;
  current_page = '';
  current_page_id = 0;  
  previousUrlSlug = '';
  private act_dialog_title: string = '';
  globalSearchData: Observable<any>;

  public left_menu_items: any[];  

  //private current_url_params;
  private general_labels;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private commonService: CommonService,
    private dialogService: DialogService,
    private siteService: SiteService,
    public globalDataService: GlobalDataService,
    private cookieService: CookieService,
    private activatedRoute: ActivatedRoute,
    private url: LocationStrategy, 
    private titleService: Title,
    private renderer: Renderer2,
    routingState: RoutingState,
    private location: Location,
    private domSanitizer: DomSanitizer,
    private deviceService: DeviceDetectorService,
    private translateService:TranslateService
  ) {
    this.translateService.get(['GENERAL']).subscribe((data:any)=> {

   this.navigationSubscription = this.router.events.subscribe((e: any) => {      
      let current_title = '';//this.titleService.getTitle();
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) { 
        let child = this.activatedRoute.firstChild;
        while (child.firstChild) {
          child = child.firstChild;
        }
        if (child.snapshot.data['title']) {
          current_title = child.snapshot.data['title'];
        }
        let fTitle = environment.name;
        if(current_title){
            fTitle += ' - ' + this.general_labels[current_title];
        }
        this.titleService.setTitle(fTitle);       

        

    /*    child.params.subscribe(params => { 
          this.current_url_params = params;
        });*/
      }
    });
    this.general_labels = data['GENERAL'];
      
          
  }); 
      routingState.loadRouting();
      this.titleService.setTitle(environment.name);

  }

  trustHtml(html) {
    return this.domSanitizer.bypassSecurityTrustHtml(html);
  }

  goToSearchPage() {
    setTimeout(() => {   
      this.siteGlobalSearch.blur();
    }, 300);    
    this.router.navigate(['/search/', {q: this.siteGlobalSearch.text}]);    
    this.siteGlobalSearch.value = '';
    this.siteGlobalSearch.text = '';      
    this.siteGlobalSearch.reset();    
  }  

  public globalSearchSelect(id, mode) {// console.log(this.siteGlobalSearch);
    setTimeout(() => {   
      this.siteGlobalSearch.value = '';
      this.siteGlobalSearch.reset();
    }, 500);
    this.router.navigate([mode+'/'+id]);

  }

  public globalSearchEnter(event, id, mode) {
    console.log('globalSearchEnter');
    if(event.keyCode == 13) {
      this.globalSearchSelect(id, mode);
    }
  }  

  onMainMenuSelect({ item }): void {
    if (!item.items) {
      if(item.path == 'logout') {
        this.logout();
      } else if(item.path) {
        this.router.navigate([ item.path ]);
      }
    }
  }

  ngOnDestroy() { 
    if(this.subscription){
      this.subscription.unsubscribe();
    } 

    if(this.navigationSubscription){
      this.navigationSubscription.unsubscribe();
    } 
  }

  logout() {
    clearInterval(this.serverInt);
    if(this.subscription){
      this.subscription.unsubscribe();
    } 

    if(this.navigationSubscription){
      this.navigationSubscription.unsubscribe();
    }     
       
    this.authenticationService.logout();
    this.currentUser = null;
    this.router.navigate(['/login']);
    
  }


  public ngOnInit() {

    this.translateService.get(['ACTIVITY.ADD_ACTIVITY', 'GENERAL.LOGOUT']).subscribe((data:any)=> {
      this.act_dialog_title = data['ACTIVITY.ADD_ACTIVITY'];

      this.left_menu_items = [
        {
            text: '',
            photo: '',
            items: [
              { text: data['GENERAL.LOGOUT'], path: 'logout'},
              { text: 'version: ' + clientVersion, path: ''}
            ]
        }
      ];      
    });   

 /*   if(!HelpersFuncs.isMobile()) {

      setTimeout(() => {
        this.globalSearchData = this.siteGlobalSearch.filterChange.asObservable()
          .debounceTime(400)
          .distinctUntilChanged()
          .do(() => this.siteGlobalSearch.loading = true)
          .switchMap(value =>
              value.length > 1
              ? this.siteService.globalSearch(value)
                .do(() => this.siteGlobalSearch.loading = false)
                .map(value => value['rows'])
              : Observable.of([]).do(() => this.siteGlobalSearch.loading = false)
            );
      }, 500);
    } else {

    }   */

    this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('action')) {
        if(res.action == 'setTitle') {
          this.moduleTitle = res.title;
          this.titleService.setTitle(this.moduleTitle);
          this.moduleManualTitle = true;
        } else if(res.action == 'setUserInfo') {  //   console.log('res.action == setUserInfo -> setUserName()');     
          this.setUserName(res.user_name);
          this.getUserInfo(false);
        } else if(res.action == 'showSavingMask') {
          this.showSavingMask = true;
        } else if(res.action == 'hideSavingMask') {
          this.showSavingMask = false;
        } else if(res.action == 'showFullScreenMask') {
          this.fullScreenMask = true;
        } else if(res.action == 'hideFullScreenMask') {
          this.fullScreenMask = false;
        } else if(res.action == 'newActivityDialog') {          
          this.callActivityDialog(res.afterSubmitAction, res.def_type, 0, res.setting);
        } else if(res.action == 'editActivityDialog') {
          this.callActivityDialog(res.afterSubmitAction, '', res.act_id, res.setting);
        } else if(res.action == 'closeActivityDialog') {
          if(this.activityDialog) {
            this.activityDialog.close();
          }
        }
      }
    });
    this.loadUser();

    this.serverInit();
  }


  callActivityDialog(afterSubmitAction:string, def_type:any, act_id:any, setting:any) {
    
    let dialogSetting = {
      //   title: act_id ? 'Activity - ' + act_id : 'Add Activity',
         minWidth: 1024,
         width: 1024,
         content: ActivityFormComponent,
         autoFocusedElement: 'activitySummary'
     };

     if(this.deviceService.isMobile()) {
      dialogSetting['minWidth'] = 0;
      delete dialogSetting['width'];
     }

    this.activityDialog = this.dialogService.open(dialogSetting);
//console.log(setting);
    var activity = this.activityDialog.content.instance;
    activity.title = act_id ? ' '  : this.act_dialog_title;
    activity.my_parent = this;
    activity.this_dialog = this.activityDialog;
    activity.afterSubmitAction = afterSubmitAction;
    if(def_type) {
        activity.def_type = def_type;
    }

    if(act_id) {
        activity.act_id = act_id;
    }  
    
    if(setting) {
        if(setting['board_id']){
            activity.board_id = setting['board_id'];
        }

        if(setting['model']){
            activity.model = setting['model'];
        }
        
        if(setting['model_id']){
            activity.model_id = setting['model_id'];
        }    
        
        if(setting['from_module']){
            activity.from_module = setting['from_module'];
        }   
        
        if(setting['from_mode']){
          activity.from_mode = setting['from_mode'];
        } 
        
        if(setting['from_component']){
          activity.from_component = setting['from_component'];
        }                 
        
        if(setting['models_connect']){
            activity.models_connect = setting['models_connect'];
        }  
        
        if(setting['is_template']){
            activity.is_template = setting['is_template'];
        }     
        
        
        if(act_id && setting['board_id'] && setting['from_module'] == 'board' && !setting['is_template'] && !setting['static_url']) {
          let params = {};
          let url_str = '/boards/' + setting['board_id'];
          if(setting['prefix']) {
            url_str += '/' + setting['prefix'];
          } else {
            params['actId'] = act_id;            
          }

          let new_url = this.router.serializeUrl(this.router.createUrlTree([url_str, params]));                  
          this.location.replaceState(new_url);   
          
        }        
        
    }
    // activity.prop_id = prop_id;

    this.activityDialog.result.subscribe((result) => { 
      if (result instanceof DialogCloseResult) {
        
        if(act_id && setting['board_id'] && setting['from_module'] == 'board' && !setting['is_template']) {
          let params = {};

          let new_url = this.router.serializeUrl(this.router.createUrlTree(['/boards/' + setting['board_id'], params]));          
          this.location.replaceState(new_url); 
        }        
        

      } else {
        console.log("action", result);
      }
    });    
    
}  



  protected getUserInfo(goHome) {
 
    this.siteService.getCurrentUser()
          .subscribe(
              results => {
                  this.globalDataService.current_user = results['user'];
              //     if(!HelpersFuncs.getLocalBranchId()){
              /*  if(results['data']['user'] && results['data']['user']['branch_id']) {
                  HelpersFuncs.setLocalBranchId(results['data']['user']['branch_id']);
                }*/
                //  }

                  let currentRealtorUser_obj = this.authenticationService.currentUserValue;
                  if(currentRealtorUser_obj) { //console.log(currentRealtorUser_obj);
                    this.currentUser = currentRealtorUser_obj;
                    if (currentRealtorUser_obj.hasOwnProperty('access_token')) {
                      let access_token = currentRealtorUser_obj.access_token
                      localStorage.setItem(environment.userKey, JSON.stringify({
                          access_token: access_token, 
                          name: this.globalDataService.current_user.name, 
                          first_name: this.globalDataService.current_user.first_name, 
                          last_name: this.globalDataService.current_user.last_name, 
                          mobile: this.globalDataService.current_user.mobile,
                          photo: this.globalDataService.current_user.photo,
                          color: this.globalDataService.current_user.color
                        }));          
                        
                      const short_name = this.globalDataService.current_user.first_name.charAt(0) + this.globalDataService.current_user.last_name.charAt(0);
                      this.setUserName(this.globalDataService.current_user.name, this.globalDataService.current_user.photo, this.globalDataService.current_user.color, short_name );
                    }                      
                  }
                  if(goHome) {    
                    setTimeout(() => {    
                      document.location.href = 'index.html';                    
                    },1000); 
                  } else if(this.fullScreenMask) {
                    this.fullScreenMask = false;
                  }
                }
          );
  }

  protected loadUser() {
    let localObj = this.authenticationService.currentUserValue;
    this.currentUser = localObj;
    if(localObj) {  
    //  console.log('loadUser() -> setUserName()');    
    //  this.setUserName(localObj.name);
      this.getUserInfo(false);
    }
  }

  protected setUserName(name: string, photo?: string, color?: string, short?: string) { 
    this.user_name = name;
    this.left_menu_items[0].text = name;
    if(photo) { //console.log(photo);
      this.left_menu_items[0].text = '<img class="main_menu_photo" src="' + photo + '" /><span class="main_menu_name">' + this.left_menu_items[0].text + '</span>';
    } else {
      let style = '';
      let add_class = '';
      if(color) {
        style = ' style="background-color: #'+ color + '"';
        add_class = 'bg_'+color;

      }
      this.left_menu_items[0].text = '<span class="login_no_photo_design ' + add_class + '" ' + style + '>' + (short ? short : this.left_menu_items[0].text.charAt(0)) + '</span><span class="main_menu_name">' + this.left_menu_items[0].text + '</span>';
    }
  }

  protected serverInit() {
    this.serverInt = setInterval(() => {
      let localObj = this.authenticationService.currentUserValue;
      if(localObj) {  
        this.intCounter++;    
        this.siteService.getServerInit(this.intCounter)
              .subscribe(
                  results => {
                    if(results['do']) {
                      if(results['action'] == 'reload') {
                        window.location.reload(); 
                      } else if(results['action'] == 'logout') {
                        this.logout();
                      } else if(results['action'] == 'reload_user') {
                        this.getUserInfo(false);
                      } else if(results['action'] == 'show_action' && results['event_id']) {
                        //this.edit_act(results.event_id, -1, 0, results.mode);
                      } 
                    }
                    //console.log(this.cookieService.get('lic_alert'));
                    if(results['messagebox'] && results['msg']) {

                      const cookieExists: boolean = this.cookieService.check('lic_alert');
                      if(!cookieExists){
                        let now_date = new Date();
                        
                        //let plus_hour = new Date(now_date.getTime() + 1 * 60 * 60 * 1000);
                        let plus_2hour = new Date(now_date.getTime() + 2 * 60 * 60 * 1000);
                        //let plus_minute = new Date(now_date.getTime() + 1 * 60 * 1000);
                        //let midnight = new Date(now_date.getFullYear(), now_date.getMonth(), now_date.getDate(), 23, 59, 59);
                        this.cookieService.set( 'lic_alert', 'SET', plus_2hour );

                        this.showMessageDialog(results['msg'], 'התראה');                                               
                      }
	
                    }                    
                  }
              );
      }
    }, this.serverIntTime);
  }  

  showMessageDialog(msg, title?) {
    if(!title) {
      title = 'הודעה';
    }
    this.messageBoxDialog = this.dialogService.open({
      title: title,
      content: MessageContentComponent
    });

    var messageBox = this.messageBoxDialog.content.instance;
    messageBox.message = msg;
    messageBox.parentDialog = this.messageBoxDialog;
  }  
}
