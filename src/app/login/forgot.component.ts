import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { NotificationService } from '@progress/kendo-angular-notification';
import { AlertService, AuthenticationService, CommonService } from '@app/_services';

@Component({templateUrl: 'forgot.component.html'})

export class ForgotComponent implements OnInit {
    @ViewChild('usernameVar', {static: false}) usernameVar:ElementRef;
    @Input() my_parent;
    @Input() this_dialog;  
    @Input() defPhone = '';  
    forgotForm: FormGroup;
    public forgotFormSubmitted = false;
    constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService){

    }

    ngOnInit() {
        this.forgotForm = this.formBuilder.group({
            username: [this.defPhone, Validators.required]
        });
        setTimeout(() => {   
            this.usernameVar.nativeElement.focus();
        }, 50);
    }

    public closeDialog() {
        if(this.this_dialog) {
            this.this_dialog.close();
        }
    }    

    public onforgotFormSubmit() {
        this.forgotFormSubmitted = true;
        let mobile = this.forgotForm.controls['username'].value;
        this.authenticationService.forgot(mobile)
            .subscribe(
                data => { 
                    this.forgotFormSubmitted = false;
                    if(data['success']) {  
                        this.my_parent.setPhoneAndCloseDialog(mobile);
                    }
                })
    }    
}