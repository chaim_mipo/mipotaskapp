﻿import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

import { NotificationService } from '@progress/kendo-angular-notification';
import { AlertService, AuthenticationService, CommonService } from '@app/_services';
import { RecaptchaComponent } from 'ng-recaptcha';
import { DialogService } from '@progress/kendo-angular-dialog';
import { ForgotComponent } from './forgot.component';
import { environment } from '@environments/environment';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    @ViewChild('captchaRef', {static: false}) captchaRef:RecaptchaComponent;
    @ViewChild('mobileVar', {static: false}) el:ElementRef;
    @ViewChild('passwordVar', {static: false}) passwordVar:ElementRef;
    
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    loading_forgot = false;
    forgot_submitted = false;  
    forgotDialog;  

    public captchaSiteKey = '6LekobcUAAAAABagh423iyK9Ww6boXuQlm2lRwn_';
    cpt = '';
    testCpt = false;    

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private notificationService: NotificationService,
        private commonService: CommonService,
        private titleService: Title,
        private dialogService: DialogService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/']);
        } else {

        }
    }

    resolved(event) {
        this.cpt = event;
      //  this.errorMsg = '';
      //  this.error = false;    
     /*   if(this.loginForm.valid) {
          this.login();
        }*/
      }    

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.titleService.setTitle(environment.name + ' - Login');
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value,  this.cpt)
            .pipe(first())
            .subscribe(
                data => { 
                    this.loading = false;
                    if(data['success']) {
                        this.router.navigate([this.returnUrl]);
                      /*  setTimeout(() => {
                            this.commonService.notifyOther({action: 'setUserInfo', user_name: data.name});
                        }, 1000);*/
                    } else if(data['msg']) {
                        this.notificationService.show({
                            content: data['msg'],
                            animation: { type: 'fade', duration: 200 },
                            position: { horizontal: 'center', vertical: 'bottom' },
                            type: { style: 'error', icon: false },
                            hideAfter: 2800
                        });

                        this.testCpt = data['check_cpt'];
                        if(this.testCpt && this.captchaRef) {
                          this.captchaRef.reset();
                        }                        
                    }
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }


      

    public showForgotDialog() {
        this.forgotDialog = this.dialogService.open({
            title: 'Reset password',
            minWidth: 320,
            width: 360,
            content: ForgotComponent
        });
   
        var dlg = this.forgotDialog.content.instance;
        dlg.my_parent = this;
        dlg.this_dialog = this.forgotDialog;  
        dlg.defPhone = this.loginForm.controls['username'].value
    }  
    
    public setPhoneAndCloseDialog(mobile) {        
        if(this.forgotDialog) {
            this.forgotDialog.close();
        }
        this.loginForm.controls['username'].setValue(mobile);
        this.loginForm.controls['password'].setValue('');
        this.passwordVar.nativeElement.focus();
    }
}
