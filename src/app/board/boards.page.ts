import { Component } from '@angular/core';

@Component({
  selector: 'boards-page',
  template: `<board-templates def_title="{{'BOARD.BOARDS' | translate}}" [is_template]="0" [slug]="'boards'" create_title="{{'BOARD.ADD_BOARD' | translate}}"></board-templates>`
})
export class BoardsPage  {

}