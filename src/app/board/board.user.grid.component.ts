import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { State } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { CategoriesService } from '@app/_services/northwind.service';
import { CommonService, BoardService } from '@app/_services/index';
import { Subscription } from 'rxjs/Subscription';
import { DialogService, DialogRef, DialogCloseResult } from '@progress/kendo-angular-dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'board-user-grid',
    providers: [CategoriesService],
    templateUrl: './board.user.grid.component.html',
    styleUrls: ['./board.component.scss']
  })

  export class BoardUserGridComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    @Input() def_take = 10;
    @Input() def_skip = 0;  
    @Input() def_sort = []; 
    @Input() board_id = 0;  

    
    public loadingIndicator: boolean = true;
    public filters = {};
    public view: Observable<GridDataResult>;

    public state: State = {
        skip: this.def_skip,
        take: this.def_take,
        sort: this.def_sort
    };
    private controller = 'board/user-table';
    public take_list = [10, 15, 25, 50, 100, 150];    

    public constructor(    
        private service: CategoriesService, 
        private commonService: CommonService,
        private dialogService: DialogService,
        private boardService: BoardService, private translateService:TranslateService
    ) {  }    


    public ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }


    public ngOnInit() {
                
        if(this.def_take > 150 ) {
            this.def_take = 50;
        }
        this.state = {
            skip: this.def_skip,
            take: this.def_take,
            sort: this.def_sort
        }  

        this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('action')) {
              if(res.action == 'loadUsers') {
                this.loadUsers();
              } else if (res.action == 'hideIndicator' + this.controller) {
                this.loadingIndicator = false;
              }
            }
          });  

        this.loadUsers();
    }

    toggleReceiveUpdates(event: any, id:number) {
      let value = event ? 1 : 0;
      this.boardService.toggleBoardUserUpdate(this.board_id, id, value)
      .subscribe(
          results => {                                                      

            if(results['success']) {
              
            }

          });           
    }

    removeBoardUser(id: number) {
      this.translateService.get(['BOARD.REMOVE_USER', 'GENERAL']).subscribe((data:any)=> {
        const dialog: DialogRef = this.dialogService.open({
            title: data['BOARD.REMOVE_USER'],
            content: data['GENERAL']['ARE_YOU_SURE'],
            height: 170,
            minWidth: 350,
            actions: [
                { text: data['GENERAL']['CANCEL'], primary: false },
                { text: data['GENERAL']['OK'], primary: true }
            ]
        });
    
        dialog.result.subscribe((result) => {
                if (result instanceof DialogCloseResult) {
            //     console.log("close");
                } else {
                if (result['primary']) {
            
                    this.boardService.deleteBoardUser(this.board_id, id)
                    .subscribe(
                        results => {                                                      
  
                          if(results['success']) {
                            this.loadUsers();
                          }
  
                        });        
                       
                }
            
            }
        })
      });
    }
       

    loadUsers() { 
        this.filters['board_id'] = this.board_id;         
        this.page();      
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
 //       console.log(this.state);
        this.loadingIndicator = true;
        this.service.query(state, this.controller, this.filters);
      }    

    page() {
        this.loadingIndicator = true;
        this.view = this.service;
        this.service.query(this.state, this.controller, this.filters);
      }    
     
  }