import { Component, OnInit, OnDestroy, Input, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonService, BoardService, GlobalDataService } from '@app/_services';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { BoardTemplateFormComponent } from './board.template.form.component';
import { DialogService, DialogRef, DialogCloseResult } from '@progress/kendo-angular-dialog';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'board-templates',
  templateUrl: './board.templates.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardTemplatesComponent implements OnInit, OnDestroy  {
    @ViewChild('boardTemplatesStart',{static: false}) boardTemplatesStart: any;
    private subscription: Subscription;
    private navigationSubscription: Subscription;  
    @Input() def_title: string = 'Templates';
    @Input() is_template: number = 1;
    @Input() slug: string = 'tboards';
    @Input() create_title: string = 'New Template';
    public orientation: string = 'horizontal';
    def_sub_title = '';
    public title = '';//this.def_title;
    public sub_title = this.def_sub_title;
    public board_id;
    public module: string = '';
    templates_loader: boolean = false;
    newBoardDialog: DialogRef;
    board_templates = [];
    board_templates_source = [];
    main_templates_loader = false;
    public search_placeholder = '';
    public select_placeholder = '';

    public searchBoardText: string = '';
    public status_id: number = 1;
    public statusList: Array<any> = [];

    private general_labels;
    private board_labels;
    public readonly_mode: boolean = false;

    constructor(
        private commonService: CommonService,
        private dialogService: DialogService, 
        private router: Router, 
        private actRoute: ActivatedRoute, 
        private location: Location, 
        private boardService: BoardService,
        public globalDataService: GlobalDataService,
        private cdr: ChangeDetectorRef,
        private translateService:TranslateService 
    ) {
     //   this.title = this.def_title;
        this.sub_title = this.def_sub_title;
        if(HelpersFuncs.isMobile()) {
            this.orientation = 'vertical';
        }

        
     }

    filterBoardSelect(event: any) {
        this.board_templates = this.board_templates_source.filter((s) => s.name.toLowerCase().indexOf(event.toLowerCase()) !== -1);
    }

    selectBoard(event: any) {
        if(event) {
            this.router.navigate([this.slug + '/' + event ]);
        } else {
            this.router.navigate([this.slug]);
        }
    }     

     

    newBoardTemplate() {
        this.newBoardDialog = this.dialogService.open({
            //title: this.is_template ? 'New Template' : 'New Board',
            title: this.is_template ? this.board_labels['NEW_TEMPLATE'] : this.board_labels['NEW_BOARD'],
            minWidth: 360,
            content: BoardTemplateFormComponent
        });
   
        var temp = this.newBoardDialog.content.instance;
        temp.my_parent = this;
        temp.this_dialog = this.newBoardDialog;  
        temp.is_template = this.is_template;   
    }

    ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();  
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();  
        }
    }

    onDeactivate(componentReference) {
        this.title = '';//this.def_title;
        this.sub_title = this.def_sub_title;
        this.board_id = '';
        this.module = '';
        this.readonly_mode = false;
        this.cdr.detectChanges();
    }

    onActivate(componentReference) {
        
        componentReference.emitBoardId.subscribe((data) => {
            this.board_id = parseInt(data['board_id']);
            this.module = data['module'];
            this.readonly_mode = false;
            this.cdr.detectChanges();
       //     console.log('goToTemp from onActivate');
            this.goToTemp(this.board_id);
            
         })   
         
                
      //  componentReference.anyFunction();
     }

    searchBoardKey(event) {
        let term = event.target.value;
    //    console.log(this.board_templates_source);
        this.board_templates = this.board_templates_source.filter((s) => s.name.toLowerCase().indexOf(term.toLowerCase()) !== -1);
    }

    ngOnInit() {    

     //   this.title = this.def_title;  
        this.sub_title = this.def_sub_title;     
        this.module = '';     
        this.getBoardTemplates();   

        this.translateService.get(['GENERAL','BOARD']).subscribe((data:any)=> {
            this.general_labels = data['GENERAL'];
            this.board_labels = data['BOARD'];


            if(this.is_template) {
                this.search_placeholder = this.board_labels['SEARCH_TEMPLATE'];
                this.select_placeholder = this.board_labels['SELECT_TEMPLATE'];
            } else {
                this.search_placeholder = this.board_labels['SEARCH_BOARD'];
                this.select_placeholder = this.board_labels['SELECT_BOARD'];
            }

            this.statusList = [
                {
                    name: this.board_labels['STATUS_ACTIVE'],
                    id: 1
                },
                {
                    name: this.board_labels['STATUS_ARCHIVE'],
                    id: 2
                },
                {
                    name: this.board_labels['STATUS_ALL'],
                    id: -1
                }                
        
            ];            

        });  
        
        this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('action')) {
              if(res.action == 'changeBoardTitle') {
              //  this.title = res.title;
                this.setTempNameOnly(res.board_id, res.title);
              } else if(res.action == 'reloadBoardTemplates') {
                this.getBoardTemplates();
              } else if(res.action == 'toggleReadonlyMode') {
                  this.readonly_mode = res.show;
              }
            }
          });         
    }

    setTempNameOnly(id, name) { //console.log(id + '  ' + name);
        var obj = this.board_templates.find(
            temp => temp.id === parseInt(id));  
            
        if(obj) {
            obj.name = name;
            this.title = obj.name;
        }
    }    
 

    addBoardTemplate(item, goto, after_id?) {
        if(after_id) {
            let obj = this.board_templates.find(
                temp => temp.id === after_id);  
    
            if(obj) {
                const index: number = this.board_templates.indexOf(obj);
                if (index !== -1) {
                    this.board_templates.splice(index + 1, 0, item);
                }
            }
        } else {
            this.board_templates.unshift(item);
        }

        if(goto) {
            this.router.navigate(['/' + this.slug + '/' + item['id']]);  
        }
    }

    copyTemp(id, is_temp, name, attr_id?) {


        const dialog: DialogRef = this.dialogService.open({
            title: this.general_labels['DUPLICATE'],//'Duplicate',
            content: this.general_labels['ARE_YOU_SURE'],
            height: 170,
            minWidth: 350,
            actions: [
                { text: this.general_labels['CANCEL'], primary: false },
                { text: this.general_labels['OK'], primary: true }
            ]
        });
    
        dialog.result.subscribe((result) => {
            if (result instanceof DialogCloseResult) {
           //     console.log("close");
            } else {
               if (result['primary']) {

                let params = {
                    id: id,
                    is_template: this.is_template, //is_temp,
                    name: name
                };

                if(attr_id) {
                    params['attr_id'] = attr_id;
                }

                this.main_templates_loader = true;
                this.boardService.copyBoard(params)
                .subscribe(
                    results => {
                        this.main_templates_loader = false;
                        if(results['success']) {
                            if(results['item']) {
                                if(is_temp) {
                                    this.addBoardTemplate(results['item'], true, id);
                                } else {
                                    this.addBoardTemplate(results['item'], true, '');
                                }
                            }
                        }
                    });     
            
                }
            }
        });
    }

    toggleFav(id: number, type?: number) {
        let obj = this.board_templates.find(
            temp => temp.id === id);  

        if(obj) {
        
            this.boardService.toggleFav(id)
            .subscribe(
                results => {
                    if(results['success']) {
                        let new_fav = 1 -obj.fav;
                        obj.fav = new_fav;

                        if(type == 1) {
                           let index = this.board_templates.indexOf(obj);
                            this.board_templates.splice(index,1);
                            this.board_templates.unshift(obj);
                        
                           setTimeout(() => {   
                                this.boardTemplatesStart.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                            }, 0);  
                        }
                    }
                }
            );

        }
    }

    editTemp(id: number) {
        let obj = this.board_templates.find(
            temp => temp.id === id);  

        if(obj) {
            
            this.newBoardDialog = this.dialogService.open({
                title: this.is_template ? this.board_labels['EDIT_TEMPLATE'] : this.board_labels['EDIT_BOARD'],
                minWidth: 560,
                content: BoardTemplateFormComponent
            });
       
            let temp = this.newBoardDialog.content.instance;
            temp.my_parent = this;
            temp.this_dialog = this.newBoardDialog;  
            temp.temp_name = obj.name; 
            temp.temp_id = obj.id;
            temp.is_template = this.is_template;  
        }
    }


    setTempName(id, name, attr?) {
        var obj = this.board_templates.find(
            temp => temp.id === id);  

        if(obj) {
            obj.name = name;
            this.title = obj.name;

            obj['attr'] = attr;
            this.sub_title = attr;

            if (obj.hasOwnProperty('attr')) { 
                this.sub_title = obj.attr;
            }
        }
    }

    goToTemp(id: number, with_scroll?:boolean) { //console.log('goToTemp');
        var obj = this.board_templates.find(
            temp => temp.id === id);  

        if(obj) {
            this.title = obj.name;

            if(this.module == 'setting') {
                this.title += ' / ' + this.board_labels['SETTING'];
            }

            if (obj.hasOwnProperty('attr')) { 
                this.sub_title = obj.attr;
            }            

            if(with_scroll) { //console.log('goToTemp -> with_scroll');
                setTimeout(() => {  
                        const itemToScrollTo = document.getElementById('board_item_' + id);
                        if (itemToScrollTo) { //console.log('goToTemp -> with_scroll -> itemToScrollTo');
                            
                            itemToScrollTo.scrollIntoView(true);
                        }

                    }, 20);
                            
            }
         /*   let index = this.board_templates.indexOf(obj);
            this.board_templates.splice(index,1);
            this.board_templates.unshift(obj);*/
           
          /*  setTimeout(() => {   
                this.boardTemplatesStart.nativeElement.scrollIntoView();
            }, 0);     */       
        }
    }

    statusChange(event) {
        this.searchBoardText = '';
        this.getBoardTemplates();
    }

    getBoardTemplates() {  
        let params = {
            is_template: this.is_template            
        };

        if(!this.is_template){
            params['status_id'] = this.status_id;
        }

        this.templates_loader = true;
        this.boardService.getBoardTemplates(params)
        .subscribe(
            results => {
                this.templates_loader = false;
                if(results['success']) {
                    this.board_templates_source = results['board_templates'];   
                    this.board_templates = this.board_templates_source.slice(); 

                    if(this.board_templates && this.board_templates.length && this.board_id) { //console.log('goToTemp from getBoardTemplates');
                        this.goToTemp(this.board_id, true);
                    }  else {

                        if(results['user_type'] == 9) {
                            if(results['board_id']) {
                                this.router.navigate(['boards/' + results['board_id'] ]);
                            }
                        }
                    }
                }
            });          
    }
}