import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonService, BoardService } from '@app/_services/index';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'board-user-form',
  templateUrl: 'board.user.form.component.html'
})

export class BoardUserFormComponent implements OnInit{
    @Input() board_id = 0;
    @Input() this_dialog;
    @Input() my_parent;


    newUserForm;
    public ready = false;
    newUserFormubmitted = false;
    public user_list = [];
    public role_list = [];
    user_list_source = [];
    

    newUserFormErrors = {
        user_id : '',
        role : ''
    };

    validationMessages = {};     

    constructor(private fb: FormBuilder, private boardService: BoardService, private router: Router, private translateService:TranslateService) {

        this.translateService.get(['GENERAL']).subscribe((data:any)=> {
            this.validationMessages = {
                'user_id': {
                    'required': data['GENERAL']['REQUIRED_FIELD']
                },
                'role': {
                    'required': data['GENERAL']['REQUIRED_FIELD']
                }
            };             
          });          
    }

    ngOnInit() {
            
        this.boardService.getBoardUserInit(this.board_id)
        .subscribe(
            results => {
                this.ready = true;
                if(results['success']) {
                    this.user_list_source = results['user_list'];  
                    this.role_list = results['role_list'];   
                    
                    this.user_list = this.user_list_source.slice();
                }
            });

        this.createForm(); 
    }

    closeDialog() {
        if(this.this_dialog) {
            this.this_dialog.close();
        }
    }

    onNewUserFormSubmit() {
        let user_id = this.newUserForm.controls['user_id'] ? this.newUserForm.controls['user_id'].value : '';
        let role = this.newUserForm.controls['role'] ? this.newUserForm.controls['role'].value : '';
        
        this.newUserFormubmitted = true;
        let params = {
            board_id: this.board_id,
            user_id: user_id,
            role: role
        };                
                
        this.boardService.createBoardUser(params)
        .subscribe(
            results => {
                this.newUserFormubmitted = false;
                
                if(results['success']) {
                    if(results['item']){
                        this.my_parent.addUser(results['item']);
                    }
                    this.closeDialog();
                    
                } else {
                    this.closeDialog();
                }
            });
        
       
    }

    createForm() {
        this.newUserForm = this.fb.group({
            user_id: ['', Validators.required ],
            role: ['', Validators.required ]
        });               

    
        this.newUserForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
  
        this.onValueChanged();    

      }   
      
      onValueChanged(data?: any) {
        if (!this.newUserForm) { return; }
        const form = this.newUserForm;
        for (const field in this.newUserFormErrors) {
          // clear previous error message (if any)
          this.newUserFormErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.newUserFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      }    
      
      userClientFilter(value) { //console.log(value);
        this.user_list = this.user_list_source.filter((s) => s.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }      
}