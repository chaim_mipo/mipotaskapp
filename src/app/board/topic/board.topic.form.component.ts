import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonService, BoardService } from '@app/_services/index';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'board-topic-form',
  templateUrl: 'board.topic.form.component.html'
})

export class BoardTopicFormComponent implements OnInit{
    @Input() board_id: number = 0;
    @Input() this_dialog: any;
    @Input() my_parent: any;
    @Input() mode: string = 'topic';

    newTopicForm: FormGroup;
    public ready: boolean = false;
    public newTopicFormubmitted: boolean = false;   

    newTopicFormErrors = {
        title : ''
    };

    validationMessages = {};     

    constructor(private fb: FormBuilder, private boardService: BoardService, private router: Router, private commonService: CommonService, private translateService:TranslateService) {
        this.translateService.get(['GENERAL']).subscribe((data:any)=> {
            this.validationMessages = {
                'title': {
                    'required': data['GENERAL']['REQUIRED_FIELD']
                }
            };             
          });          
    }

    ngOnInit() {
        this.ready = true;  
        this.createForm(); 
    }

    closeDialog(): void {
        if(this.this_dialog) {
            this.this_dialog.close();
        }
    }

    public onNewTopicFormSubmit(): void {
        const title = this.newTopicForm.controls['title'] ? this.newTopicForm.controls['title'].value : '';
        
        this.newTopicFormubmitted = true;
        const params = {
            board_id: this.board_id,
            title: title
        };                
                
        this.boardService.createBoardAttr(params, this.mode)
        .subscribe(
            results => {
                this.afterCreate(results);
            });               
    }

    private afterCreate(results: {}): void {
        this.newTopicFormubmitted = false;
                
        if(results['success']) {
            const act: string = this.mode == 'topic' ? 'reloadBoardTopics' : 'reloadBoardSprints';
            this.commonService.notifyOther({action: act});
            this.closeDialog();
        } else {
            this.closeDialog();
        }
    }

    createForm(): void {
        this.newTopicForm = this.fb.group({
            title: ['', Validators.required ]
        });               

    
        this.newTopicForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
        this.onValueChanged();    

      }   
      
      onValueChanged(data?: any) {
        if (!this.newTopicForm) { return; }
        const form = this.newTopicForm;
        for (const field in this.newTopicFormErrors) {
          // clear previous error message (if any)
          this.newTopicFormErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.newTopicFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      }         
}