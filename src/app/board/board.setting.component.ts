import { Component, OnInit, OnDestroy, Output, EventEmitter  } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { BoardService, CommonService } from '@app/_services/index';
import { DialogService, DialogCloseResult, DialogRef } from '@progress/kendo-angular-dialog';
import { NotificationService } from '@progress/kendo-angular-notification';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BoardTopicFormComponent } from './topic/board.topic.form.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'board-setting',
  templateUrl: './board.setting.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardSettingComponent implements OnInit, OnDestroy  {
  private navigationSubscription: Subscription;  
  private fSubscription: Subscription; 
  public loaded = false;
  public board_id: number;
  public status_list = [];
  public max_status_count: number = 8;
  public new_status_creating: boolean = false;
  public boardForm: FormGroup;
  public is_rtl: boolean;
  public newTopicDialog: DialogRef;
  public newSprintDialog: DialogRef;
  @Output()emitBoardId: EventEmitter<any> = new EventEmitter<any>();

  boardFormErrors = {
    name : '',
    board_key : ''
  };

  validationMessages = {}; 

  constructor(
    private fb: FormBuilder,
    private router: Router, 
    private actRoute: ActivatedRoute, 
    private commonService: CommonService,
    private boardService: BoardService, 
    private notificationService: NotificationService,
    private dialogService: DialogService,
    private translateService:TranslateService) {

      this.translateService.get(['GENERAL']).subscribe((data:any)=> {
        this.validationMessages = {
            'name': {
                'required': data['GENERAL']['REQUIRED_FIELD']
            },
            'board_key': {
                'required': data['GENERAL']['REQUIRED_FIELD']
            }
        };             
      });        

  }

  createForm() {
    this.boardForm = this.fb.group({
        name: ['', Validators.required ],
        board_key: ['', Validators.required ],
        is_rtl: this.is_rtl,
        hide_in_table: '',
        status_id: 1
    });               


    this.fSubscription = this.boardForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();    

  } 

  onValueChanged(data?: any) {
    if (!this.boardForm) { return; }
    const form = this.boardForm;
    for (const field in this.boardFormErrors) {
      // clear previous error message (if any)
      this.boardFormErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.boardFormErrors[field] += messages[key] + ' ';
        }
      }
    }
  }   

  ngOnDestroy() {
    if(this.navigationSubscription) {
        this.navigationSubscription.unsubscribe();  
    }

    if(this.fSubscription) {
      this.fSubscription.unsubscribe();  
    } 
   
    if(this.newTopicDialog) {
      this.newTopicDialog.close();
    }

    if(this.newSprintDialog) {
      this.newSprintDialog.close();
    }
   
  }  

  ngOnInit() {
    this.navigationSubscription = this.actRoute.paramMap.subscribe(params => { //console.log(params);
        if(params.get('id')) {
            this.board_id = parseInt(params.get('id'));
            if(/^\d+$/.test(String(this.board_id)) && this.board_id > 0) {
              //console.log(this.board_id);
              this.createForm();
              this.emitBoardId.emit({board_id: this.board_id, module: 'setting'});
              this.loadSetting();
            } else {
              this.router.navigate(['boards']);
         }

        } else {
            this.router.navigate(['boards']);
        }                        
    }); 
  }

  loadSetting() {
    this.boardService.getBoardSetting(this.board_id)
    .subscribe(
        results => {                                                      

          if(results['success']) {
            this.status_list = results['status_list'];

            this.status_list.forEach(function(obj) { 
              obj.edit = 0;  
              obj.prev_name = obj.name;              
            });
            this.loaded = true;
            this.boardForm.controls['name'].setValue(results['name']);
            this.boardForm.controls['board_key'].setValue(results['board_key']);
            this.boardForm.controls['is_rtl'].setValue(results['is_rtl']);
            this.boardForm.controls['hide_in_table'].setValue(results['hide_in_table']);
            this.boardForm.controls['status_id'].setValue(results['status_id']);

          } else {            
              this.router.navigate(['boards']);            
          }

        });     
  }

  public autoSave(field: any, event: any) {
    
    if((this.boardForm.controls[field] && this.boardForm.controls[field].valid)) {

      let val = this.boardForm.controls[field].value;
      if(field == 'hide_in_table') {
        val = event;
      }
   //   console.log(val);

      this.boardService.saveBoardSetting(this.board_id, field, val)
      .subscribe(
          results => {                                                      
  
            if(results['success']) {
              if(field == 'name') {
                this.commonService.notifyOther({action: 'changeBoardTitle', title: val, board_id: this.board_id});
                this.emitBoardId.emit({board_id: this.board_id, module: 'setting'});
              } else if(field == 'status_id' ) {
                this.commonService.notifyOther({action: 'reloadBoardTemplates'});
              }
            } else if(results['error']) {
              this.boardFormErrors[field] = results['error'];
            }
  
          });    

    }
  }

  public beginStatusEdit(id: number) {
    var obj = this.status_list.find(
      status => status.id === id);  
    
      if(obj) {
        obj.prev_name = obj.name;   
        obj.edit = 1;
      }
  }

  public cancelStatusEdit(id: number) {
    var obj = this.status_list.find(
      status => status.id === id);  
    
      if(obj) {
        obj.edit = 0;
        obj.name = obj.prev_name;   
      }    
  }

  public saveStatusEdit(id: number) {
    var obj = this.status_list.find(
      status => status.id === id);  
    
      if(obj && obj.name.length > 0) {
        obj.edit = 0;           
        if(obj.name != obj.prev_name) {
          obj.prev_name = obj.name;
          this.boardService.saveBoardStatusName(this.board_id, id, obj.name)
          .subscribe(
              results => {                                                      
      
                if(results['success']) {
                      
                } else if(results['name']) {
                  obj.prev_name = results['name'];   
                  obj.name = results['name'];   
                }
      
              });         
          }
      }     
  }

  public newStatusBoard() {
    if(this.status_list.length <= this.max_status_count) {


      this.new_status_creating = true;
      this.boardService.saveBoardStatusName(this.board_id, '', 'New Status')
      .subscribe(
          results => {                                                      
            this.new_status_creating = false;
            if(results['success']) {
              let new_status = {
                id: results['id'],
                name: results['name'],
                prev_name: results['name'],
                edit: 1
              }                  

              this.status_list.push(new_status);
            } 
  
          });  
 
    }
  }

  public addTopicClick(): void {

    this.translateService.get(['BOARD.ADD_TOPIC']).subscribe((data:any)=> {

        this.newTopicDialog = this.dialogService.open({
          title: data['BOARD.ADD_TOPIC'],
          minWidth: 360,
          content: BoardTopicFormComponent
      });

      var temp = this.newTopicDialog.content.instance;
      temp.my_parent = this;
      temp.this_dialog = this.newTopicDialog;  
      temp.board_id = this.board_id;      

    });
  }

  public addSprintClick(): void {

    this.translateService.get(['BOARD.ADD_SPRINT']).subscribe((data:any)=> {

        this.newSprintDialog = this.dialogService.open({
          title: data['BOARD.ADD_SPRINT'],
          minWidth: 360,
          content: BoardTopicFormComponent
      });

      var temp = this.newSprintDialog.content.instance;
      temp.my_parent = this;
      temp.this_dialog = this.newSprintDialog;  
      temp.board_id = this.board_id;  
      temp.mode = 'sprint';    

    });
  }  

  public removeStatus(id: number) {
    this.translateService.get(['BOARD.REMOVE_BOARD_STATUS', 'GENERAL']).subscribe((data:any)=> {
      const dialog: DialogRef = this.dialogService.open({
        title: data['BOARD.REMOVE_BOARD_STATUS'],
        content: data['GENERAL']['ARE_YOU_SURE'],
        height: 170,
        minWidth: 350,
        actions: [
            { text: data['GENERAL']['CANCEL'], primary: false },
            { text: data['GENERAL']['OK'], primary: true }
        ]
      });

      dialog.result.subscribe((result) => {
        if (result instanceof DialogCloseResult) {
      //     console.log("close");
        } else {
          if (result['primary']) {

            this.boardService.removeBoardStatus(this.board_id, id)
                  .subscribe(
                      results => {                                                      

                        if(results['success']) {
                          
                          var obj = this.status_list.find(
                            status => status.id === id);                         

                          const index: number = this.status_list.indexOf(obj);
                          if (index !== -1) {
                              this.status_list.splice(index, 1);
                          }                         

                        } else if(results['error']) {
                          this.notificationService.show({
                            content: results['error'],
                            animation: { type: 'fade', duration: 200 },
                            position: { horizontal: 'center', vertical: 'bottom' },
                            type: { style: 'error', icon: false },
                            hideAfter: 2800
                        });
                        }

                      });
          }
        }
        

      });    
    });
  }
}