import { Component } from '@angular/core';

@Component({
  selector: 'board-templates-page',
  template: `<board-templates def_title="{{'BOARD.TEMPLATES' | translate}}" create_title="{{'BOARD.ADD_TEMPLATE' | translate}}"></board-templates>`
})
export class BoardTemplatesPage  {

}