import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'board-template-item-page',
  template: `<app-board [id]="board_id" [is_template]="'1'"></app-board>`,
  styleUrls: ['./board.component.scss']
})
export class BoardTemplateItemPage implements OnInit, OnDestroy {
    private navigationSubscription: Subscription;  
    board_id;
    @Output()emitBoardId: EventEmitter<any> = new EventEmitter<any>();
    constructor(private router: Router, private actRoute: ActivatedRoute, private location: Location){}

    ngOnDestroy() {
        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();  
        }
    }

    ngOnInit() {
        this.navigationSubscription = this.actRoute.paramMap.subscribe(params => {
            if(params.get('id')) {
                this.board_id = params.get('id');
                if(/^\d+$/.test(String(this.board_id)) && this.board_id > 0) {
                  //console.log(this.board_id);
                  this.emitBoardId.emit({board_id: this.board_id, module: 'tboard'});
                } else {
                  this.router.navigate(['tboards']);
             }

            } else {
                this.router.navigate(['tboards']);
            }                        
        });        
    }
}