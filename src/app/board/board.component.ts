import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';
import { CommonService, BoardService, ActivityService, GlobalDataService } from '@app/_services';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DialogService, DialogRef, DialogCloseResult } from '@progress/kendo-angular-dialog';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { BoardUserFormComponent } from './board.user.form.component';
import { debounceTime, map, distinctUntilChanged, filter } from "rxjs/operators";
import { TranslateService } from '@ngx-translate/core';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';
import { ActivatedRoute } from '@angular/router';
import { orderBy } from '@progress/kendo-data-query';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit, OnDestroy {
  @ViewChild('filterSearchTerm', {static: false, read: ElementRef}) filterSearchTerm: ElementRef;   
  private subscription: Subscription;
  private subscriptionEvent: Subscription;  
  private navigationSubscription: Subscription;  
  
  @Input() id = 1;
  @Input() selected_users = [];
  @Input() search_term: string = '';
  @Input() selected_topics: string = '';
  @Input() selected_sprint: number = null;
  @Input() selected_release: number = null;
  @Input() selected_types: string = '';
  @Input() is_template = 0;

  @Input() status_list = [];/*[
    {id: 1, name: 'TO DO'},
    {id: 2, name: 'IN PROGRESS'},
    {id: 3, name: 'DONE'}
  ]*/

  public board_loader: boolean = false;
  public title: string = "לוח";
  public sub_title: string = "";
  public activities_status_1: boardActivityItem[] = [];
  public activities_status_2: boardActivityItem[] = [];
  public activities_status_3: boardActivityItem[] = [];
  public activities_status_4: boardActivityItem[] = [];
  public activities_status_5: boardActivityItem[] = [];
  public activities_status_6: boardActivityItem[] = [];
  public activities_status_7: boardActivityItem[] = [];
  public activities_status_8: boardActivityItem[] = [];
  private maxBoardColumns: number = 8;
  can_remove_act: number = 0;
  public board_item_actions = [];

  public users = [];
  public topic_list: {id: number, name: string}[] = [];
  public sprint_list: {id: number, name: string}[] = [];
  public release_list: {id: number, name: string}[] = [];
  public type_list: {id: number, name: string}[] = [];
  newUserDialog: DialogRef;

  public minWidthOfColumn: number = 274;
  public isDragging: boolean = true;
  public max_show_users: number = 6;
  public show_all_users: boolean = false;

  public board_readonly = 0;
  public read_only_button: boolean = false;
  public sort_by_mode = 0;
  public sort_by_dir = 1;
  public sorting: boolean = false;
  public types_info = {
    1: {name: 'Task', icon: 'k-i-check'},
    2: {name: 'Bug', icon: 'k-i-cancel'},
    3: {name: 'Feature', icon: 'k-i-gear'}
  }

  constructor(
    private router: Router, 
    private location: Location,  
    private activityService: ActivityService, 
    private commonService: CommonService, 
    private boardService: BoardService, 
    private dialogService: DialogService,
    public globalDataService: GlobalDataService,
    private translateService:TranslateService,
    private actRoute: ActivatedRoute
  ) {
   
   }


    ngAfterViewInit() {    
      if(!this.is_template) {
        this.subscriptionEvent = fromEvent(this.filterSearchTerm.nativeElement, 'keyup').pipe(
          map((event: any) => {
          return event.target.value;
          })
          ,filter(res => res.length >= 0)
          ,debounceTime(450)        
          ,distinctUntilChanged()
          ).subscribe((text: string) => {
          
          this.search_term = text;
          this.buildUrl();
          this.loadBoard(true, true);
          });        
      }
  }

  ngDestroy() {

  }

  readonlyModeToggle(): void {
    this.read_only_button = !this.read_only_button;
    this.isDragging = !this.read_only_button;

    if(this.read_only_button) {
      this.sort_by_mode = 0;
      this.sort_by_dir = 1;
      this.sorting = false;
    } else {
      this.loadBoard(true, true);
    }

    this.commonService.notifyOther({action: 'toggleReadonlyMode', show: this.read_only_button});
  }

  public clickOnSortByMode(mode: number): void {
    this.sort_by_dir = mode == this.sort_by_mode ? 0 - this.sort_by_dir : 1;
    this.sort_by_mode = mode;
    this.sortByField();
  }

  private sortByField(): void {
    let field: string = '';

    switch (this.sort_by_mode) {
      case 1:
        field = 'summary';
        break;
      case 2:
        field = 'full_date';//'update';
        break;
      case 3:
        field = 'id';
        break;                  
    }

    if(field) {
      this.sorting = true;
      for(let i = 1; i <= this.maxBoardColumns; i++) {
        let activities: boardActivityItem[] = this['activities_status_' + i];
        if(activities && activities.length) {
          if(field == 'update') {
            this['activities_status_' + i] = activities.sort((a, b) => {
              const aa = new Date(a.date);
              const bb = new Date(b.date);
              if (aa !== bb) {
                if (aa > bb) { return this.sort_by_dir; }
                if (aa < bb) { return -this.sort_by_dir; }
              } else {
                return 0;
              }
            });
          } else {
            this['activities_status_' + i] = orderBy(activities, [{ field: field, dir: this.sort_by_dir == 1 ? "asc" : "desc" }]);
          }
         
        }
      }

      setTimeout(() => {
        this.sorting = false;
      }, 300);

    }
  }

  toggleShowMoreUsers(): void {
    this.show_all_users = !this.show_all_users;
  }

  onBoardActivityControlClick(event, act_id: number, summary?: string): void {
  //  console.log(event, act_id);
    //delete
    if(event.id == 2) {


      this.translateService.get(['ACTIVITY.REMOVE_ACTIVITY', 'GENERAL']).subscribe((data:any)=> {

          const dialog: DialogRef = this.dialogService.open({
            title: data['ACTIVITY.REMOVE_ACTIVITY'],
            content: data['GENERAL']['ARE_YOU_SURE'],
            height: 170,
            minWidth: 350,
            actions: [
                { text: data['GENERAL']['CANCEL'], primary: false },
                { text: data['GENERAL']['OK'], primary: true }
            ]
        });

        dialog.result.subscribe((result) => {
            if (result instanceof DialogCloseResult) {
          //     console.log("close");
            } else {
              if (result['primary']) {
          

              let params = {};
              params['deleted'] = 1;

                const setting = {
              /*      from_module: "board",
                    from_mode: "page"*/
                };

                this.activityService.autoSave(act_id, params, setting)
                      .subscribe(
                          results => {                                                      

                            if(results['success']) {
                              this.loadBoard(true, false);
                            }

                          });
              }
            }
            

          });

      }); 
    } else if(event.id == 1) {

    }
  }

  private setMinWithByColumnsNumber() :void{
    switch(this.status_list.length){
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
        this.minWidthOfColumn = 274;
        break;
      case 6:
        this.minWidthOfColumn = 267;
        break;        
      case 7:
        this.minWidthOfColumn = 228;
        break;
      case 8:
        this.minWidthOfColumn = 200;
        break;                  
    }
  }

  ngOnInit() {

    if(HelpersFuncs.isMobile()) {
      this.minWidthOfColumn = 0;
      this.isDragging = false;
    } else {
      this.minWidthOfColumn = 270;
      this.isDragging = true;
    }

    this.navigationSubscription = this.actRoute.paramMap.subscribe(params => {
      this.read_only_button = false;
    });    

    this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('action')) {
        if(res.action == 'addActivityToBoard') {
          this.activities_status_1.push(res.new_item);
        } else if(res.action == 'refreshBoard') {
          this.loadBoard(false, true);
        }
        
      }
    });       


    this.loadBoard(true, false);
  }

  ngOnDestroy() {

    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.subscriptionEvent) {
      this.subscriptionEvent.unsubscribe();
    }    

    if(this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }    
  }

  private resetFilter(): void {
    this.search_term = '';
    this.selected_topics = '';
    this.selected_sprint = null;
    this.selected_release = null;
    this.selected_types = '';
    this.selected_users = [];
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes["id"] && !changes["id"].isFirstChange()) {
        this.resetFilter();
        this.loadBoard(true, false);
    }  
  }   

  loadBoard(with_loader: any, filter: any): void {
    const params = {
      id: this.id,
      filters: {}
    };   
    params['filters']['selected_users'] = this.selected_users;
    params['filters']['search_term'] = this.search_term;
    params['filters']['selected_topics'] = this.selected_topics;
    params['filters']['selected_sprint'] = this.selected_sprint;
    params['filters']['selected_release'] = this.selected_release;
    params['filters']['selected_types'] = this.selected_types;
    
    
    if(filter) {
      
      this.resetActivities();
    } else {
      this.resetActivities();
      this.status_list = [];
      this.users = [];
  //    this.selected_users = [];
    }


    if(with_loader) {
      this.board_loader = true;
    }

    /*if(this.read_only_button) {
      this.read_only_button = false;
      this.isDragging = !this.read_only_button;
      this.sort_by_mode = 0;
      this.sort_by_dir = 1;     
      this.commonService.notifyOther({action: 'toggleReadonlyMode', show: this.read_only_button}); 
    }*/

    this.boardService.getBoard(params)
    .subscribe(
        results => {
          this.board_loader = false;
          if(results['success']) {

            if(!filter) {
              this.title = results['title'];
              this.status_list = results['status_list'];
              this.users = results['users'];
              this.topic_list = results['topic_list'];
              this.sprint_list = results['sprint_list'];
              this.release_list = results['release_list'];
              this.type_list = results['type_list'];
              this.can_remove_act = results['can_remove_act'];
              this.board_readonly = results['board_readonly'];

              this.setMinWithByColumnsNumber();

              if(this.board_readonly) {
                this.isDragging = false;
              } else {
                if(!HelpersFuncs.isMobile()){
                  this.isDragging = true;
                }
              }

              this.show_all_users = false;
              if(this.can_remove_act) {
                this.translateService.get(['GENERAL.REMOVE']).subscribe((data:any)=> {
                  this.board_item_actions = [
                    {id: 2, 'name': data['GENERAL.REMOVE']}
                  ];
                });
              }

              let that = this;
//console.log(that.selected_users);
//console.log(that.users);
              if(this.users && this.users.length) {
                this.users.forEach(function(obj) { 
                  if(that.selected_users.length && that.selected_users.includes(obj.id)) {
                    obj.selected = 1;
                  } else {
                    obj.selected = 0;
                  }
                });
              }
            }

            if(results['activities']) {
              this.fillActivities(results['activities']);                        
            }
            
          } else {
            if(this.is_template) {
              this.router.navigate(['tboards']);
            } else {
              this.router.navigate(['boards']);
            }
          }
        }
      );
  }

  toggleUser(id: number): void {  //console.log(id); console.log(this.selected_users);
    const obj = this.users.find(
      user => user.id === id);    
   
    obj.selected = 1 - obj.selected;
    //console.log(obj.selected);
    if(obj.selected) {
      this.selected_users.push(id);
    } else {
        const index: number = this.selected_users.indexOf(id);
        //console.log(index);
        if (index !== -1) {
            this.selected_users.splice(index, 1);
        }       
    }

    this.buildUrl();
    this.loadBoard(true, true);
  }

  public changeTopic(event: any): void {    
    this.selected_topics = event;
    this.buildUrl();
    this.loadBoard(true, true);    
  }

  public changeSprint(event: any): void {    
    this.selected_sprint = event;
    this.buildUrl();
    this.loadBoard(true, true);    
  }  

  public changeRelease(event: any): void {    
    this.selected_release = event;
    this.buildUrl();
    this.loadBoard(true, true);    
  }    

  public changeType(event: any): void {    
    this.selected_types = event;
    this.buildUrl();
    this.loadBoard(true, true);    
  }  

  buildUrl(): void {
    
    const params = {};
    const selected_users = this.selected_users.join(',');
    if(selected_users) {
      params['selected_users'] = selected_users;
    }

    if(this.search_term) {
      params['search_term'] = this.search_term;
    }

    if(this.selected_topics) {
      params['selected_topics'] = this.selected_topics;
    }  

    if(this.selected_sprint) {
      params['selected_sprint'] = this.selected_sprint;
    }

    if(this.selected_release) {
      params['selected_release'] = this.selected_release;
    }    
    
    if(this.selected_types) {
      params['selected_types'] = this.selected_types;
    }      

    const new_url = this.router.serializeUrl(this.router.createUrlTree(['/boards/' + this.id, params]));          
    this.location.replaceState(new_url);      
  }


  fillActivities(activities: {[key: number]: boardActivityItem[]}[]): void {
    if(activities) {
      for(let i = 1; i <= this.maxBoardColumns; i++) {
        if(activities[i]) {
          this['activities_status_' + i] = activities[i];
        }
      }

      if(this.read_only_button && this.sort_by_mode) {
        this.sortByField();
      }

    }
  }

  resetActivities(): void {
    for(let i = 1; i <= this.maxBoardColumns; i++) {     
        this['activities_status_' + i] = [];     
    }    
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      if(event.previousIndex !== event.currentIndex) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
 // console.log(event);      
        let params = {
          act_id: event.container.data[event.currentIndex]['id'],
          new_pos: event.currentIndex,
          new_status: '',
          board_id: this.id          
        }

        this.changePosStatus(params);
      }
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);

      const params = {
        act_id: event.container.data[event.currentIndex]['id'],
        new_pos: event.currentIndex,
        new_status: event.container.id.replace('board_status_column_',''),
        board_id: this.id          
      }

      this.changePosStatus(params);
//console.log(event.container);
    }
  } 
  
  changePosStatus(params: {}): void {
    this.boardService.changeActivityPosStatus(params)
    .subscribe(
        () => {

        }
      );
  }

  newActivityToBoard(): void {
    const setting = {
      board_id: this.id,
      from_module: 'board',
      is_template: this.is_template
    }
    this.commonService.notifyOther({action: 'newActivityDialog', afterSubmitAction: "addActivityToBoard", def_type: "1", setting: setting});
  }
 
  openBoardActivity(event, act_id: any, prefix: any): void { //console.log(event.target.className);
    if(!this.board_readonly) {
      if(event.target.className != 'k-button' && event.target.className != 'k-icon k-i-more-horizontal' && event.target.className != 'k-item ng-star-inserted k-state-focused') {
        const setting = {
          board_id: this.id,
          from_module: 'board',
          is_template: this.is_template,
          prefix: prefix
        }    
        this.commonService.notifyOther({action: 'editActivityDialog', afterSubmitAction: '', act_id: act_id, setting: setting});
      }
    }
  }

  public addUserDialog(): void {

    this.translateService.get(['BOARD.ADD_USER']).subscribe((data:any)=> {

      this.newUserDialog = this.dialogService.open({
          title: data['BOARD.ADD_USER'],
          minWidth: 360,
          content: BoardUserFormComponent
      });

      const temp = this.newUserDialog.content.instance;
      temp.my_parent = this;
      temp.this_dialog = this.newUserDialog;  
      temp.board_id = this.id;  
    });
  }


  public addUser(item): void {
    item.selected = 0;
    this.users.push(item);
  }

}


interface boardActivityItem {
  id: number,
  summary: string,
  date?: string,
  prefix?: string,
  type?: string,
  update?: string,
  full_date?: number,
  time?: string,
  status_position?: number,
  user_id?: number,
  user_name?: string
  user_color?: string,
  priority?: {name?: string, icon?: string, color?: string}
}

