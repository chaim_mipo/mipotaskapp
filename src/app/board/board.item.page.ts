
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService, ActivityService } from '@app/_services';

@Component({
  selector: 'board-item-page',
  template: `<app-board 
                [id]="board_id" 
                [selected_topics]="selected_topics" 
                [selected_sprint]="selected_sprint"
                [selected_release]="selected_release"
                [selected_types]="selected_types" 
                [selected_users]="selected_users" 
                [search_term]="search_term"
            ></app-board>`,
  styleUrls: ['./board.component.scss']
})
export class BoardItemPage implements OnInit, OnDestroy {
    private navigationSubscription: Subscription;  
    public board_id: any;
    public selected_users = [];
    public selected_topics = '';
    public selected_sprint: number = null;
    public selected_release: number = null;
    public selected_types = '';
    public search_term = '';
    private actId: any;
    private actUrl: any = '';

    @Output()emitBoardId: EventEmitter<any> = new EventEmitter<any>();
    constructor(
        private router: Router, 
        private actRoute: ActivatedRoute, 
        private commonService: CommonService,
        private activityService: ActivityService
    ){}

    ngOnDestroy() {
        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();  
        }
    }

 
    ngOnInit() {
        this.navigationSubscription = this.actRoute.paramMap.subscribe(params => { //console.log(params);
            if(params.get('id')) {
                this.commonService.notifyOther({action: 'closeActivityDialog'});
                this.board_id = params.get('id');
                this.selected_users = params.get('selected_users') && params.get('selected_users').length ? params.get('selected_users').split(',').map(Number) : [];
                this.search_term = params.get('search_term') && params.get('search_term').length ? params.get('search_term') : '';
                this.selected_topics = params.get('selected_topics') && params.get('selected_topics').length ? params.get('selected_topics') : '';
                this.selected_types = params.get('selected_types') && params.get('selected_types').length ? params.get('selected_types') : '';
                this.selected_sprint = params.get('selected_sprint') && params.get('selected_sprint').length ? parseInt(params.get('selected_sprint')) : null;
                this.selected_release = params.get('selected_release') && params.get('selected_release').length ? parseInt(params.get('selected_release')) : null;
                
                this.actId = params.get('actId') && params.get('actId').length ? params.get('actId') : '';
            
                this.actUrl = params.get('actUrl');

                if(/^\d+$/.test(String(this.board_id)) && this.board_id > 0) {
                  //console.log(this.board_id);
                  this.emitBoardId.emit({board_id: this.board_id, module: 'board'});

                  if(/^\d+$/.test(String(this.actId)) && this.actId > 0) {
                    this.goToActivity(false);
                  } else if(this.actUrl && this.actUrl.length > 2) {
                    if(this.testUrl(this.actUrl)) {
                        // console.log(this.actUrl);
                        this.activityService.getIdUrl(this.actUrl, this.board_id)
                        .subscribe(   
                            results => {
                                if(results['success'] && results['act_id']) {
                                    this.actId = results['act_id'];
                                    this.goToActivity(true);
                                } else {
                                    this.router.navigate(['boards/' + this.board_id]);
                                    this.commonService.notifyOther({action: 'closeActivityDialog'});
                                }
                            }
                        );
                    } else {
                        this.router.navigate(['boards']);
                        this.commonService.notifyOther({action: 'closeActivityDialog'});
                    }
                  }

                } else {
                  this.router.navigate(['boards']);
                  this.commonService.notifyOther({action: 'closeActivityDialog'});
             }

            } else {
                this.router.navigate(['boards']);
            }                        
        });        
    }

    private goToActivity(static_url: boolean) {
        let setting = {
            board_id: this.board_id,
            from_module: 'board',
            is_template: 0,
            static_url: 0//static_url
        }   

        if(this.actUrl) {
            setting['prefix'] = this.actUrl;
        }        

        setTimeout(() => { 
            this.commonService.notifyOther({action: 'editActivityDialog', afterSubmitAction: '', act_id: this.actId, setting: setting});
        }, 800);
    }

    private testUrl(url:string) {
        let regex= /^[A-Z0-9]+-[0-9]+$/; 
        return url.match(regex) ? true : false;
    }
}