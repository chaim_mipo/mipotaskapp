import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { BoardService } from '@app/_services/index';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'board-template-form',
  templateUrl: 'board.template.form.component.html'
})

export class BoardTemplateFormComponent implements OnInit{
    @Input() temp_id = 0;
    @Input() temp_name = '';
    @Input() this_dialog;
    @Input() my_parent;
    @Input() is_template = 1;

    newTemplateForm;
    public ready = false;
    newTemplateFormSubmitted = false;
    public template_list = [];
    public attr_list_source = [];
    public attr_list = [];
    public attr_title = '';

    newTemplateFormErrors = {
        name : ''
    };

    validationMessages = {};     

    constructor(private fb: FormBuilder, private boardService: BoardService, private router: Router, private translateService:TranslateService) {
        this.translateService.get(['GENERAL']).subscribe((data:any)=> {
            this.validationMessages = {
                'name': {
                    'required': data['GENERAL']['REQUIRED_FIELD']
                },
                'temp_id': {
                    'required': data['GENERAL']['REQUIRED_FIELD']
                }
            };             
          });        
    }

    attrFilter(value) {
        this.attr_list = this.attr_list_source.filter((s) => s.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }

    ngOnInit() {
        if(this.is_template || this.temp_id) {
            this.ready = true;
        } else {
            let params = {
                is_template: 1,
                component: 'form'
            };
            this.boardService.getBoardTemplates(params)
            .subscribe(
                results => {
                    this.ready = true;
                    if(results['success']) {
                        this.template_list = results['board_templates'];  
                      //  this.attr_list = results['attr_list'];
                        this.attr_list_source = results['attr_list'];                          
                        this.attr_list = this.attr_list_source.slice();

                        this.attr_title = results['attr_title'];
                        if(this.attr_title) {
                            this.newTemplateForm.controls["attr_id"].setValidators(Validators.required);
                            this.newTemplateForm.controls["attr_id"].updateValueAndValidity();                        
                        }
                    }
                });
            
        }

        this.createForm(); 
    }

    closeDialog() {
        if(this.this_dialog) {
            this.this_dialog.close();
        }
    }

    onNewTemplateFormSubmit() {
        let selected_temp_id = this.newTemplateForm.controls['temp_id'] ? this.newTemplateForm.controls['temp_id'].value : '';
        let attr_id = this.attr_title ? this.newTemplateForm.controls['attr_id'].value : '';
        if(this.is_template || (!this.is_template && !selected_temp_id)) {
            this.newTemplateFormSubmitted = true;
            let params = {
                id: this.temp_id,
                name: this.newTemplateForm.controls['name'].value,
                is_template: this.is_template
            };                
            
            if(this.attr_title) {
                params['attr_id'] =  attr_id;             
            }
                
            this.boardService.createBoardTemplate(params)
            .subscribe(
                results => {
                    this.newTemplateFormSubmitted = false;
                    this.closeDialog();
                    if(results['success']) {
                        if(this.temp_id) {
                            this.my_parent.setTempName(this.temp_id, results['name']);
                        } else if(results['item']) {
                            this.my_parent.addBoardTemplate(results['item'], true, '');                        
                        }
                        //this.router.navigate(['tboards/' + results['new_id']]);
                    }
                });
        } else {
            
            this.closeDialog();
            
            this.my_parent.copyTemp(selected_temp_id, 0, this.newTemplateForm.controls['name'].value, attr_id);   
        }
       
    }

    createForm() {
        if(this.is_template) {
            this.newTemplateForm = this.fb.group({
                name: [this.temp_name, Validators.required ]
            }); 
        } else {
            if(this.temp_id) {
                this.newTemplateForm = this.fb.group({
                    name: [this.temp_name, Validators.required ]
                });               
            } else {
                this.newTemplateForm = this.fb.group({
                    name: [this.temp_name, Validators.required ],
                    temp_id: '',//['', Validators.required ]
                    attr_id: ''
                });               

            }
        }
    
        this.newTemplateForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
  
        this.onValueChanged();    

      }   
      
      onValueChanged(data?: any) {
        if (!this.newTemplateForm) { return; }
        const form = this.newTemplateForm;
        for (const field in this.newTemplateFormErrors) {
          // clear previous error message (if any)
          this.newTemplateFormErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.newTemplateFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      }        
}