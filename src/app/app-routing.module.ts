import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from '@app/_guards';
import { LoginComponent } from '@app/login';
import { MainPage } from '@app/main/main.page';
import { HomeComponent } from '@app/home';

import { CustomersPage } from '@app/customer/customers.page';
import { CustomerItemPage } from '@app/customer/customer.item.page';
import { CustomerEditPage } from '@app/customer/customer.edit.page';
import { ActivitiesPage } from '@app/activity/activities.page';
import { BoardItemPage } from '@app/board/board.item.page';
import { BoardTemplatesPage } from '@app/board/board.templates.page';
import { BoardsPage } from '@app/board/boards.page';
import { BoardTemplateItemPage } from '@app/board/board.template.item.page';
import { BoardSettingComponent } from './board/board.setting.component';
import { TimeReportPage } from '@app/time/time.report.page';
import { TimePage } from '@app/time/time.page';
import { SprintsPage } from '@app/sprints/sprints.page';
import { ReleasesPage } from '@app/releases/releases.page';
import { ReleaseItemPage } from '@app/releases/release.item.page';

const routes: Routes = [
  { path: '', component: MainPage, canActivate: [AuthGuard],
    children:
    [
      {
        path: '', redirectTo: 'home', pathMatch: 'full'
      },       
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard], pathMatch: 'full',
        data: { title: ''}
      },        
     
      { path: 'customers', component: CustomersPage, canActivate: [AuthGuard], pathMatch: 'full',
        data: { title: 'Customers'}
      },
      { path:  'customers/:id', component: CustomerItemPage, canActivate: [AuthGuard],
        data: { title: 'Customer'}
      },
      { path:  'customers/:id/edit', component: CustomerEditPage, canActivate: [AuthGuard],
        data: { title: 'Edit Customer'}
      },
      { path: 'activities', component: ActivitiesPage, canActivate: [AuthGuard], pathMatch: 'full',
        data: { title: 'Activities'}
      },      
      { path: 'tboards', component: BoardTemplatesPage, canActivate: [AuthGuard], data: { title: 'Templates'},
        children: [
            {
              path:  ':id',
              component:  BoardTemplateItemPage,
              pathMatch: 'full',
              data: { title: 'Template'}
            }
        ]
      }, 
      { path: 'boards', component: BoardsPage, canActivate: [AuthGuard], data: { title: 'Boards'},
        children: [
            {
              path:  ':id',
              component:  BoardItemPage,
              pathMatch: 'full',
              data: { title: 'Board'}
            },          
            { path: ':id/setting', component: BoardSettingComponent, canActivate: [AuthGuard],
              data: { title: 'Board Setting', showUpdateDate: false}
            },            
            {
              path:  ':id/:actUrl',
              component:  BoardItemPage,
              pathMatch: 'full',
              data: { title: 'Board'}
            }
        ]
      },    
      { path: 'time', component: TimePage, canActivate: [AuthGuard], pathMatch: 'full',
        data: { title: 'Time'}
      },    
      { path: 'sprints', component: SprintsPage, canActivate: [AuthGuard], pathMatch: 'full',
        data: { title: 'Sprints'}
      },   
      { path: 'releases', component: ReleasesPage, canActivate: [AuthGuard], pathMatch: 'full',
        data: { title: 'Releases'}
      },
      { path:  'releases/:id', component: ReleaseItemPage, canActivate: [AuthGuard],
        data: { title: 'Release'}
      },                        
      { path: 'time-report', component: TimeReportPage, canActivate: [AuthGuard], pathMatch: 'full',
        data: { title: 'Reported Hours'}
      }            
    ]
  },  
  { path: 'login', component: LoginComponent, data: { title: 'login'} },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routing = RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules });