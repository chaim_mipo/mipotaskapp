import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { CustomerService, CommonService, GlobalDataService, PropertyService } from '@app/_services';
import { TabStripComponent } from '@progress/kendo-angular-layout';

@Component({
  selector: 'customer-item-page',
  templateUrl: './customer.item.page.html'
})
export class CustomerItemPage implements OnInit, OnDestroy {
    @ViewChild('kendoTabStripInstance', {static: false}) kendoTabStripInstance: TabStripComponent;
    private navigationSubscription: Subscription; 
    public customer_id: number = 0;
    public item;
    public noteName : string = 'Customer';
    public selectedStatusItem: number = 0;
    public statusItemModel: string = this.noteName;
    public statusItemField: string = 'status_id';
    public statusItems = ['1', '2', '3', '4', '5', '6'];    
    public mainStatusList = [];

    public page_title = '';
    public customerModel;
    public page_description = '';
    public page_price = '';
    public page_price_description = '';
    public prop_action_title = 'sell';
    url_params = {};

    main_container_col1 = 8;
    main_container_col2 = '';    

    customerItemLoaded = false;
    objectKeys = Object.keys;
    constructor(
        private customerService: CustomerService, 
        private commonService: CommonService,
        private propertyService: PropertyService,
        private globalDataService: GlobalDataService,
        private router: Router, private actRoute: ActivatedRoute, private location: Location) { }

    ngOnDestroy() {
        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    } 

    ngOnInit() {
        this.navigationSubscription = this.actRoute.paramMap.subscribe(params => {
            this.url_params = params['params'];
            if(params.get('id')) {
                let id = params.get('id');
                if(/^\d+$/.test(String(id)) && parseInt(id) > 0) {
                    setTimeout(() => {                        
                        this.commonService.notifyOther({action: 'setTitle', title: 'customer - ' + id});
                    }, 100);

                    this.loadDetails(id, params);                  

                } else {
                  this.router.navigate(['customers']);
             }

            } else {
                this.router.navigate(['customers']);
            }         

        });
    }

    toEditCustomer() {
        this.router.navigate(['customers/' + this.customer_id + '/edit/']);
    }

    loadDetails(id, params?) {

        this.customerService.getCustomerDetails(id)
        .subscribe(
            results => {
                this.customerItemLoaded = true;
                this.customer_id = id;
                this.customerModel = results['model'];
                this.statusItems = results['pipeItems'];
                this.selectedStatusItem = results['model'].status_id;
                this.mainStatusList = results['main_status_list'];

                this.buildPageTitles();

                this.globalDataService.last_visited_customer = this.customer_id;

                if(params) {
                    if (params.get('main_tab') && (parseInt(params.get('main_tab')) >= 0 && parseInt(params.get('main_tab')) < 4)) { 
                        setTimeout(() => {
                          this.kendoTabStripInstance.selectTab(parseInt(params.get('main_tab')));

                          if(parseInt(params.get('main_tab')) == 1){
                              this.main_container_col1 = 12;
                              this.main_container_col2 = 'col2_main_hide_view';
                          }
                          else {
                            this.main_container_col1 = 8;
                            this.main_container_col2 = '';
                          }

                        }, 50);
                      }
                }
            },
            error => {
                this.customerItemLoaded = true;
                this.router.navigate(['customers']);
            }
        );

    }


    onMainTabSelect(event){
        let params = Object.assign({}, this.url_params);
        params['main_tab'] = event.index;

        if(params['main_tab'] == 1) {
          this.main_container_col1 = 12;
          this.main_container_col2 = 'col2_main_hide_view';
        } else {
          this.main_container_col1 = 8;
          this.main_container_col2 = '';
        }
      
        if(params.hasOwnProperty('id')) {
          delete params['id'];
        }      
  
        let new_url = this.router.serializeUrl(this.router.createUrlTree(['/customers/' + this.customer_id, params]));
        // this.router.navigate(['/property/property-grid', params], { replaceUrl: false });
        this.location.replaceState(new_url);
    }

    public buildPageTitles() {
        this.page_title = this.customerModel.full_name;
        this.page_description = this.customerModel.phone;
  
        if (this.customerModel.search_price) {
          this.page_price = this.customerModel.search_price;
        }
  
        if (this.customerModel.prop_types) {
          this.page_price_description = this.customerModel.prop_types;
        }

        if (this.customerModel.search_action_id) {
            if (this.customerModel.search_action_id == 1) {
              this.prop_action_title = 'rent';
            } else if (this.customerModel.search_action_id == 2) {
              this.prop_action_title = 'sell';
            }
          }        
  
    }

    updateMainStatus(event) {
        this.selectAutoSave(event, 'main_status');       
    }

    selectAutoSave(event, field) {
        let val = event;
        if(event && event.target && event.target.value) {
          val = event.target.value;
        }
    
        let params = 'RealtorCustomer[' + field + ']=' + val;
    
        this.commonService.notifyOther({action: 'showSavingMask'});
        this.propertyService.autoSave(this.customer_id, 'RealtorCustomer', params)
           .subscribe(
               results => {
                   this.commonService.notifyOther({action: 'hideSavingMask'});
                   if (results['lastNote']) {
                    this.commonService.notifyOther({action: 'addNoteToList', item: results['lastNote']});
                  }               
               }
           );
      }    

}