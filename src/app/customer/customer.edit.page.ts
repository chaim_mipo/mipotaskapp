import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { CustomerService, CommonService, GlobalDataService } from '@app/_services';
import { TabStripComponent } from '@progress/kendo-angular-layout';

@Component({
  selector: 'customer-edit-page',
  templateUrl: './customer.edit.page.html'
})
export class CustomerEditPage implements OnInit, OnDestroy {
    @ViewChild('kendoTabStripInstanceCustomerEdit', {static: false}) kendoTabStripInstanceCustomerEdit: TabStripComponent;
    private navigationSubscription: Subscription; 
    customerFiles = [];
    customer_id: number;
    public constructor(public globalDataService: GlobalDataService, private customerService: CustomerService, public _location: Location, private actRoute: ActivatedRoute, private router: Router ) { }

    ngOnDestroy() {
        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    } 

    ngOnInit() {

        this.navigationSubscription = this.actRoute.params.subscribe(params => {
            this.customer_id = parseInt(params['id']);
    
            if (this.customer_id && this.customer_id == params['id']) {
              if (params.hasOwnProperty('main_tab') && (params['main_tab'] >= 0 && params['main_tab'] < 3)) {
                setTimeout(() => {
                  if(this.kendoTabStripInstanceCustomerEdit) {
                    this.kendoTabStripInstanceCustomerEdit.selectTab(parseInt(params['main_tab']));
                  }
                }, 50);
              }
            } else {
              this.router.navigate(['customers']);
            }
    
        });
    }

    onEditCustomerTabSelect(event){

        let params = {};
        params['main_tab'] = event.index;
        let new_url =  this.router.serializeUrl(this.router.createUrlTree(['/customers/' + this.customer_id + '/edit', params]));
        // this.router.navigate(['/property/property-grid', params], { replaceUrl: false });
        this._location.replaceState(new_url);
    
        if (event.index > 0) {
          if(event.index == 1) {
            this.customerFiles = [];
          }
    
       /*   this.customerService.getCustomerFiles(this.customer_id, event.index)
                .subscribe(
                    results => {
                      if (event.index == 1) {
                        this.customerFiles = results.files;
                      }
    
                      this.branch_id = results.branch_id;
                    }
                );*/
        }
    
      }
    
    public goback() {
        this._location.back();
      }
    

}