import { Component, OnInit, OnDestroy, Input, ViewChild, EventEmitter, Output, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { State } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent, GridComponent } from '@progress/kendo-angular-grid';
import { CategoriesService } from '@app/_services/northwind.service';
import { CustomerService, CommonService } from '@app/_services/index';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'customer-grid',
    providers: [CategoriesService],
    templateUrl: './customer.grid.component.html'
  })

  export class CustomerGridComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private navigationSubscription: Subscription; 
    @ViewChild('customerGridInstance', {static: false}) customerGridInstance: GridComponent;
    @Input() def_take = 15;
    @Input() def_skip = 0;  
    @Input() def_sort = []; 
    @Input() module = '';  
    @Input() view_mode = 'page';  
    @Input() searchTerm = '';  
    @Output()emitState: EventEmitter<any> = new EventEmitter<any>();        
    
    public loadingIndicator: boolean = true;
    public filters = {};
    public view: Observable<GridDataResult>;

    public state: State = {
        skip: this.def_skip,
        take: this.def_take,
        sort: this.def_sort
    };
    private controller = 'customer/table';
    public take_list = [10, 15, 25, 50, 100, 150];    

    public constructor(    
        private service: CategoriesService, 
        private commonService: CommonService,
        private customerService: CustomerService
    ) {  }    

    isFavSorting() {
        return this.state['sort'].length && this.state['sort'][0] && this.state['sort'][0]['field'] == 'fav' && this.state['sort'][0]['dir'] ? 1 : 0
    }

    favToggle(cust_id) {

        let obj = this.customerGridInstance.data['data'].find(
          row => row.id === cust_id);
    
        let new_fav = 1 -obj.fav;
        obj.fav = new_fav;
    
        this.customerService.toggleFav(cust_id)
        .subscribe(
            results => {
                
            }
        );
    
      }    

    public ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();     
        }
    }


    public ngOnInit() {
                
        if(this.def_take > 150 ) {
            this.def_take = 50;
        }
        this.state = {
            skip: this.def_skip,
            take: this.def_take,
            sort: this.def_sort
        }  

        this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('action')) {
              if(res.action == 'loadCustomer') {
                this.loadCustomers();
              } else if (res.action == 'hideIndicator' + this.controller) {
                this.loadingIndicator = false;
              }
            }
          });  

        this.loadCustomers();
    }

    ngOnChanges(changes: SimpleChanges) {//console.log(changes);
        if((changes["searchTerm"] && !changes["searchTerm"].isFirstChange()) || 
            (changes["cities"] && !changes["cities"].isFirstChange()) ||
            (changes["areas"] && !changes["areas"].isFirstChange()) ||            
            (changes["estate_types"] && !changes["estate_types"].isFirstChange())
        ) {
            this.state.skip = 0;
            this.loadCustomers();
        }        

    }     

    loadCustomers() {
        this.filters['module'] = this.module;
        this.filters['view_mode'] = this.view_mode;
        this.filters['search_term'] = this.searchTerm;             
        this.page();      
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
 //       console.log(this.state);
        this.emitState.emit(this.state);
        this.loadingIndicator = true;
        this.service.query(state, this.controller, this.filters);
      }    

    page() {
        this.loadingIndicator = true;
        this.view = this.service;
        this.service.query(this.state, this.controller, this.filters);
      }    
     
  }