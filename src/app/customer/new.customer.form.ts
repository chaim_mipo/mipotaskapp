import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from '@app/_services/customer.service';
import { SiteService } from '@app/_services/site.service';
import { CommonService } from '@app/_services/common.service';
import { IntlService } from '@progress/kendo-angular-intl';
import { AutoCompleteComponent } from '@progress/kendo-angular-dropdowns';
import { Router } from '@angular/router';

@Component({
  selector: 'new-customer-form',
  templateUrl: 'new.customer.form.html'
})

export class NewCustomerForm implements OnInit{
    @ViewChild('myCompAuto', {static: false}) myCompAuto: AutoCompleteComponent;
    @Input() public my_parent;
    @Input() public this_dialog;
    @Input() cust_id : number = 0;
    @Input() grid_col = 6;

    public customerFormSubmitted = false;
    public ready = false;
    public saving = false;    
    public dialog_form_content_add_class = '';
    public company_loading = false;
    public popupSettings = {};
    public company_label: string = 'חברה';
    public compData = [];  
    public formValues = {};

    multi_required_fields = ['search_city_ids'];
  
    customerForm: FormGroup;
    managers = [];
    source_list = [];
    action_list = [];
    city_list = [];
    company_list = [];
    prop_types = [];
    form_data = {};   
    customerFormLables = {};        
    
    customerFormErrors = {
        company : '',
        full_name : '',
        phone : ''
      };
            
    validationMessages = {
     /*   'summary': {
            'required': 'שדה חובה',
            'maxlength': 'Max length 180'
        },*/
        
        'full_name': {
            'required': 'שדה חובה'
        },
        'phone': {
            'required': 'שדה חובה'
        },
        'company': {
            'required': 'שדה חובה'
        }
    };    

    constructor(
        private intl: IntlService, 
        private fb: FormBuilder, 
        private commonService: CommonService,
        private customerService: CustomerService,
        private siteService: SiteService,
        private router: Router) {
        }

    custAutoFilterChange(term, mode) {
        if(term) {
            if(mode == 'company') {
                this.myCompAuto.loading = true;
            }
        
            this.customerService.searchByTerm(term, mode)
                  .subscribe(
                      results => {
                        if(mode == 'company') {
                            this.myCompAuto.loading = false;
                            this.compData = results['rows'];
                        }
                      }
                    );
                  }
    }

    public ngOnInit() {    

        this.customerService.getCustomerFormSettings(this.cust_id)
        .subscribe(
            results => {
                this.action_list = results['action_list'];
                this.customerFormLables = results['labels'];  
                this.source_list = results['source_list'];  

                
                if(this.cust_id) {
                    if(results['form_data_success']) {
                        this.formValues = results['form_data'];

                        for (var key in this.formValues) {
                            var attrName = key;
                            var attrValue = this.formValues[key];

                            if (this.customerForm.controls[attrName]) {
                                if(attrName == 'birthday' && attrValue) {
                                  attrValue = new Date(attrValue);
                                } 

                                this.customerForm.controls[attrName].setValue(attrValue);
                              } 


                        }

                    }

                }

                this.ready = true;
            });
        

        this.createForm();        
    }
 
    onValueChanged(data?: any) {
        if (!this.customerForm) { return; }
        const form = this.customerForm;
        for (const field in this.customerFormErrors) {
          // clear previous error message (if any)
          this.customerFormErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.customerFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      }    

    closeDialog() {
        if(this.this_dialog) {
            this.this_dialog.close();
        }
    }

    onCustomerFormSubmit() {
        this.customerFormSubmitted = true;
        const formData = new FormData();
               
        formData.append('RealtorCustomer[id]', String(this.cust_id));     

        Object.keys(this.customerForm.controls).forEach(key => {     
            if(key != 'myUpload') {   
                let val = this.customerForm.controls[key].value;
                if(key == 'from_date') {
                    val = this.intl.formatDate(val, 'dd/MM/yyyy');
                } else if (Object.prototype.toString.call( val ) === '[object Array]' ) {
                    val = val.join(',');
                    if (val == 'null' || val == 'undefined' || val == 'NaN') {
                        val = '';
                    }
                }
                formData.append('RealtorCustomer[' + key + ']', val);
            }
        });
        this.customerService.saveItem(formData)
        .subscribe(
            results => {
                this.customerFormSubmitted = false;               
                this.closeDialog();
                if(results['cust_id']){
                    this.router.navigate(['customers/' + results['cust_id']]);
                } else {
                    this.commonService.notifyOther({action: 'loadCustomer'});
                }
            });

    }

    createForm() {
        this.customerForm = this.fb.group({
            company: ['', Validators.required ],            
            full_name: ['', Validators.required ],
            phone: ['', Validators.required ],  
            phone2: '',                     
            email: '',
            details: ''
//  summary : ['', Validators.compose([Validators.maxLength(180), Validators.required])],            
        });
    
        this.customerForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
  
        this.onValueChanged();    

      }


      public autoSave(field, event) {
                
        if (this.cust_id && this.customerForm.controls[field] && 
            ((this.multi_required_fields.indexOf(field) == -1 && this.customerForm.controls[field].valid)  || (this.multi_required_fields.indexOf(field) > -1 && event ))) {
                
      //      console.log(event + ' ;;; ' + this.customerForm.controls[field].value);                                    
            let params = '';
            if (field == 'manager_ids' || field == 'search_city_ids' || field == 'search_prop_type_id') {
                if(event == null){
                event = '';
                }
                params = 'RealtorCustomer[' + field + ']=' + encodeURIComponent(event);
            } else {
                let val = this.customerForm.controls[field].value;
                if (Object.prototype.toString.call( val ) === '[object Array]' ) {
                val = val.join(',');
                } else {
                val = encodeURIComponent(val);
                if (val == 'true') {
                    val = 1;
                } else if (val == 'false') {
                    val = 0;
                } else if (val == 'null') {
                    val = '';
                }
                }
        
                params = 'RealtorCustomer[' + field + ']=' + val;
            }
  //      console.log(params);
           if (params) {
                this.commonService.notifyOther({action: 'showSavingMask'});
                this.siteService.autoSave(this.cust_id, 'RealtorCustomer', params)
                    .subscribe(
                        results => {
                            this.commonService.notifyOther({action: 'hideSavingMask'});
                        }
                    );
            }
        
    
        }
      }      
             
 }