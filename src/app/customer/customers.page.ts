import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Input } from '@angular/core';
import { debounceTime, map, distinctUntilChanged, filter } from "rxjs/operators";
import { CommonService, CustomerService } from '@app/_services';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { fromEvent, Subscription } from 'rxjs';
import { NewCustomerForm } from './new.customer.form';
import { DialogService } from '@progress/kendo-angular-dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'customers-page',
  templateUrl: './customers.page.html',
})
export class CustomersPage implements OnInit, OnDestroy {
    @ViewChild('customersFilterSearchTerm', {static: false, read: ElementRef}) customersFilterSearchTerm: ElementRef;    
    @Input() view_mode = 'page'; 
    private subscription: Subscription;
    private navigationSubscription: Subscription;  
    public customers_loaded = false;
    public search_placeholder = 'ID | Name | Company | Phone';
    public isSearching = false;
    public area_count = 1;
    customerDialog;

    private new_customer_title = 'New Customer';

    public selected_filter = {
        searchTerm: ''
      };  
  
      public def_take = 15;
      public def_skip = 0;
      public def_sort = [];
  
      public grid_params = {
        def_take: this.def_take,
        def_skip: this.def_skip,
        def_sort: this.def_sort
      }

    constructor(
        private customerService: CustomerService,
        private router: Router, 
        private actRoute: ActivatedRoute, 
        private location: Location, 
        private dialogService: DialogService,
        private translateService:TranslateService) {
          this.translateService.get(['CUSTOMER']).subscribe((data:any)=> {
            this.search_placeholder = data['CUSTOMER']['SEARCH_PLACEHOLDER'];
            this.new_customer_title = data['CUSTOMER']['NEW_CUSTOMER'];
          });           
         }
    
    ngAfterViewInit() {    
        this.subscription = fromEvent(this.customersFilterSearchTerm.nativeElement, 'keyup').pipe(
            // get value
            map((event: any) => {
            return event.target.value;
            })
            // if character length greater then 2
            ,filter(res => res.length >= 0)
            // Time in milliseconds between key events
            ,debounceTime(450)        
            // If previous query is diffent from current   
            ,distinctUntilChanged()
            // subscription for response
            ).subscribe((text: string) => {
            this.isSearching = true;
            this.selected_filter.searchTerm = text;
            this.changeFilter(true);
            });    
    }

    ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }  
    
    
    ngOnInit() {
        this.navigationSubscription = this.actRoute.params.subscribe(params => {
            this.customerService.getGridSettings().subscribe(
              results => {                
                
                this.customers_loaded = true;
  
                for (let key in this.selected_filter) {               
  
                  if (params.hasOwnProperty(key) && params[key]) {
                    let val = params[key];
                    if(key == 'source_id') {
                      val = parseInt(params[key]);
                    }
                    this.selected_filter[key] = val;
                  }
                }  
                
                for (let key in this.grid_params) {               
  
                  if (params.hasOwnProperty(key) && params[key]) {
                    let val = parseInt(params[key]);
                    
                    this.grid_params[key] = val;
                    this[key] = val
                  }
                }
                
                if (params.hasOwnProperty('dir') && (params['dir'] == 'asc' || params['dir'] == 'desc') && params.hasOwnProperty('field') && params['field']) {
                //    this.grid_params.def_sort.push({dir: params['dir'], field: params['field']});
                    this.def_sort.push({dir: params['dir'], field: params['field']});
                }                
  
              });
          });        
    }

    addCustomerDialog() {
        this.customerDialog = this.dialogService.open({
            title: this.new_customer_title,
            minWidth: 320,
            width: 580,
            content: NewCustomerForm
        });
   
        var activity = this.customerDialog.content.instance;
        activity.my_parent = this;
        activity.this_dialog = this.customerDialog;                       
    }

    changeFilter(def_state?: boolean): void {
        if(this.view_mode == 'page') {
          if(def_state) { 
            this.grid_params.def_skip = 0;
          }
          let params = {}    
          for (let key in this.selected_filter) {
            let current_param = this.selected_filter[key];
            if(current_param) {
              params[key] = current_param;
            }
          }

          for (let key in this.grid_params) {
            let current_param = this.grid_params[key];
            if(current_param) {
              if(key == 'def_sort') {
                if(current_param.length) {
                    params['dir'] = current_param[0].dir;
                    params['field'] = current_param[0].field;
                }
              } else {              
                params[key] = current_param;
              }
            }
          }        

          const new_url = this.router.serializeUrl(this.router.createUrlTree(['/customers', params]));          
          this.location.replaceState(new_url);    
        }
      } 
      
      stateGrid(event): void {
        // console.log(event);
        this.grid_params.def_skip = event.skip;
        this.grid_params.def_take = event.take;

        if(event.sort && event.sort[0] && event.sort[0].dir) {
          this.grid_params.def_sort = [];
          this.grid_params.def_sort.push(event.sort[0]);
        } else {
            this.grid_params.def_sort = [];
        }        

        this.changeFilter(false);
      }      
}