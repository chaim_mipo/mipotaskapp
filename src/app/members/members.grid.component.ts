import { Component, OnInit, OnDestroy, Input, EventEmitter, Output, SimpleChanges, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { State } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { CategoriesService } from '@app/_services/northwind.service';
import { CommonService, GlobalDataService } from '@app/_services/index';
import { fromEvent, Subscription } from 'rxjs';
import { memberRowItem } from './member.model';
import { NotificationService } from '@progress/kendo-angular-notification';
import { debounceTime, map, distinctUntilChanged, filter } from "rxjs/operators";
import { DialogService, DialogCloseResult, DialogRef } from '@progress/kendo-angular-dialog';
import { MemberFormDialog } from './member.form.component';
import { CustomerService } from '@app/_services/customer.service';

@Component({
    selector: 'members-grid',
    providers: [CategoriesService],
    templateUrl: './members.grid.component.html'
  })

  export class MembersGridComponent implements OnInit, OnDestroy {
    @ViewChild('membersFilterSearchTerm', {static: false, read: ElementRef}) membersFilterSearchTerm: ElementRef;      
    private subscriptions: Subscription[] = [];
    @Input() controller: string = 'member/table';
    @Input() itemName: string = 'Sprint';
    @Input() def_take: number = 50;
    @Input() def_skip: number = 0;  
    @Input() def_sort = []; 
    @Input() CustomerId: number = null;   
    @Input() searchTerm: string = '';
    @Input() status: number = null;
    @Input() reloadGrid: boolean = false;
    @Output()emitState: EventEmitter<any> = new EventEmitter<any>(); 
    @Output()emitAction: EventEmitter<any> = new EventEmitter<any>();       
    
    public loadingIndicator: boolean = true;
    public filters = {};
    public view: Observable<GridDataResult>;
    public memberDialog: DialogRef = null;
    public confirmDialog: DialogRef = null;

    public state: State = {
        skip: this.def_skip,
        take: this.def_take,
        sort: this.def_sort
    };

    public take_list: number[] = [10, 15, 25, 50, 100, 150];    
    public search_placeholder: string = 'ID, fullName, phone, email';
    public statusList: {id: number, name: string}[] = [
        {id: 1, name: 'Active'},
        {id: -1, name: 'Not Active'},
    ];

    public constructor(    
        private service: CategoriesService, 
        private commonService: CommonService,
        public globalDataService: GlobalDataService,
        private notificationService: NotificationService,
        private dialogService: DialogService,
        private customerService: CustomerService
    ) {  }    


    public ngOnDestroy() {
        this.subscriptions.forEach((s) => s.unsubscribe());
        if(this.confirmDialog) {
            this.confirmDialog.close();
        }

        if(this.memberDialog) {
            this.memberDialog.close();
        }
    }

    ngAfterViewInit() {    
        this.subscriptions.push(fromEvent(this.membersFilterSearchTerm.nativeElement, 'keyup').pipe(
            // get value
            map((event: any) => {
            return event.target.value;
            })
            // if character length greater then 2
            ,filter(res => res.length >= 0)
            // Time in milliseconds between key events
            ,debounceTime(650)        
            // If previous query is diffent from current   
            ,distinctUntilChanged()
            // subscription for response
            ).subscribe((text: string) => {

            this.searchTerm = text;
            this.loadMembers();
        }));    
    }     

    public changeStatus(value: number): void {
        this.status = value;
        this.loadMembers();
    }

    public ngOnInit() {
                
        if(this.def_take > 150 ) {
            this.def_take = 50;
        }
        this.state = {
            skip: this.def_skip,
            take: this.def_take,
            sort: this.def_sort
        }  

        this.subscriptions.push(this.commonService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('action')) {
              if(res.action == 'loadMembers') {
                this.loadMembers();
              } else if (res.action == 'hideIndicator' + this.controller) {
                this.loadingIndicator = false;
              }
            }
          }));

        this.loadMembers();
    }

    ngOnChanges(changes: SimpleChanges) {//console.log(changes);
        if((changes["searchTerm"] && !changes["searchTerm"].isFirstChange()) || 
            (changes["CustomerId"] && !changes["CustomerId"].isFirstChange()) ||
            (changes["reloadGrid"] && !changes["reloadGrid"].isFirstChange())
        ) {
            this.state.skip = 0;
            this.loadMembers();
        }        

    }

    private openItemDialog(mode: string = 'data', item?: memberRowItem): void {
        this.memberDialog = this.dialogService.open({
            title: mode == 'data' ? (!item ? 'New Member' : 'Edit Member') : 'Reset Password',
            width: mode == 'data' ? 480 : 320,
            content: MemberFormDialog
        });
        this.memberDialog.dialog.location.nativeElement.classList.add("DigModal");

        const idContent = this.memberDialog.content.instance;
        idContent.this_dialog = this.memberDialog;
        idContent.modelId = item ? item.id : null;
        idContent.CustomerId = this.CustomerId;
        idContent.mode = mode;

        this.subscriptions.push(this.memberDialog.result.subscribe((result: any) => {
            if (!(result instanceof DialogCloseResult)) {
              if(result.action == 'updateMembers') { //console.log(result);

                this.loadMembers();
              }
            }

          }));
    }    
    
    public onButtonAddMemberClick(): void {
        // this.tempNotif();
        this.openItemDialog('data');
    }
    
    public editMember(item: memberRowItem): void {
        //this.emitAction.emit({action: 'edit' + this.itemName, item: item});
        this.openItemDialog('data', item);
    }

    public deleteMember(item: memberRowItem): void {
        //this.emitAction.emit({action: 'delete' + this.itemName, item: item});
        this.presentAlertConfirm(item.id);
    }

    private presentAlertConfirm(id: number): void {        
        this.confirmDialog = this.dialogService.open({
            title: 'Delete Member',
            content: 'Are you sure?',
            actions: [
                { text: 'Yes', primary: true },
                { text: 'No' }
            ],
            width: 380,
            minHeight: 230,
            minWidth: 250
          });

        this.subscriptions.push(this.confirmDialog.result.subscribe((result) => {
            if (!(result instanceof DialogCloseResult)) {
                if(result['primary']) {
                    this.removeMember(id);
                }
            }
        }));
    }
    
    private removeMember(id: number): void {
        this.customerService.removeMember(id).subscribe(
            () => {
                this.loadMembers();
                this.notificationService.show({
                    content: 'Member deleted successfully',
                    animation: { type: 'fade', duration: 200 },
                    position: { horizontal: 'center', vertical: 'top' },
                    type: { style:"success", icon: true },
                    hideAfter: 3000
                });                 
            }
        )
    }

    public memberPassword(item: memberRowItem): void {
        this.openItemDialog('password', item);
    }

    private tempNotif(): void {
        this.notificationService.show({
            content: 'Under Construction',
            animation: { type: 'fade', duration: 200 },
            position: { horizontal: 'center', vertical: 'top' },
            type: { style: 'info', icon: true },
            hideAfter: 2800
        });
    }

    loadMembers(): void {
        if(this.CustomerId) {
            this.filters['CustomerId'] = this.CustomerId;
        } else if(this.filters.hasOwnProperty('CustomerId')) {
            delete this.filters['CustomerId'];
        }

        if(this.status) {
            this.filters['active'] = this.status == 1 ? 1 : 0;
        } else if(this.filters.hasOwnProperty('active')) {
            delete this.filters['active'];
        }        

        this.filters['search_term'] = this.searchTerm;             
        this.page();      
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
 //       console.log(this.state);
        this.emitState.emit(this.state);
        this.loadingIndicator = true;
        this.service.query(state, this.controller, this.filters);
      }    

    page() {
        this.loadingIndicator = true;
        this.view = this.service;
        this.service.query(this.state, this.controller, this.filters);
      }    
     
  }