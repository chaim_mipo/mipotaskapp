import { Component, OnInit, Input, OnDestroy, ElementRef  } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { CustomerService } from '@app/_services/customer.service';
import { DialogRef, DialogService } from '@progress/kendo-angular-dialog';
import { Subscription } from 'rxjs';
import { memberFormItem, modelField, memberTypeList, memberColorList } from './member.model';
import { HttpErrorResponse  } from '@angular/common/http';
import { NotificationService } from '@progress/kendo-angular-notification';

@Component({
    selector: 'member-form-dialog',
    templateUrl: 'member.form.component.html',
    styleUrls: ['./member.form.component.scss'],
  })

export class MemberFormDialog implements OnInit, OnDestroy {
    @Input() this_dialog: DialogRef;
    @Input() CustomerId: number = null;
    @Input() modelId: number = null;
    @Input() itemTitle: string = '';
    @Input() mode: string = 'data';

    private item: memberFormItem = null;    
    public memberColorList = memberColorList;
    public memberFields: modelField[] = [];  

    private confirmDialog: DialogRef;
    private subscriptions: Subscription[] = [];
    public newModelForm: FormGroup;
    public ready: boolean = false;
    public newModelFormSubmitted: boolean = false;
    public showErrors: boolean = false;
    public showField: {[key: string]: {field: string, value: any}}[] = [];
    public fieldsErrors: { [key: string]: string }  = {
        first_name: 'Required Field',
        username: 'Invalid Phone', 
        password: 'Required Field',
        email: 'Invalid email',
        type: 'Required Field',
        color: 'Required Field',
    }
/*'מספר טלפון לא תקין',*/
    public serverFieldsErrors: { [key: string]: boolean } = {
        first_name: false,
        username: true,
        password: false,
        email: false,
        type: false,
        color: false
    }


    constructor(
        private fb: FormBuilder,
        private el: ElementRef,
        private dialogService: DialogService,
        private customerService: CustomerService,
        private notificationService: NotificationService,
        ) {

        }

    ngOnInit() {
        this.initMemberFields();
        if(this.modelId) {
            this.loadData();
        } else {
            this.ready = true;
            this.createForm();
        }
    }

    ngOnDestroy() {
        this.subscriptions.forEach((s) => s.unsubscribe());

        if(this.confirmDialog) {
            this.confirmDialog.close();
        }
    }

    private loadData(): void {
        this.customerService.getMember(this.modelId).subscribe(
            (data: {item: memberFormItem}) => { 
                this.item = data.item;
                this.ready = true;
                this.createForm();
            },
            () => {
                this.ready = true;
            }
        )
    }

    private initMemberFields(): void {
        if(this.mode == 'data') {
            this.memberFields.push(        
                {name: 'active', required: false, type: 'checkbox', label: 'Active', control: 'checkbox', defValue: true },
                {name: 'username', required: true, defValue: '', type: 'tel', label: 'Mobile', control: 'input'},
            );
        }

        if(!this.modelId || this.mode == 'password') {
            this.memberFields.push({name: 'password', required: true, defValue: '', type: 'text', label: 'Password', control: 'input'});
        }

        if(this.mode == 'data') {
            this.memberFields.push(
                {name: 'first_name', required: true, defValue: '', type: 'text', label: 'First Name', control: 'input'},
                {name: 'last_name', required: false, defValue: '', type: 'text', label: 'Last Name', control: 'input'},
                {name: 'email', required: true, defValue: '', type: 'email', label: 'Email', control: 'input'},
                {name: 'type', required: true, defValue: 9, type: 'text', label: 'Member Type', control: 'select', options: memberTypeList},
                {name: 'color', required: true, defValue: '9C27B0', type: 'text', label: 'Color', control: 'colorSelect', options: memberColorList},
                {name: 'can_remove_act', required: false, type: 'checkbox', label: 'Remove Tasks', control: 'checkbox', defValue: false },
                {name: 'allow_new_board', required: false, type: 'checkbox', label: 'New Board', control: 'checkbox', defValue: false },
                {name: 'allow_board_setting', required: false, type: 'checkbox', label: 'Board Settings', control: 'checkbox', defValue: false },
                {name: 'time_report', required: false, type: 'checkbox', label: 'Time Report', control: 'checkbox', defValue: false },
                {name: 'allow_sprint', required: false, type: 'checkbox', label: 'Allow Sprint', control: 'checkbox', defValue: false },
                {name: 'allow_release', required: false, type: 'checkbox', label: 'Allow Release', control: 'checkbox', defValue: false },            
            );
        }
    }

    closeDialog(): void {
      if(this.this_dialog) {
          this.this_dialog.close();
      }
    }

    private createForm(): void {
        this.newModelForm = this.fb.group({});

        if(this.memberFields && this.memberFields.length) {
            this.memberFields.forEach((field: modelField) => {

                if(field.hasOwnProperty('show')) {
                    this.showField[field.name] = field.show;
                }

                let fldValid = null;
                if(field.required) {
                    fldValid = Validators.required;
                }

                if(field.hasOwnProperty('type')) {
                    switch(field.type) {
                        case 'email':
                            fldValid = Validators.compose([fldValid, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]); //Validators.email;
                            break;
                        case 'tel':
                            // ^\+(?:[0-9] ?){6,14}[0-9]$
                            //fldValid = Validators.compose([fldValid, Validators.pattern('^05[0-9]+$'), Validators.maxLength(10), Validators.minLength(9)]);
                            fldValid = Validators.compose([fldValid, Validators.pattern('^[0-9]+$'), Validators.maxLength(15), Validators.minLength(6)]);
                            break;
                    }
                }

                if(this.item && field.name == 'action_id' && (typeof(this.item[field.name]) === "undefined" || this.item[field.name] === null)) {
                    this.item[field.name] = field.defValue;
                }

                this.newModelForm.addControl(field.name, new FormControl(this.item ? this.item[field.name] : (field.defValue ? field.defValue : '') , fldValid));
                this.newModelForm.controls[field.name].updateValueAndValidity();
            });

        }
    }


    public onNewModelFormSubmit(force?: boolean): void {
        if(this.newModelForm.valid) {
            if(this.mode == 'data') {
                let formData = {};
                this.memberFields.forEach((field: modelField) => {
                    let val = this.newModelForm.controls[field.name].value;
                    if(field.type == 'checkbox') {
                        val = val ? 1 : 0;
                    }
                    formData[field.name] = val;
                })
                //formData = this.newModelForm.value;
                if(this.modelId) {
                    formData['id'] = this.modelId;
                } 

                formData['cust_id'] = this.CustomerId;

                this.saveData(formData);
            } else {
                this.resetPwd();
            }
        } else {
            this.clientNotValid();            
        }
    }

    private clientNotValid(): void {
        this.showErrors = true;
        this.newModelFormSubmitted = false;
        const invalidControl = this.el.nativeElement.querySelector('form .ng-invalid');
        if (invalidControl) {
            invalidControl.focus();
        }
    }    

    private resetPwd(): void {
        this.newModelFormSubmitted = true;
        const params = {
            password: this.newModelForm.controls['password'].value,
            id: this.modelId
        };

        this.customerService.resetMemberPassword(params).subscribe(
            () => {
                this.newModelFormSubmitted = false;
                this.this_dialog.close({ action: "resetPwd"});
                this.notificationService.show({
                    content: 'Password Reset Successfully',
                    animation: { type: 'fade', duration: 200 },
                    position: { horizontal: 'center', vertical: 'top' },
                    type: { style:"success", icon: true },
                    hideAfter: 3000
                });                 
            },
            () => {
                this.newModelFormSubmitted = false;
            }
        );
    }
    
    private saveData(formData: {}): void {
        this.cancelServerErrors();
        this.newModelFormSubmitted = true;

       this.customerService.saveMember(formData).subscribe((data: any) => {
            this.newModelFormSubmitted = false;
            this.this_dialog.close({ action: "updateMembers", isNew: !this.modelId ? true : false});
        },
        (errors: HttpErrorResponse ) => {
                let errMess: string = '';
                this.newModelFormSubmitted = false;
                if(errors.error && errors.error.detail) {
                    if(errors.error.detail.constructor.name == 'Object') {
                        for (let key in errors.error.detail) {
                            // check if the property/key is defined in the object itself, not in parent
                            if (this.serverFieldsErrors.hasOwnProperty(key)) {                                
                                errMess += errors.error.detail[key][0];
                                this.serverFieldsErrors[key] = true;
                            }
                        }
                    }
                }

                if(errMess) {
                    this.notificationService.show({
                        content: errMess,
                        animation: { type: 'fade', duration: 200 },
                        position: { horizontal: 'center', vertical: 'top' },
                        type: { style: "error", icon: true },
                        hideAfter: 3600
                    });          
                }      
            }
        );
    }

    private cancelServerErrors(): void {
        for (let key in this.serverFieldsErrors) {
            this.serverFieldsErrors[key] = false;
        }
    }
 }