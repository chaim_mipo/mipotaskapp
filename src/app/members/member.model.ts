export interface modelField {
    name: string,
    required: boolean,
    value?: any,
    label?: string,
    type?: string,
    defValue?: any,
    control?: string,
    show?: {field?: string, value: any},
    options?: {value: number | string, text?: string | number}[]
}

export interface memberRowItem {
    id?: number,
    cust_id?: number,
    fullName?: string,
    company?: string,
    color?: string,
    created?: string,
}

export interface memberFormItem {
    id?: number,
    active?: boolean,
    type?: number,
    username?: string,
    password?: string,
    first_name?: string,
    last_name?: string,
    email?: string,
    color?: string,
    can_remove_act?: boolean
    allow_new_board?: boolean, 
    allow_board_setting?: boolean, 
    time_report?: boolean, 
    allow_sprint?: boolean,
    allow_release?: boolean
}

export const memberTypeList: {value: number, text: string}[] = [
    {value: 9, text: 'Customer'},
    {value: 30, text: 'Member-Mipo'},
    {value: 90, text: 'Admin'},
    {value: 100, text: 'Developer'},
];

export const memberColorList: {value: string, label?: string}[] = [
    {value: '9C27B0'},
    {value: 'EC407A'},
    {value: '35AC19'},                
    {value: '512DA8'},
    {value: '5C6BC0'},
    {value: '455A64'},
    {value: '7E57C2'},
    {value: '0151CC'},
    {value: '00A4BE'},
    {value: '4A8BF6'},
    {value: 'FF0000'},
    {value: 'BF9000'},
    {value: '85200C'},
    {value: 'A4C2F4'},
    {value: '666666'},
];