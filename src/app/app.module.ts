import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { JwtInterceptor, ErrorInterceptor, UploadInterceptor } from '@app/_helpers';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
//import { EditorModule } from '@progress/kendo-angular-editor';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { MenuModule } from '@progress/kendo-angular-menu';
import { NotificationModule } from '@progress/kendo-angular-notification';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { PopupModule } from '@progress/kendo-angular-popup';
import { UploadModule } from '@progress/kendo-angular-upload';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { IntlModule } from '@progress/kendo-angular-intl';
import { RTL } from '@progress/kendo-angular-l10n';
import '@progress/kendo-angular-intl/locales/he/all';
import '@progress/kendo-angular-intl/locales/en/all';

import { MatButtonModule, MatCheckboxModule, MatSlideToggleModule, MatTooltipModule} from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ResponsiveModule } from 'ngx-responsive';
import { LightboxModule } from 'ngx-lightbox';
import { EditorModule } from '@tinymce/tinymce-angular';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import { CommonService } from '@app/_services/common.service';
import { GlobalDataService } from '@app/_services/globaldata.service';
import { CookieService } from 'ngx-cookie-service';
import { RoutingState } from '@app/_models/routing.state';
import { OnlyNumber } from './onlynumber.directive';

import { MainPage } from '@app/main/main.page';
import { NoContentComponent } from './no-content/no-content.component';
import { LoginComponent } from '@app/login';
import { ForgotComponent } from '@app/login/forgot.component';
import { HomeComponent } from '@app/home';
import { ActivitiesPage } from '@app/activity/activities.page';
import { ActivityGridComponent } from '@app/activity/activity.grid.component'
import { ActivityFormComponent } from '@app/activity/activity.form.component';
import { ActivityTopicComponent } from '@app/activity/activity.topic.component';
import { BoardComponent } from './board/board.component';
import { BoardTemplatesComponent } from './board/board.templates.component';
import { BoardTemplatesPage } from './board/board.templates.page';
import { BoardsPage } from './board/boards.page';
import { BoardTemplateFormComponent } from './board/board.template.form.component';
import { BoardTemplateItemPage } from './board/board.template.item.page';
import { BoardItemPage } from './board/board.item.page';
import { BoardUserFormComponent } from './board/board.user.form.component';
import { BoardTopicFormComponent } from './board/topic/board.topic.form.component';
import { BoardTopicGridComponent } from './board/topic/board.topic.grid.component';

import { BoardSprintGridComponent } from './board/sprint/board.sprint.grid.component';

import { BoardSettingComponent } from './board/board.setting.component';
import { BoardUserGridComponent } from './board/board.user.grid.component';
import { MessageContentComponent } from '@app/_components/message-content/message.content.component';
import { MultiCheckboxComponent } from '@app/_components/multi-checkbox/multi.checkbox.component';
import { ItemNavigationComponent } from '@app/_components/item-navigation/item.navigation.component';
import { MessagelistComponent } from '@app/_components/messagelist/messagelist.component';
import { NoteComponent } from '@app/_components/note/note.component';
import { CustomersPage } from '@app/customer/customers.page';
import { CustomerGridComponent } from '@app/customer/customer.grid.component'
import { CustomerItemPage } from '@app/customer/customer.item.page';
import { CustomerEditPage } from '@app/customer/customer.edit.page';
import { NewCustomerForm } from '@app/customer/new.customer.form';
import { RecaptchaModule } from 'ng-recaptcha';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { AutofocusDirective } from '@app/_components/auto-focus.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChecklistComponent } from '@app/activity/components/checklist.component';
import { SubtasksComponent } from '@app/activity/components/subtasks/subtasks.component';
import { ProgressBarModule } from '@progress/kendo-angular-progressbar';
import { DeviceDetectorModule } from 'ngx-device-detector';

import { TimeReportPage } from '@app/time/time.report.page';
import { TimePage } from '@app/time/time.page';
import { TimeGridComponent } from '@app/time/time.grid.component';
import { ExcelExportModule } from '@progress/kendo-angular-excel-export';

import { SprintsPage } from '@app/sprints/sprints.page';
import { SprintsGridComponent } from '@app/sprints/sprints.grid.component';
import { SprintFormDialog } from '@app/sprints/sprint.form.dialog';

import { ReleasesPage } from '@app/releases/releases.page';
import { ReleaseItemPage } from '@app/releases/release.item.page';
import { ScrollableItemDirective } from '@app/_helpers/scrollable-item.directive';
import { MembersGridComponent } from '@app/members/members.grid.component';
import { MemberFormDialog } from '@app/members/member.form.component';
import { SpecPhoneDirective } from '@app/_helpers/specPhoneDirective';

import "froala-editor/js/plugins.pkgd.min.js";
import "froala-editor/js/third_party/font_awesome.min.js";
import "froala-editor/js/third_party/image_tui.min.js";
import "froala-editor/css/froala_editor.pkgd.min.css";
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/third_party/image_tui.min.css";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    OnlyNumber,
    LoginComponent,
    ForgotComponent,
    HomeComponent,
    NoContentComponent,  
    MainPage,
    ActivitiesPage,
    ActivityGridComponent,
    ActivityFormComponent,
    ActivityTopicComponent,
    BoardComponent,
    BoardTemplatesPage,
    BoardsPage,
    BoardItemPage,
    BoardTemplatesComponent,
    BoardTemplateFormComponent,
    BoardUserFormComponent,
    BoardTopicFormComponent,
    BoardTopicGridComponent,
    BoardSprintGridComponent,
    BoardTemplateItemPage,
    BoardSettingComponent,
    BoardUserGridComponent,
    MessageContentComponent,
    MultiCheckboxComponent,
    ItemNavigationComponent,
    NoteComponent,
    MessagelistComponent,
    CustomersPage,
    CustomerGridComponent,
    CustomerItemPage,
    CustomerEditPage,
    NewCustomerForm,
    AutofocusDirective,
    ChecklistComponent,
    SubtasksComponent,
    TimeReportPage,
    TimePage,
    TimeGridComponent,
    SprintsPage,
    SprintsGridComponent,
    SprintFormDialog,
    ReleasesPage,
    ReleaseItemPage,
    ScrollableItemDirective,
    MembersGridComponent,
    MemberFormDialog,
    SpecPhoneDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonsModule,
    BrowserAnimationsModule,
    DateInputsModule,
    DialogsModule,
    DropDownsModule,
    EditorModule,
    GridModule,
    ExcelModule,
    InputsModule,
    LabelModule,
    LayoutModule,
    MenuModule,
    NotificationModule,
    PDFExportModule,
    PopupModule,
    UploadModule,
    HttpClientModule,
    IntlModule,
    MatButtonModule, MatCheckboxModule, MatSlideToggleModule, MatIconModule, MatTooltipModule,
    DragDropModule,
    ResponsiveModule.forRoot(),
    RecaptchaModule,
    MatProgressBarModule,
    LightboxModule,
    FormsModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(), 
    ProgressBarModule,
    DeviceDetectorModule.forRoot(),
    ExcelExportModule,
  //  TranslateModule.forRoot()
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
      }
    }) 
  ],
  entryComponents: [
    MessageContentComponent,    
    ActivityFormComponent,
    BoardTemplateFormComponent,
    BoardUserFormComponent,
    BoardTopicFormComponent,
    ForgotComponent,
    NewCustomerForm,
    SprintFormDialog,
    MemberFormDialog,
  ],  
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },  
    { provide: HTTP_INTERCEPTORS, useClass: UploadInterceptor, multi: true }, 
 //   { provide: LOCALE_ID, useValue: 'he-IL'},
 //   { provide: RTL, useValue: true },   
    CommonService,
    CookieService,
    GlobalDataService,
    RoutingState  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
