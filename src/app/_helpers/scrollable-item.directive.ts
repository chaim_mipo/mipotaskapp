import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[appScrollableItem]'
})
export class ScrollableItemDirective {

  @Input('appScrollableItem') public name: string
  constructor(private el: ElementRef<HTMLElement>) {}

  public scrollIntoView() {
    this.el.nativeElement.scrollIntoView({ behavior: 'smooth'});
  }

}