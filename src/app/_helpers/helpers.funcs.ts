import { ElementRef } from '@angular/core';

export class HelpersFuncs {

  static getProductName() {
    return 'anglo';
  }

  static isDevVersion() {
    return 1;
  }

  static getProductDomain() {
    let str  = this.getProductName();
    if(this.isDevVersion()) {
      str += 'dev';
    } else {
      if(str == 'mivne') {
        str += 'prod';
      }
    }

    return str;
  }

  static getDomain() {
    return 'https://' + this.getProductDomain() + '.mipo.co.il/';
  }

  static copyMessage(val: string){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }   

  static getCdnDomain() {
    return 'https://anglocdn.mipo.co.il/';
  }

  static getUserKey() {
    let addStr = '';
    if(this.getProductName() != 'anglo') {
      addStr = this.getProductName();
    }

    return 'current' + addStr + 'RealtorUser';
  }  

  static getBaseDomainUrl() {
    return this.getDomain() + 'realtor';
  }

  static getClientVersion() {
    return '3.20';
  }

  static isApp() {
    let app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
    return app;  
  }

  static isMobile() {
    let ret = false;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      ret = true;
     }  
     return ret;  
  }

  static isCordova() {
  /*  let ret = false;
    if(window.hasOwnProperty("cordova")) {
      ret = true;
    }*/
	let ret = false;
	let app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
	if ( app ) {
		ret = true;
	}
	
    return ret;
  }

  static parseAddress( obj: Object ) {
    let ret = [];
    ret['address'] = '';
    ret['city'] = '';
    ret['street'] = '';
    ret['street_number'] = '';
    ret['neighborhood'] = '';
    ret['lng'] = 0;
    ret['lat'] = 0;

    if(obj && obj !== 'null' && obj !== 'undefined') {
      let address_components = obj['address_components'];
      let location = obj['geometry']['location'];

      ret['address'] = obj['formatted_address'];
      
      for(let i = 0; i < address_components.length; i++ ) {
        // console.log(address_components[i]);
        let types = address_components[i]['types']
        if(types.length) {
          if(types.indexOf("route") > -1) {
            ret['street'] = address_components[i]['long_name'];
          }
          
          if(types.indexOf("street_number") > -1) {
            ret['street_number'] = address_components[i]['long_name'];
          }    
          
          if(types.indexOf("neighborhood") > -1) {
            ret['neighborhood'] = address_components[i]['long_name'];
          }                        

          if(types.indexOf("locality") > -1) {
            ret['city'] = address_components[i]['long_name'];
          }                  
        }
        

      }
    
      ret['lat'] = location.lat();
      ret['lng'] = location.lng();    
    }

    return ret;
}

  static grepArray( elems: any[], callback: (obj: any) => boolean, inv: boolean ) {
      let ret = [];

      // Go through the array, only saving the items
      // that pass the validator function
      for ( let i = 0, length = elems.length; i < length; i++ ) {
          if ( !inv !== !callback( elems[ i ]) ) {
              ret.push( elems[ i ] );
          }
      }

      return ret;
  }

  static addServerErrors(errors, formErrorsMsgs, formControls) {
    for(var key in errors) {
        formErrorsMsgs[key] = '';
        for(let errorMsg of errors[key]) {
          formControls.controls[key].setErrors({ 'notUnique': true });
          formErrorsMsgs[key] += errorMsg;
      }
    }
  }


  static getFormValues(form, form_prefix, params) {
    let form_values = form.value;

    let url_str = Object.keys(form_values).map(function(key) {
      let tval: any = form_values[key];

      if (Object.prototype.toString.call( tval ) === '[object Array]' ) {
        tval = tval.join(',');
        if (tval == 'null' || tval == 'undefined' || tval == 'NaN') {
          tval = '';
        }
      } else {
        tval = encodeURIComponent(tval);
        if (tval == 'true') {
          tval = 1;
        } else if (tval == 'false') {
          tval = 0;
        } else if (tval == 'null' || tval == 'undefined' || tval == 'NaN') {
          tval = '';
        }
      }

      return form_prefix + '[' + encodeURIComponent(key) + ']' + '=' + tval;
    }).join('&');

    if (url_str) {
      url_str = '&' + url_str + params;
    }

    return url_str;
  }

  static getAccessToken() {
    let ret = null;
    let currentRealtorUser = localStorage.getItem(this.getUserKey());
    if (currentRealtorUser) {
      let currentRealtorUser_obj = JSON.parse(currentRealtorUser);
      if (currentRealtorUser_obj.hasOwnProperty('accessToken')) {
          ret = currentRealtorUser_obj.accessToken;
      }
    }

    return ret;
  }

  static getLocalUserObj() {
    let ret;
    let currentRealtorUser = localStorage.getItem(this.getUserKey());
    if (currentRealtorUser) {
      ret = JSON.parse(currentRealtorUser);
    }
    return ret;
  }

  static getLocalBranchId() {
    let ret = '';
    let currentRealtorBranchId = localStorage.getItem('currentRealtorBranchId');
    if (currentRealtorBranchId) {
      ret = currentRealtorBranchId;
    }
    return ret;
  }

  static setLocalBranchId(branch_id) {
    let ret = '';
    localStorage.setItem('currentRealtorBranchId', branch_id);
  }

  static fixDateFormat(date) {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    return day + '/' + month + '/' + year;
  }

  static getCurrentDate() {
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    return day + '_' + month + '_' + year + '_' + now.getHours() + '_' + now.getMinutes() + '_' + now.getSeconds();
  }

  static filterPage(grid_params, selected_filter, data, new_state?:boolean) {
    if(!new_state) { 
        grid_params.def_skip = 0;
      } 

      let params = {}    
      for (let key in data) {
        let current_param = data[key];
        if(current_param) {
          params[key] = current_param;
        }

        if(!new_state) {
          selected_filter[key] = current_param;            
        }
      }

      for (let key in grid_params) {
        let current_param = grid_params[key];
        if(key == 'def_sort') {
          if(current_param.length) {
              params['dir'] = current_param[0].dir;
              params['field'] = current_param[0].field;
          }
        } else {            
          if(current_param) {
            params[key] = current_param;
          }
        }
      }  
      
      return params;
}  
}
