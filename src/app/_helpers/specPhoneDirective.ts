import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[specPhoneOnly]'
})
export class SpecPhoneDirective {
    @Input() specPhoneOnly: boolean = true;
    constructor(private el: ElementRef, private control : NgControl) {
    }
 
  @HostListener('paste', ['$event'])
  onPaste(event: Event) {
    if(this.specPhoneOnly) {
        let pastedText = (<any>window).clipboardData && (<any>window).clipboardData.getData('Text') // If IE, use window
        || <ClipboardEvent>event && (<ClipboardEvent>event).clipboardData.getData('text/plain'); // Non-IE browsers
        
        if(pastedText.length >= 10) {
            pastedText = pastedText.replace(/-/g, '').replace('+972', '');//console.log(text);
            if(pastedText.substring(0,3) == '972') {
                pastedText = pastedText.replace('972', '');
            }

            if(pastedText.length == 9) {
                pastedText = '0' + pastedText;
            }

            event.preventDefault();
            this.control.control.setValue(pastedText);
        } 
    }     
  }

}