import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SprintsService } from '@app/_services/sprints.service';
import { IntlService } from '@progress/kendo-angular-intl';
import { DialogRef } from '@progress/kendo-angular-dialog';
import { Subscription } from 'rxjs';
import { sprintItem } from './sprint.model';

@Component({
  selector: 'sprint-form-dialog',
  templateUrl: 'sprint.form.dialog.html'
})

export class SprintFormDialog implements OnInit{
    @Input() public this_dialog: DialogRef;
    @Input() sprint_id : number = 0;
    @Input() save_url : string = 'sprint/save-sprint';
    private subscriptions: Subscription[] = [];
    public sprintFormSubmitted = false;
    public ready: boolean = false;
    public saving: boolean = false;    

  
    public sprintForm: FormGroup;
    public customers_list: {id: number, name: string}[] = [];
    public item: sprintItem = null;
    
    sprintFormErrors = {
        title : '',
        customer : ''
      };
            
    validationMessages = {        
        'title': {
            'required': 'שדה חובה'
        },
        'customer': {
            'required': 'שדה חובה'
        }
    };    

    constructor(
        private intl: IntlService, 
        private fb: FormBuilder, 
        private sprintsService: SprintsService) {
        }


    public ngOnInit() {        
        this.ready = true;       
        this.createForm();        
    }

    ngOnDestroy() {
        this.subscriptions.forEach((s) => s.unsubscribe());
    }      
 
    onValueChanged(data?: any) {
        if (!this.sprintForm) { return; }
        const form = this.sprintForm;
        for (const field in this.sprintFormErrors) {
          // clear previous error message (if any)
          this.sprintFormErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.sprintFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      }    

    closeDialog() {
        if(this.this_dialog) {
            this.this_dialog.close();
        }
    }

    onSprintFormSubmit() {
        this.sprintFormSubmitted = true;
        const formData = new FormData();
        
        if(this.sprint_id) {
            formData.append('id', String(this.sprint_id));
        }

        Object.keys(this.sprintForm.controls).forEach(key => {     
            if(key != 'myUpload') {   
                let val = this.sprintForm.controls[key].value;
                if(key == 'from_date') {
                    val = this.intl.formatDate(val, 'dd/MM/yyyy');
                } else if (Object.prototype.toString.call( val ) === '[object Array]' ) {
                    val = val.join(',');
                    if (val == 'null' || val == 'undefined' || val == 'NaN') {
                        val = '';
                    }
                }
                formData.append(key, val);
            }
        });
        this.sprintsService.saveItem(formData, this.save_url)
        .subscribe(
            () => {
                this.sprintFormSubmitted = false;
                this.this_dialog.close({action: 'closeAndRefresh'});
               
            },
            () => {
                this.sprintFormSubmitted = false
            }
        );

    }

    createForm() {
        this.sprintForm = this.fb.group({
            title: [this.item && this.item.title ? this.item.title : '', Validators.required ],                       
        });

        if(this.customers_list && this.customers_list.length) {
            this.sprintForm.addControl('cust_id', new FormControl(this.item && this.item.cust_id ? this.item.cust_id : '', Validators.required));
            this.sprintForm.controls['cust_id'].updateValueAndValidity();
        }
    
        this.subscriptions.push(this.sprintForm.valueChanges
        .subscribe(data => this.onValueChanged(data)));
  
        this.onValueChanged();    
      }
              
}