import { Component, OnInit, OnDestroy, Input, EventEmitter, Output, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { State } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { CategoriesService } from '@app/_services/northwind.service';
import { CommonService, GlobalDataService } from '@app/_services/index';
import { Subscription } from 'rxjs/Subscription';
import { sprintItem } from './sprint.model';

@Component({
    selector: 'sprints-grid',
    providers: [CategoriesService],
    templateUrl: './sprints.grid.component.html'
  })

  export class SprintsGridComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private navigationSubscription: Subscription; 
    @Input() controller: string = 'sprint/table';
    @Input() itemName: string = 'Sprint';
    @Input() def_take: number = 15;
    @Input() def_skip: number = 0;  
    @Input() def_sort = []; 
    @Input() CustomerId: number = null;   
    @Input() searchTerm: string = '';  
    @Input() reloadGrid: boolean = false;
    @Output()emitState: EventEmitter<any> = new EventEmitter<any>(); 
    @Output()emitAction: EventEmitter<any> = new EventEmitter<any>();       
    
    public loadingIndicator: boolean = true;
    public filters = {};
    public view: Observable<GridDataResult>;

    public state: State = {
        skip: this.def_skip,
        take: this.def_take,
        sort: this.def_sort
    };

    public take_list: number[] = [10, 15, 25, 50, 100, 150];    

    public constructor(    
        private service: CategoriesService, 
        private commonService: CommonService,
        public globalDataService: GlobalDataService
    ) {  }    


    public ngOnDestroy() {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }

        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();     
        }
    }


    public ngOnInit() {
                
        if(this.def_take > 150 ) {
            this.def_take = 50;
        }
        this.state = {
            skip: this.def_skip,
            take: this.def_take,
            sort: this.def_sort
        }  

        this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
            if (res.hasOwnProperty('action')) {
              if(res.action == 'loadStrints') {
                this.loadSprints();
              } else if (res.action == 'hideIndicator' + this.controller) {
                this.loadingIndicator = false;
              }
            }
          });  

        this.loadSprints();
    }

    ngOnChanges(changes: SimpleChanges) {//console.log(changes);
        if((changes["searchTerm"] && !changes["searchTerm"].isFirstChange()) || 
            (changes["CustomerId"] && !changes["CustomerId"].isFirstChange()) ||
            (changes["reloadGrid"] && !changes["reloadGrid"].isFirstChange())
        ) {
            this.state.skip = 0;
            this.loadSprints();
        }        

    }    
    
    public editSprint(item: sprintItem): void {
        this.emitAction.emit({action: 'edit' + this.itemName, item: item});
    }

    public deleteSprint(item: sprintItem): void {
        this.emitAction.emit({action: 'delete' + this.itemName, item: item});
    }

    loadSprints(): void {
        if(this.CustomerId) {
            this.filters['CustomerId'] = this.CustomerId;
        } else if(this.filters.hasOwnProperty('CustomerId')) {
            delete this.filters['CustomerId'];
        }

        this.filters['search_term'] = this.searchTerm;             
        this.page();      
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
 //       console.log(this.state);
        this.emitState.emit(this.state);
        this.loadingIndicator = true;
        this.service.query(state, this.controller, this.filters);
      }    

    page() {
        this.loadingIndicator = true;
        this.view = this.service;
        this.service.query(this.state, this.controller, this.filters);
      }    
     
  }