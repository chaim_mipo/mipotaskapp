import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { debounceTime, map, distinctUntilChanged, filter } from "rxjs/operators";
import { SprintsService } from '@app/_services';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { fromEvent, Subscription } from 'rxjs';
import { DialogService, DialogCloseResult, DialogRef } from '@progress/kendo-angular-dialog';
import { SprintFormDialog } from './sprint.form.dialog';
import { sprintItem } from './sprint.model';


@Component({
  selector: 'sprints-page',
  templateUrl: './sprints.page.html',
})
export class SprintsPage implements OnInit, OnDestroy {
    @ViewChild('sprintsFilterSearchTerm', {static: false, read: ElementRef}) sprintsFilterSearchTerm: ElementRef;  
    public controller: string = 'sprints';
    public search_placeholder = 'ID | Title';
    public selected_filter = {
      searchTerm: '',
      CustomerId: null
    };  

    public def_take: number = 50;
    public def_skip: number = 0;

    public grid_params = {
      def_take: this.def_take,
      def_skip: this.def_skip
    }

    public sprints_loaded: boolean = false;
    public reloadGrid: boolean = false;
    private subscriptions: Subscription[] = [];
  
    public customers_list: {id: number, name: string}[] = [];
    public sprintDialog: DialogRef;

    constructor(
        private router: Router, 
        private actRoute: ActivatedRoute, 
        private location: Location,
        private sprintsService: SprintsService,
        private dialogService: DialogService
      ) { }

      ngAfterViewInit() {    
        this.subscriptions.push(fromEvent(this.sprintsFilterSearchTerm.nativeElement, 'keyup').pipe(
            // get value
            map((event: any) => {
            return event.target.value;
            })
            // if character length greater then 2
            ,filter(res => res.length >= 0)
            // Time in milliseconds between key events
            ,debounceTime(450)        
            // If previous query is diffent from current   
            ,distinctUntilChanged()
            // subscription for response
            ).subscribe((text: string) => {

            this.selected_filter.searchTerm = text;
            this.changeFilter(true);
        }));    
    }   

    ngOnDestroy() {
      this.subscriptions.forEach((s) => s.unsubscribe());
      if(this.sprintDialog){
          this.sprintDialog.close();
      }
    }      
    
    ngOnInit() { 
      this.subscriptions.push(this.actRoute.params.subscribe(params => {

            for (let key in this.selected_filter) {               
                if (params.hasOwnProperty(key) && params[key]) {
                    let val = params[key];
                    if(key == 'CustomerId') {
                        val = parseInt(params[key]);
                    }
                    this.selected_filter[key] = val;
                }
            }          

            this.sprintsService.init().subscribe(
                (results: {customers?: {id: number, name: string}[]}) => {
                this.sprints_loaded = true;
                if(results.hasOwnProperty('customers')) {
                    this.customers_list = results.customers;
                }
            },
            () => {
                this.sprints_loaded = true;
            }
            
            );

        }));
    } 
    
    public changeCustomer(event: number): void {
        this.selected_filter.CustomerId = event;
        this.changeFilter();      
      }

    changeFilter(def_state?: boolean): void {

        if(def_state) { 
            this.grid_params.def_skip = 0;
        }
        const params = {}    
        for (let key in this.selected_filter) {
            let current_param = this.selected_filter[key];
            if(current_param) {
                params[key] = current_param;
            }
        }

        for (let key in this.grid_params) {
            let current_param = this.grid_params[key];
            if(current_param) {
                if(key == 'def_sort') {
                    if(current_param.length) {
                        params['dir'] = current_param[0].dir;
                        params['field'] = current_param[0].field;
                    }
                } else {              
                    params[key] = current_param;
                }
            }
        }        

        const new_url = this.router.serializeUrl(this.router.createUrlTree(['/' + this.controller, params]));          
        this.location.replaceState(new_url);    

      } 



    public onButtonAddSprintClick(): void {
        this.openSprintDialog('New Sprint');
    }

    public gridAction(event: {action: string, item: sprintItem}): void {
        if(event.action == 'editSprint') {
            this.openSprintDialog('Edit Sprint', event.item);
        } else if(event.action == 'deleteSprint') {
            this.openRemoveConfirm(event.item.id);
        }
    }

    private openRemoveConfirm(id: number): void {
        const dialog: DialogRef = this.dialogService.open({
            title: 'Remove Sprint',
            content: 'Are you sure?',
            actions: [                
                { text: 'OK', primary: true },
                { text: 'Cancel' }
            ],
            width: 320,
            minHeight: 230,
            minWidth: 250
          });

        this.subscriptions.push(dialog.result.subscribe((result) => {
            if (!(result instanceof DialogCloseResult)) {
                if(result['primary']) {
                 this.deleteSprint(id);
                }
            }
        }));          
    }    

    private deleteSprint(id: number): void {
        this.sprintsService.deleteItem(id).subscribe(
        () => {
            this.doRealodGrid();
        },
        () => {
            this.sprints_loaded = true;
        }
        
        );        
    }
    
    openSprintDialog(title: string, item?: sprintItem): void {
        this.sprintDialog = this.dialogService.open({
            title: title,
            width: 380,
            content: SprintFormDialog
        });
        
        const spContent = this.sprintDialog.content.instance;
        spContent.this_dialog = this.sprintDialog;
        spContent.customers_list = this.customers_list;

        if(item && item.id) {
            spContent.sprint_id = item.id;
            spContent.item = item;
        }
    

        this.subscriptions.push(this.sprintDialog.result.subscribe((result: any) => { 
        if (!(result instanceof DialogCloseResult)) {
            if(result.action == 'closeAndRefresh') {                    
                this.doRealodGrid();
            }
        }
        })); 
    }
      

    private doRealodGrid(): void {
      this.reloadGrid = !this.reloadGrid;
    }

    stateGrid(event: {skip: number, take: number}): void {
      this.grid_params.def_skip = event.skip;
      this.grid_params.def_take = event.take;
   //   this.changeFilter(false);
    }    
}