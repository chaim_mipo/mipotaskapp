import { Component, OnInit, Input, ViewChild, OnDestroy, OnChanges, Output, EventEmitter, forwardRef, HostListener, ViewChildren, ElementRef, QueryList  } from '@angular/core';
import { PopupModule, Align } from '@progress/kendo-angular-popup';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl, NgControl, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { ScrollableItemDirective } from '@app/_helpers/scrollable-item.directive';

@Component({ 
  selector: 'multi-checkbox',
  templateUrl: './multi.checkbox.component.html',
  styleUrls: ['./multi.checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiCheckboxComponent),
      multi: true
    }  
  ]
})
export class MultiCheckboxComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChildren(ScrollableItemDirective) scrollableItems: QueryList<ScrollableItemDirective>
  @ViewChild('anchor', {static: false}) public anchor: ElementRef;
  @ViewChild('popup', {static: false, read: ElementRef }) public popup: ElementRef;  
  @ViewChild('searchIn', {static: false, read: ElementRef }) public searchIn: ElementRef;
  @Input() options: {id: number, name: string, selected: boolean, hasBorder?: boolean}[] = [
    {id: 1, name: 'check 1', selected: false, hasBorder: false},
    {id: 2, name: 'check 2', selected: true, hasBorder: false},
    {id: 3, name: 'check 3', selected: false, hasBorder: false},
    {id: 4, name: 'check 4', selected: true, hasBorder: false}
  ];
  @Input('value') _value = '';
  @Output() valueChange = new EventEmitter<String>();
  @Input() defText = 'no selected';
  @Input() readonly = false;
  @Input() popupHeight = '220px';
  @Input() hideAllClear: boolean = false;
  @Input() withSearch: boolean = true;

  public toggleText: string = '';
  public show: boolean = false;
  private anchorAlign: Align = { horizontal: environment.dir == 'rtl' ? "right" : "left", vertical: "bottom" };
  private popupAlign: Align = { horizontal:  environment.dir == 'rtl' ? "right" : "left", vertical: "top" };

  onChange: any = () => { };
  onTouched: any = () => { };  

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
  }


  public constructor(){}



  checkAll(check) {
    if(this.options.length) {      
      this.options.forEach(function(obj) { 
         obj.selected = check;
      });

      this.buildText(0);
    }    
  }

  initSelected() {
    if(typeof this.value != 'undefined' && this.value !== 'undefined' && this.value != '' && this.options.length) {
        let splitted = String(this.value).split(",").map(Number);
        this.options.forEach(function(obj) { 
            if(splitted.indexOf(obj.id) > -1) {
                obj.selected = true;
            } else {
                obj.selected = false;
            }
        });
    } else if (this.options.length) {
      this.options.forEach(obj => {
        obj.selected = false;
      });
    }
  }

  public ngOnInit() {
 //   this.toggleText = this.defText;
  }

  ngOnChanges() {
    this.initSelected();
    this.buildText(1);
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
      if (event.keyCode === 27) {
          this.onToggle(false);
      }
  }

  @HostListener('document:click', ['$event'])
  public documentClick(event: any): void {
      if (!this.contains(event.target)) {
        this.onToggle(false);
      }
  }

  private contains(target: any): boolean {
    return this.anchor.nativeElement.contains(target) ||
        (this.popup ? this.popup.nativeElement.contains(target): false);
}  

  public onToggle(show?: boolean): void {
    this.show = show !== undefined ? show : !this.show;
    if(this.show) {
      this.searchFocus();
    }
  }

  public searchOption(searchTerm: string): void {
    this.options.forEach(opt => opt.hasBorder = false);
    if(searchTerm) {
      const findOption = this.scrollableItems.find((option) => option.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 );
      if(findOption) {
        const opt = this.options.find((option) => option.name.toLowerCase().indexOf(findOption.name.toLowerCase()) > -1 );
        if(opt) {
          opt.hasBorder = true;
        }
        findOption.scrollIntoView();
      }
    }
  }  

  private searchFocus(): void {
    setTimeout(() => {
      if(this.withSearch && this.searchIn) {
        this.searchIn.nativeElement.focus();
      }
    }, 60)
  }


registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) { 
    this.onTouched = fn;
  }

  writeValue(value) {//console.log(value);
    if (value) {
      this.value = value;
    } else {
      this.value = '';
      this.checkAll(false);
    }
    this.initSelected();
    this.buildText(1); 
  }


buildText(init) {
    let selected_count = 0;
    let text = '';
    let selected_val = '';
    if(this.options.length) {
        this.options.forEach(function(obj) { 
            obj.hasBorder = false;
            if(obj.selected) {
                selected_count++;
                if(text){
                    text += ', ';
                    selected_val += ','
                }
                text += obj.name;
                selected_val += obj.id;
            }
        });
        if(selected_count) {
            this.toggleText = text;
            this.value = selected_val;         
        }
    }
    if(!selected_count) {
        this.toggleText = this.defText;
        this.value = '';
    }

    if(!init){//console.log(this.value);
        this.valueChange.emit(this.value);
        this.onChange(this.value);
        this.onTouched();
    }
}


  autoSave(event, id) {
    let obj = this.options.find(
    row => row.id === id); 

    obj.selected = event.checked;    
    this.buildText(0);   

  }

  public ngOnDestroy() {

  }
 
}
