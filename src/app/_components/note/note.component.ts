import { HostListener, Component, OnInit, Input,  ElementRef,ViewChild, OnDestroy, OnChanges } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NoteService } from '@app/_services/note.service';
import { Subscription } from 'rxjs/Subscription';
import { CommonService } from '@app/_services/common.service';
import { GlobalDataService } from '@app/_services/globaldata.service';
import { SiteService } from '@app/_services/site.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { NotificationService } from '@progress/kendo-angular-notification';;

@Component({
  selector: 'note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css'],
  animations: [
     trigger('anim', [
       transition('void => active', [
         style({transform: 'translateY(-100%)'}),
         animate('0.15s ease-in-out', style({transform: 'translateY(0%)'}))
         /*style({transform: 'translateX(100%)'}),
         animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))*/

        ])
    ])
  ]
})
export class NoteComponent implements OnInit, OnDestroy, OnChanges{
  private subscription: Subscription;
  @ViewChild('someVar', {static: false}) el:ElementRef;
  @ViewChild('startNote', {static: false}) startNote:ElementRef;   
  @Input() mode = 'regular';
  @Input() specMode = '';
  @Input() moduleName;
  @Input() moduleId;
  @Input() messages;
  @Input() alert = '';
  note_type = 0;
  defCompTitle: string = 'History and updates';
  compTitle: string = this.defCompTitle;
  noteForm: FormGroup;
  noteMessageLoaded = false;
  noteFormSubmitted = false;
  showAnimation = false;
  noteFormErrors = {
    text: ''
  };
  public fixedPanel: boolean = false; 

  tmpModuleName: string = '';
  tmpModuleId: number = 0;

  validationMessages = {
    'text': {
      'required':      'Required field'
    }
  };

  public editorOptions: Object;
  public inlineEditorOptions: Object;
  initControls;
  constructor(private globalDataService: GlobalDataService, private _service: NotificationService, private _sanitizer:DomSanitizer, private siteService: SiteService, private fb: FormBuilder, private noteService: NoteService, private commonService: CommonService) {

    this.editorOptions = {
      language: 'he',      
      heightMin: 80,
      fileAllowedTypes: ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.ms-excel',
    'application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '.csv'],
      fileUploadURL: HelpersFuncs. getBaseDomainUrl() + '/site/upload-editor-file',
   //   imagePasteProcess: true,
      toolbarButtons: ['bold', 'italic', 'underline', '|', 'align', '|', 'insertLink', '|', 'insertImage', '|', 'insertFile'],
      pluginsEnabled: ['image', 'link', 'align', 'file'],
//      direction: 'rtl',
      placeholderText: 'הזן טקסט',
      charCounterCount: false,
      requestHeaders: {'Authorization' : 'Bearer ' + HelpersFuncs.getAccessToken()},   
      imageUploadURL: HelpersFuncs. getBaseDomainUrl() + '/site/upload-editor-image',    
      imageUploadParams: {
        id: 'my_editor'
      },
      imageMaxSize: 1024 * 1024,
      imageAllowedTypes: ['jpeg', 'jpg', 'png'],
      events : {
        'froalaEditor.image.beforeRemove' : function(e, editor, $img) {
       /*   siteService.deleteEditorImage($img.attr('src'))
            .subscribe(
                results => {     
                  
                  if(results.success) {

                  }
                }
            ); */

        },
        'froalaEditor.contentChanged': function (e, editor) {
        // console.log(editor.html.get());
        }        
      }       
    };    

  /*  this.inlineEditorOptions = JSON.parse(JSON.stringify( this.editorOptions ));
    this.inlineEditorOptions['toolbarInline'] = true;
    this.inlineEditorOptions['heightMin'] = 60;*/

    this.inlineEditorOptions = {
      language: 'he',      
      heightMin: 40,
      toolbarInline: true,
      fileAllowedTypes: ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.ms-excel',
    'application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '.csv'],
      fileUploadURL: HelpersFuncs. getBaseDomainUrl() + '/site/upload-editor-file',      
   //   imagePasteProcess: true,
      toolbarButtons: ['bold', 'italic', 'underline', '|', 'align', '|', 'insertLink', '|', 'insertImage', '|', 'insertFile'],
      pluginsEnabled: ['image', 'link', 'align', 'file'],
//      direction: 'rtl',
      placeholderText: 'הזן טקסט',
      charCounterCount: false,
      requestHeaders: {'Authorization' : 'Bearer ' + HelpersFuncs.getAccessToken()},   
      imageUploadURL: HelpersFuncs. getBaseDomainUrl() + '/site/upload-editor-image',    
      imageUploadParams: {
        id: 'my_editor'
      },
      imageMaxSize: 1024 * 1024,
      imageAllowedTypes: ['jpeg', 'jpg', 'png'],
      events : {
        'froalaEditor.image.beforeRemove' : function(e, editor, $img) {
        /*  siteService.deleteEditorImage($img.attr('src'))
            .subscribe(
                results => {     
                  
                  if(results.success) {

                  }
                }
            ); */

        },
        'froalaEditor.contentChanged': function (e, editor) {
        // console.log(editor.html.get());
        }        
      }       
    };     

  }

 /* @HostListener("window:scroll", [])
  onWindowScroll() { 
     let num = this.doc.body.scrollTop;
     console.log(num);
     if ( num > 50 ) {
         this.fixedPanel = true;
     }else if (this.fixedPanel && num < 5) {
         this.fixedPanel = false;
     }
  }*/

  @HostListener('window:scroll', ['$event'])
  handleScroll(){
      let start_scrl = 155;
      if(this.moduleName == 'Memo' || this.moduleName == 'Lead') {
        start_scrl = 130;
      }
      const windowScroll = window.pageYOffset;
      if(windowScroll >= start_scrl){
          this.fixedPanel = true;
      } else {
          this.fixedPanel = false;
      }
  }



  transform(html: string): SafeHtml {
    return this._sanitizer.bypassSecurityTrustHtml(html);
 }

  closeNotePanel() {
    this.commonService.notifyOther({action: 'close' + this.moduleName + 'NotePanel'});
  }

  toAddActivityDialog() {
    if(this.alert) {
        this._service.show({
          content: this.alert,
          animation: { type: 'fade', duration: 200 },
          position: { horizontal: 'center', vertical: 'bottom' },
          type: { style: 'error', icon: false },
          hideAfter: 2800
      });      
    } else {

      let setting = {
          from_module: 'log',
          model: this.moduleName,
          model_id: this.moduleId
      }

      this.commonService.notifyOther({action: 'newActivityDialog', afterSubmitAction: "addActivityToLog", def_type: "1", setting: setting});        
    }
  }

  editActivity(act_id) {
    let setting = {
      model: this.moduleName,
      model_id: this.moduleId,
      from_module: 'log'
    }    

    this.commonService.notifyOther({action: 'editActivityDialog', afterSubmitAction: 'updateActivityInLog', act_id: act_id, setting: setting});          
  }  


  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnChanges() {
    this.loadMessages();
    this.createForm();
  }
  public ngOnInit() {

    this.subscription = this.commonService.notifyObservable$.subscribe((res) => {
          if (res.hasOwnProperty('action')) {
            if (res.action == 'replaceNoteComponent') {
              this.tmpModuleId = res.item_id;
              this.tmpModuleName = res.model;
              this.compTitle = res.title;
              this.loadMessages();
            } else if (res.action == 'resetReplaceNoteComponent') {
              if (this.tmpModuleId && this.tmpModuleName) {
                this.closeTmpNotes();
              }
            } else if (res.action == 'addNoteToList' || res.action == 'addActivityToLog') {
              if(res.item) {
                this.addItemToList(res.item);
                this.startNote.nativeElement.scrollIntoView();
              }
            } else if(res.action == 'saveAndCloseActivityDialog') {
              this.loadMessages();
            } else if(res.action == 'reloadWall') {
              this.loadMessages();
            } else if(res.action == 'updateActivityInLog') {
              if(res.item && res.item['id']){
                var objIndex = this.messages.findIndex(
                  message => message.id == parseInt(res.item['id']));  
                if(objIndex !== -1){
                  this.messages[objIndex] = res.item;
                }

              } else {
                  this.loadMessages();
              }                
            }

          }
        });
  }

  readPdf(doc_id) {
    this.commonService.notifyOther({action: 'readPdf', doc_id: doc_id, name: 'cust_order_' + this.moduleId + '_'});
  }

  closeTmpNotes() {
    this.resetTmpStatus();
    this.loadMessages();
    this.commonService.notifyOther({action: 'deselectingPropertyCustomerGrid'});
  }

  setLogType(type) {
    this.note_type = type;
    this.loadMessages();
  }

  public initialize(initControls) {
    this.initControls = initControls;
    
  }

  loadMessages() {
    this.noteMessageLoaded = false;
    this.messages = [];

    var moduleName = this.moduleName;
    var moduleId = this.moduleId;

    if(this.tmpModuleId && this.tmpModuleName) {
      moduleName = this.tmpModuleName;
      moduleId = this.tmpModuleId;
    }

    if(moduleId) {
      this.noteService.getNotes(moduleName, moduleId, this.note_type)
          .subscribe(
              results => {
                  this.messages = results['items'];
                  
                  this.messages.forEach(function(obj) { obj.edit_mode = 0; obj.more_mode = 0; obj.loading = 0;});
  //console.log(this.messages);
                  this.noteMessageLoaded = true;
              }
      );
    } 
  }

  resetTmpStatus() {
      this.tmpModuleId = 0;
      this.tmpModuleName = '';
      this.compTitle = this.defCompTitle;
  }

  public isEditableItem(type, is_mine) {
    let ret = 0;
    if ((type == 0 || type == 21) && is_mine) {
      ret = 1;
    }
    return ret;
  }

  public openActivityDialog(act_id, is_mine, prop_id, mode) {
    if(act_id) {
      let act = is_mine ? 'editActivityDialog' : 'viewActivityDialog';
      this.commonService.notifyOther({action: act, act_id: act_id, prop_id: prop_id, mode: mode});
    }
  }

  public isRemovableItem(type, is_mine) {
    let ret = 0;
    if(/*this.globalDataService.current_user.is_zakiyan || this.globalDataService.current_user.is_manager ||*/ ((type == 0 || type == 21) && is_mine)) {
      ret = 1;
    }
    return ret;
  }  

  

  deleteNote(note) {
      const index: number = this.messages.indexOf(note);
      if (index !== -1) {
          this.messages.splice(index, 1);
      }        
  } 

  removeNote(id) {
    var obj = this.messages.find(
          message => message.id === id);  

    obj.loading = 1;
    this.noteService.removeNote(id, this.moduleName)
        .subscribe(
            results => {
              obj.loading = 0;
              if(results['success']) {
                this.deleteNote(obj);
              }
            }
    );
  }

  thumbtackNote(id) {
    var obj = this.messages.find(
          message => message.id === id);    
    obj.loading = 1;
    this.noteService.thumbtackNote(id)
        .subscribe(
            results => {
              obj.thumbtack = 1 - obj.thumbtack;
              obj.loading = 0;
            }
    );

  }

  editNote(id) {
    var obj = this.messages.find(
      message => message.id === id); 
    if (obj.edit_mode == 0) {   
      obj.edit_mode = 1; 
      obj.prev_text = obj.text;   
    } else {
      obj.edit_mode = 0;   
      obj.text = obj.prev_text;   
    }
  }

  moreNote(id) {
    var obj = this.messages.find(
      message => message.id === id); 

    obj.more_mode = !obj.more_mode;
  }  

  cancelEditNote(id) {
    var obj = this.messages.find(
      message => message.id === id); 
     obj.edit_mode = 0;
     obj.text = obj.prev_text; 
  }

  saveNote(id) {
    var obj = this.messages.find(
      message => message.id === id); 
    let text = obj.text.trim();

    if(text) {
      if(this.mode == 'regular') {
        text = text.replace(/(<([^>]+)>)/ig,"").replace(/(?:\r\n|\r|\n)/g, '<br />');
      }
      obj.edit_mode = 0;
      obj.loading = 1;

  //    text = encodeURIComponent(text);
      let params = {
        text: text,
        note_id: id,
        moduleName: this.moduleName
      }
      this.noteService.saveNote(params)
        .subscribe(
            results => {
                obj.loading = 0;
                if(results['success']) {
                }
            }
        );      
    }   
  }

  isPrivateMessage(type) {
    let ret = false;
    if(type == 21 || type == 34 || type == 35 || type == 36) {
      ret = true;
    }
    return ret;
  }

  br2nl(str) {
    return str.replace(/<br\s*\/?>/mg,"\n");
  }

  onNoteFormSubmit() {

    if(this.alert) {
      
      this._service.show({
        content: this.alert,
        animation: { type: 'fade', duration: 200 },
        position: { horizontal: 'center', vertical: 'bottom' },
        type: { style: 'error', icon: false },
        hideAfter: 2800
    });      


    } else {

      this.noteFormSubmitted = true;

      if(this.mode == 'regular') {

      let text = this.noteForm.value['text'].replace(/(<([^>]+)>)/ig,"").replace(/(?:\r\n|\r|\n)/g, '<br />');
      let url_str = 'text='+encodeURIComponent(text) + '&model=' + this.moduleName + '&model_id=' + this.moduleId;
      if (this.tmpModuleId && this.tmpModuleName) {
        url_str = 'text=' + encodeURIComponent(text) + '&model=' + this.tmpModuleName + '&model_id=' + this.tmpModuleId;
      }

      if  (this.noteForm.value['private']) {
        url_str += '&mode=private';
      }    

  //   console.log(url_str);

      this.noteService.newNote(url_str)
          .subscribe(
              results => {
                  if(results['success']) {
                    this.noteForm.controls['text'].setValue('');
                    this.noteForm.controls['private'].setValue('');
                    this.addItemToList(results['item']);
                    this.el.nativeElement.focus();
                  }
                  this.noteFormSubmitted = false;
              }
          );
      } else {
      //  this.noteFormSubmitted = true;
        let params = {
          text: this.noteForm.value['text'],
          model: this.moduleName,
          model_id: this.moduleId
        };

        if(this.specMode) {
          params['spec_mode'] = this.specMode;
        }


        this.noteService.newNote2(params)
        .subscribe(
            results => {
                if(results['success']) {
                  this.noteForm.controls['text'].setValue('');
                  this.addItemToList(results['item']);                
                }
                this.noteFormSubmitted = false;
            }
        );            
      }
    }
  }

  addItemToList(item) {
    if(!this.showAnimation) {
      this.showAnimation = true;
    }

    item['state'] = 'active';
    item['edit_mode'] = 0;
    item['more_mode'] = 0;
    item['loading'] = 0;      
    
    this.messages.unshift(item);
  }


  createForm() {
    this.noteForm = this.fb.group({
      text : ['', Validators.required ],
      private: ''
    });
  }

}