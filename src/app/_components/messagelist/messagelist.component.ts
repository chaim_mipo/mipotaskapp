import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { NoteService } from '@app/_services/note.service';
import { Router } from '@angular/router';
import { trigger, transition, style, animate } from '@angular/animations'

@Component({
  selector: 'messagelist',
  templateUrl: './messagelist.component.html',
  styleUrls: ['./messagelist.component.css'],
  animations: [
     trigger('anim', [
       transition('* => void', [
         //style({transform: 'translateX(0%)'}),
         //animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
         animate('0.5s ease-in-out', style({width: 0, height:0}))
        ])
    ])
  ]
})
export class MessagelistComponent implements OnInit, OnChanges{

  public messages = [];
  public noteMessagelistLoaded : boolean = false;

  constructor(private noteService: NoteService, private router: Router) {}

  public ngOnDestroy() {

  }

  ngOnChanges() {
    
  }

  public ngOnInit() {
    this.loadMessages();
    
  }

  removeMessage(id) {
     var obj = this.messages.find(
          message => message.id === id);  

    obj.loading = 1;
    this.noteService.removeMessage(id)
        .subscribe(
            results => {
              obj.loading = 0;
              this.deleteMsg(obj);
            }
    );
  }

  deleteMsg(msg) {
      const index: number = this.messages.indexOf(msg);
      if (index !== -1) {
          this.messages.splice(index, 1);
      }        
  }  

  thumbtackMessage(id) {
    var obj = this.messages.find(
          message => message.id === id);    
    obj.loading = 1;
    this.noteService.thumbtackMessages(id)
        .subscribe(
            results => {
              obj.thumbtack = 1 - obj.thumbtack;
              obj.loading = 0;
            }
    );

  }

  goToItem(id) {
    if(id) {
      this.router.navigate(['property/'+id]);
    }
  }


  loadMessages() {
    this.noteMessagelistLoaded = false;
  //  this.messages = [];

    this.noteService.getMessages()
        .subscribe(
            results => {
                this.messages = results['items'];
                this.noteMessagelistLoaded = true;
            }
    );
  }

}