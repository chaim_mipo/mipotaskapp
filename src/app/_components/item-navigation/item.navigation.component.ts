import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { HelpersFuncs } from '@app/_helpers/helpers.funcs';
import { CommonService } from '@app/_services/common.service';
import { Router } from '@angular/router';
import { RoutingState } from '@app/_models/routing.state'
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'item-navigation',
  templateUrl: './item.navigation.component.html'
  //,styleUrls: ['./status.bar.component.css']
})
export class ItemNavigationComponent implements OnInit, OnChanges {
  @Input() items = [];
  @Input() moduleName;
  @Input() currentItem;  
  @Input() hasTab = true;  
  nav_panel_active = false; 
  is_mobile = false; 
  next_tool = '';
  prev_tool = '';

 
  public nav_panel = false;

  public is_first_item = true;
  public is_last_item = true;
  public wrap_class = 'float_left ml20px mt15px';
  
  public constructor(private url: LocationStrategy, private routingState: RoutingState, private commonService: CommonService, private router: Router){}

  public ngOnInit() {
    
    if(HelpersFuncs.isMobile()) {
        this.wrap_class = '';
        this.is_mobile = true;
    }

    if(this.moduleName == 'properties') {
        this.next_tool = 'נכס קודם';
        this.prev_tool = 'נכס הבא';
    } else if(this.moduleName == 'customers') {
        this.next_tool = 'Prev';
        this.prev_tool = 'Next';        
    } else if(this.moduleName == 'contracts') {
        this.next_tool = 'חוזה קודם';
        this.prev_tool = 'חוזה הבא';
    }
  }

  goToItem(step) {
    let index = this.items.indexOf(parseInt(this.currentItem));
    let new_index = index + step;
    if(new_index < 0) {
      new_index = 0;
    } else if(new_index > this.items.length - 1) {
      new_index = this.items.length - 1;
    }

    let nextItem = this.items[new_index];

    let params = this.hasTab ? {goto:1, main_tab: 0} : {};

    this.router.navigate(['/' + this.moduleName +'/' + nextItem, params]);

  }

  ngOnChanges() {
    this.testNav();
  }


  testNav() {
    this.nav_panel_active = false;
    this.nav_panel = false;
    let prev_url = this.routingState.getPreviousUrl();
    let substring = '/' + this.moduleName + '';
    let substring2 = 'goto=1';
 //   console.log('this.currentItem = ' + this.currentItem); 
  //  console.log('prev_url = ' + prev_url); 
    if(prev_url && prev_url.length && (prev_url.indexOf(substring) !== -1 || this.url.path().indexOf(substring2) !== -1))  {
 //       console.log('TUT1 ');       
        if(this.items && this.items.indexOf(parseInt(this.currentItem)) != -1) {
            
            this.nav_panel = true;
            let len = this.items.length;

            if(this.items.length > 1) {
                // not first
                if(this.items[0] != this.currentItem) {
                    this.is_first_item = false;
                } else {
                    this.is_first_item = true;
                }

                if(this.items[len - 1] != this.currentItem) {
                    this.is_last_item = false;
                } else {
                    this.is_last_item = true;
                }
            }

        }

        setTimeout(() => {
            this.nav_panel_active = true;
        }, 600);
    }    

  }



}