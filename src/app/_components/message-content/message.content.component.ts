import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'message-content',
  providers: [],
  templateUrl: './message.content.component.html',
  styleUrls: ['./message.content.component.css']
})
export class MessageContentComponent implements OnInit {

  public parentDialog;
  public message;

  public constructor() {

  }

  public ngOnInit() {

  }

  close(){
    if (this.parentDialog) {
      this.parentDialog.close();
    }
  }

  

}