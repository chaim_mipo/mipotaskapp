import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { debounceTime, map, distinctUntilChanged, filter } from "rxjs/operators";
import { ReleasesService } from '@app/_services';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { fromEvent, Subscription } from 'rxjs';
import { DialogService, DialogCloseResult, DialogRef } from '@progress/kendo-angular-dialog';
import { SprintFormDialog } from '../sprints/sprint.form.dialog';
import { sprintItem } from '../sprints/sprint.model';


@Component({
  selector: 'releases-page',
  templateUrl: './releases.page.html',
})
export class ReleasesPage implements OnInit, OnDestroy {
    @ViewChild('releasesFilterSearchTerm', {static: false, read: ElementRef}) releasesFilterSearchTerm: ElementRef;  
    public controller: string = 'releases';
    public search_placeholder = 'ID | Title';
    public selected_filter = {
      searchTerm: '',
      CustomerId: null
    };  

    public def_take: number = 50;
    public def_skip: number = 0;

    public grid_params = {
      def_take: this.def_take,
      def_skip: this.def_skip
    }

    public releases_loaded: boolean = false;
    public reloadGrid: boolean = false;
    private subscriptions: Subscription[] = [];
  
    public customers_list: {id: number, name: string}[] = [];
    public releaseDialog: DialogRef;
    private save_url: string = 'release/save-release';

    constructor(
        private router: Router, 
        private actRoute: ActivatedRoute, 
        private location: Location,
        private releasesService: ReleasesService,
        private dialogService: DialogService
      ) { }

      ngAfterViewInit() {    
        this.subscriptions.push(fromEvent(this.releasesFilterSearchTerm.nativeElement, 'keyup').pipe(
            // get value
            map((event: any) => {
            return event.target.value;
            })
            // if character length greater then 2
            ,filter(res => res.length >= 0)
            // Time in milliseconds between key events
            ,debounceTime(450)        
            // If previous query is diffent from current   
            ,distinctUntilChanged()
            // subscription for response
            ).subscribe((text: string) => {

            this.selected_filter.searchTerm = text;
            this.changeFilter(true);
        }));    
    }   

    ngOnDestroy() {
      this.subscriptions.forEach((s) => s.unsubscribe());
      if(this.releaseDialog){
          this.releaseDialog.close();
      }
    }      
    
    ngOnInit() { 
      this.subscriptions.push(this.actRoute.params.subscribe(params => {

            for (let key in this.selected_filter) {               
                if (params.hasOwnProperty(key) && params[key]) {
                    let val = params[key];
                    if(key == 'CustomerId') {
                        val = parseInt(params[key]);
                    }
                    this.selected_filter[key] = val;
                }
            }  

            this.releasesService.init().subscribe(
                (results: {customers?: {id: number, name: string}[]}) => {
                this.releases_loaded = true;
                if(results.hasOwnProperty('customers')) {
                    this.customers_list = results.customers;
                }
            },
            () => {
                this.releases_loaded = true;
            }
            
            );

        }));
    } 
    
    public changeCustomer(event: number): void {
        this.selected_filter.CustomerId = event;
        this.changeFilter();      
      }

    changeFilter(def_state?: boolean): void {

        if(def_state) { 
            this.grid_params.def_skip = 0;
        }
        const params = {}    
        for (let key in this.selected_filter) {
            let current_param = this.selected_filter[key];
            if(current_param) {
                params[key] = current_param;
            }
        }

        for (let key in this.grid_params) {
            let current_param = this.grid_params[key];
            if(current_param) {
                if(key == 'def_sort') {
                    if(current_param.length) {
                        params['dir'] = current_param[0].dir;
                        params['field'] = current_param[0].field;
                    }
                } else {              
                    params[key] = current_param;
                }
            }
        }        

        const new_url = this.router.serializeUrl(this.router.createUrlTree(['/' + this.controller, params]));          
        this.location.replaceState(new_url);    

      } 



    public onButtonAddReleaseClick(): void {
        this.openReleaseDialog('New Release');
    }

    public gridAction(event: {action: string, item: sprintItem}): void {
        if(event.action == 'editRelease') {
            this.openReleaseDialog('Edit Release', event.item);
        } else if(event.action == 'deleteRelease') {
            this.openRemoveConfirm(event.item.id);
        }
    }

    private openRemoveConfirm(id: number): void {
        const dialog: DialogRef = this.dialogService.open({
            title: 'Remove Release',
            content: 'Are you sure?',
            actions: [                
                { text: 'OK', primary: true },
                { text: 'Cancel' }
            ],
            width: 320,
            minHeight: 230,
            minWidth: 250
          });

        this.subscriptions.push(dialog.result.subscribe((result) => {
            if (!(result instanceof DialogCloseResult)) {
                if(result['primary']) {
                 this.deleteRelease(id);
                }
            }
        }));          
    }    

    private deleteRelease(id: number): void {
        this.releasesService.deleteItem(id).subscribe(
        () => {
            this.doRealodGrid();
        },
        () => {
            this.releases_loaded = true;
        }
        
        );        
    }
    
    openReleaseDialog(title: string, item?: sprintItem): void {
        this.releaseDialog = this.dialogService.open({
            title: title,
            width: 380,
            content: SprintFormDialog
        });
        
        const spContent = this.releaseDialog.content.instance;
        spContent.this_dialog = this.releaseDialog;
        spContent.customers_list = this.customers_list;
        spContent.save_url = this.save_url;

        if(item && item.id) {
            spContent.sprint_id = item.id;
            spContent.item = item;
        }
    

        this.subscriptions.push(this.releaseDialog.result.subscribe((result: any) => { 
        if (!(result instanceof DialogCloseResult)) {
            if(result.action == 'closeAndRefresh') {                    
                this.doRealodGrid();
            }
        }
        })); 
    }
      

    private doRealodGrid(): void {
      this.reloadGrid = !this.reloadGrid;
    }

    stateGrid(event: {skip: number, take: number}): void {
      this.grid_params.def_skip = event.skip;
      this.grid_params.def_take = event.take;
   //   this.changeFilter(false);
    }    
}