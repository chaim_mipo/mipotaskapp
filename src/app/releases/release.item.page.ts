import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ReleasesService, CommonService } from '@app/_services';

@Component({
  selector: 'release-item-page',
  templateUrl: './release.item.page.html'
})
export class ReleaseItemPage implements OnInit, OnDestroy {
    private navigationSubscription: Subscription; 
    private controller: string = 'releases';
    public release_id: number = 0;
    public page_title: string = '';
    public customer: string = '';
    public releaseItemLoaded: boolean = false;

    constructor( 
        private commonService: CommonService,
        private releasesService: ReleasesService,
        private router: Router, private actRoute: ActivatedRoute, private location: Location) { }

    ngOnDestroy() {
        if(this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    } 

    ngOnInit() {
        this.navigationSubscription = this.actRoute.paramMap.subscribe(params => {
            const url_params = params['params'];
            if(params.get('id')) {
                const id = params.get('id');
                if(/^\d+$/.test(String(id)) && parseInt(id) > 0) {
                    this.release_id = parseInt(id);
                    this.initPage(this.release_id); 
                } else {
                  this.router.navigate([this.controller]);
             }

            } else {
                this.router.navigate([this.controller]);
            }         

        });
    }

    private initPage(rid: number): void {

        this.releasesService.getItem(rid).subscribe(
            (res: {title: string, customer?: string}) => {
                this.releaseItemLoaded = true;
                this.page_title = res.title;
                if(res.customer) {
                    this.customer = res.customer;
                }

                this.commonService.notifyOther({action: 'setTitle', title: 'Release ' + this.page_title});
            },
            () => {
                this.releaseItemLoaded = true;
            }
        );

    }

}