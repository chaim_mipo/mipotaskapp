import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class NoteService {

  constructor(private http: HttpClient) { }

  public newNote2(params) {
    let url = environment.apiUrl + '/site/new-note/';
    return this.http.post(url, params);            
  }

  public newNote(params) {
    return this.http.get(environment.apiUrl + '/site/new-note/?' + params);
  }

  public saveNote(params) {
    let url = environment.apiUrl + '/site/save-note/';
    return this.http.post(url, params);       

  }

  public getNotes(model: string, item_id: number, type: number) {
    return this.http.get(environment.apiUrl + '/site/notes/?model=' + model + '&item_id=' + item_id + '&type=' + type);
  }

  public thumbtackMessages(msg_id: number) {
    return this.http.get(environment.apiUrl + '/site/set-thumbtack/?msg_id=' + msg_id);
  } 

  public getMessages() {    
    return this.http.get(environment.apiUrl + '/site/messages/');
  }

  public thumbtackNote(note_id: number) {
    return this.http.get(environment.apiUrl + '/site/set-note-thumbtack/?note_id=' + note_id);
  }  

  public removeNote(note_id: number, moduleName) {
    return this.http.get(environment.apiUrl + '/site/remove-note/?note_id=' + note_id + '&moduleName=' + moduleName);
  } 

  public removeMessage(msg_id: number) {
    return this.http.get(environment.apiUrl + '/site/remove-message/?msg_id=' + msg_id);
  }    

}
