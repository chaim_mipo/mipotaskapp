import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class ReleasesService {
    constructor(private http: HttpClient) { }

    init(): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/release/init`, {});
    }

    public saveItem(data: {}): Observable<{}> {
        return this.http.post(`${environment.apiUrl}/release/save-release`, data);
    }  
    
    deleteItem(id: number): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/release/delete-item?id=${id}`, {});
    }    

    getItem(id: number): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/release/get-item?id=${id}`, {});
    }   
}