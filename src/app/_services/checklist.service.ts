import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class ChecklistService {
    constructor(private http: HttpClient) { }


    public autoSave(parent_id: number, id: number, field:any, value: any) {
        return this.http.put(`${environment.apiUrl}/checklist/autosave`, {parent_id: parent_id, id: id, field: field, value: value});
    }

    public saveItem(params: any) {
        return this.http.post(`${environment.apiUrl}/checklist/save-item`, params);
    }         

    public removeItem(parent_id: number, id: number) {
        return this.http.delete(`${environment.apiUrl}/checklist/remove-item?id=` + id + `&parent_id=` + parent_id, {});
    }  
  
  
    public getItems(parent_id: number) {
        return this.http.get(`${environment.apiUrl}/checklist/get-items/?parent_id=` + parent_id, {});
    }     
  
}