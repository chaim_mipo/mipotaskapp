import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class TimeService {
    constructor(private http: HttpClient) { }


    public getPageSetting() {
        return this.http.get(`${environment.apiUrl}/time/table-setting`, {});
    }   

    public removeTimeItem(id: number) {
        return this.http.get(`${environment.apiUrl}/time/remove-item?id=` + id, {});
    }       
    
    public saveTimeItem(params: any) {
        return this.http.post(`${environment.apiUrl}/time/save-time`, params);
    }  
    
    public getStat(filters: any) {
        return this.http.get(`${environment.apiUrl}/time/table?` + filters, {});
    }  
  
}