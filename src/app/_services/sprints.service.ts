import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class SprintsService {
    constructor(private http: HttpClient) { }

    init(): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/sprint/init`, {});
    }

    public saveItem(data: {}, url: string): Observable<{}> {        
        return this.http.post(`${environment.apiUrl}/${url}`, data);
    }  
    
    deleteItem(id: number): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/sprint/delete-item?id=${id}`, {});
    }    
   
}