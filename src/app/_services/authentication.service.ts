import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(environment.userKey)));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string, cpt?) {

        var params = {mobile: username, password: password};

        if(cpt !== undefined && cpt != '') {
          params['cpt'] = cpt;
        }        

        return this.http.post<any>(`${environment.apiUrl}/site/login`, params)
            .pipe(map(result => { 
                // login successful if there's a jwt token in the response
                if (result['data'] && result['data']['success']) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    let data = result['data'];                    
                    localStorage.setItem(environment.userKey, JSON.stringify(result['data']));
                    this.currentUserSubject.next(result['data']);                    
                } 

                return result['data'];
            }));
    }

    forgot(username: string) {

        var params = {mobile: username};

        return this.http.post<any>(`${environment.apiUrl}/site/forgot`, params)
            .pipe(map(result => { 
                return result['data'];
            }));
    }    

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem(environment.userKey);
        this.currentUserSubject.next(null);
    }
}