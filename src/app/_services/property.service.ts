import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class PropertyService {
    constructor(private http: HttpClient) { }

    public toggleFav(prop_id: number) {
        return this.http.get(environment.apiUrl + '/property/property-toggle-fav/?prop_id=' + prop_id, {});
    } 

    public toggleMarketParam(prop_id, key) {
        return this.http.get(environment.apiUrl + '/marketplace/save-param/?prop_id=' + prop_id + '&key=' + key, {});
    }     

    public toggleMarket(prop_id) {
        return this.http.get(environment.apiUrl + '/marketplace/toggle/?prop_id=' + prop_id, {});
    }      

    public getPropertyGridSettings(mode, prop_id?) {
        let add_params = '';
        if(prop_id) {
          add_params = '&prop_id=' + prop_id;
        }
        return this.http.get(environment.apiUrl + '/property/property-grid-settings/?mode=' + mode + add_params);
    }

    public getPropertyDetails(id) {
        return this.http.get(`${environment.apiUrl}/property/property-item/?id=` + id, {});
    }

    public autoSave(item_id: number, model: string, params: string) {
        return this.http.get(environment.apiUrl + '/site/autosave/?item_id='+item_id+'&model='+model+'&'+params, {});
    }

    public removeAllPropertyCustomerRows(prop_id, cust_id) {
        return this.http.get(environment.apiUrl + '/property-customer/remove-all-property-customer/?prop_id='+prop_id+'&cust_id='+cust_id, {});
    }   
    
    public getPropertyCustomerToggleDelete(item_id: number, prop_id: number) {
        return this.http.get(environment.apiUrl + '/property-customer/toggle-delete/?item_id='+item_id+'&prop_id='+prop_id, {});
    }

    public changePropertyCustomerStatus(id: number, status: number, prop_id: number) {
        return this.http.get(environment.apiUrl + '/property-customer/change-status/?id='+id+'&status='+status+'&prop_id='+prop_id, {});
    }    

    public getPropertyCustomerSetting() {
        return this.http.get(environment.apiUrl + '/property-customer/settings/', {});
    }    

    public togglePropertyCustomerFav(item_id) {
        return this.http.get(environment.apiUrl + '/property/property-customer-toggle-fav/?item_id=' + item_id, {});
    }    
    
    public getPropertyCustomerDetails(id, mode?) {
        let str = mode ? '&mode=' + mode : '';
        return this.http.get(environment.apiUrl + '/property-customer/property-customer-details/?id=' + id + str, {});
    }    
    
    public addCustomerToProperty(cust_id: number, prop_id: number) {
        return this.http.get(environment.apiUrl + '/property-customer/add-customer-property/?cust_id='+cust_id+'&prop_id='+prop_id, {});
    }    
    
}