import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class LeadService {
    constructor(private http: HttpClient) { }

    initLeads() {
        return this.http.post(`${environment.apiUrl}/lead/init-leads`, {});
    }

}