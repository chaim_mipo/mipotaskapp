import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class UpdatesService {
    constructor(private http: HttpClient) { }

    newNote(params) {
        return this.http.post(`${environment.apiUrl}/updates/new-note`, params);
    }

    getNotes(model, model_id) {
        return this.http.post(`${environment.apiUrl}/updates/get-notes`, {model: model, model_id: model_id});
    }

    removeNote(id, model) {
        return this.http.post(`${environment.apiUrl}/updates/remove-note`, {id: id, model: model});
    }    

    thumbtackNote(id) {
        return this.http.post(`${environment.apiUrl}/updates/set-thumbtack`, {id: id});
    }

    saveNote(params) {
        return this.http.post(`${environment.apiUrl}/updates/save-note`, params);
    }    
    
}