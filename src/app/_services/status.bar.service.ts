import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class StatusBarService {
  constructor(private http: HttpClient) { }

  public changeStatus(model: string, item_id: number, field: string, status: number) {
    return this.http.get(environment.apiUrl + '/site/change-status/?model='+model+'&item_id='+item_id+'&field='+field+'&status='+status);
  }

  public getStatus(model: string, item_id: number, field: string) {
    return this.http.get(environment.apiUrl + '/site/get-status/?model='+model+'&item_id='+item_id+'&field='+field);
  }

}
