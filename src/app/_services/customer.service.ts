import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class CustomerService {
    constructor(private http: HttpClient) { }
   
    public getCustomerFormSettings(cust_id: number) {
        return this.http.get(`${environment.apiUrl}/customer/customer-form-settings/?cust_id=`+cust_id, {});
    }    

    public searchByTerm(term, mode): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/customer/search-by-term/?term=` + term + `&mode=` + mode, {});
    }   

    public saveItem(data: {}): Observable<{}> {
        return this.http.post(`${environment.apiUrl}/customer/save-customer`, data);
    }    

    public getCustomerDetails(id): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/customer/customer-item/?cust_id=` + id, {});
    }

    public toggleFav(cust_id): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/customer/customer-toggle-fav/?cust_id=` + cust_id, {});
    }    
    
    public getGridSettings(): Observable<{}> {
        return this.http.get(`${environment.apiUrl}/customer/customer-grid-settings/`, {});
    }   
    
    saveMember(params: {}): Observable<{}>  {
        return this.http.post(`${environment.apiUrl}/member/save-member`, params);
    }    

    getMember(id: number): Observable<{}>  {
        return this.http.get(`${environment.apiUrl}/member/get-item?id=${id}`);
    }

    resetMemberPassword(params: {}): Observable<{}>  {
        return this.http.post(`${environment.apiUrl}/member/reset-member-password`, params);
    }  
    
    removeMember(id: number): Observable<{}>  {
        return this.http.get(`${environment.apiUrl}/member/delete-item?id=${id}`);
    }    
}