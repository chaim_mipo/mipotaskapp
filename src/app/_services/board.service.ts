import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class BoardService {
    constructor(private http: HttpClient) { }

    getBoard(params: any) {
        return this.http.post(`${environment.apiUrl}/board/get-board`, params);
    }

    changeActivityPosStatus(params: any) {
        return this.http.post(`${environment.apiUrl}/board/change-activity-pos-status`, params);
    }

    createBoardTemplate(params: any) {
        return this.http.post(`${environment.apiUrl}/board/create-board-template`, params);
    }    

    getBoardTemplates(params: any) {
        return this.http.post(`${environment.apiUrl}/board/get-board-templates`, params);
    }

    copyBoard(params: any) {
        return this.http.post(`${environment.apiUrl}/board/copy-board`, params);
    }   
    
    getBoardUserInit(board_id: number) {
        return this.http.get(`${environment.apiUrl}/board/board-user-init/?board_id=` + board_id, {});
    }   
    
    createBoardUser(params: any) {
        return this.http.post(`${environment.apiUrl}/board/create-board-user`, params);
    }   
    
    deleteBoardUser(board_id: number, id: number) {
        return this.http.get(`${environment.apiUrl}/board/delete-board-user/?board_id=` + board_id + '&id=' + id, {});
    }  

    toggleBoardUserUpdate(board_id: number, id: number, value: number) {
        return this.http.get(`${environment.apiUrl}/board/toggle-board-user-update/?board_id=` + board_id + '&id=' + id + '&value=' + value, {});
    }      
    
    getBoardSetting(board_id: number) {
        return this.http.get(`${environment.apiUrl}/board/get-board-setting/?board_id=` + board_id, {});
    }    

    saveBoardStatusName(board_id: number, id: any, name: string) {
        return this.http.get(`${environment.apiUrl}/board/save-board-status/?board_id=` + board_id + '&id=' + id + '&name=' + name, {});
    }     

    removeBoardStatus(board_id: number, id: number) {
        return this.http.get(`${environment.apiUrl}/board/remove-board-status/?board_id=` + board_id + '&id=' + id, {});
    }     
    
    saveBoardSetting(board_id: number, field: string, value: string) {
        return this.http.get(`${environment.apiUrl}/board/save-board-setting/?board_id=` + board_id + '&field=' + field + '&value=' + value, {});
    }   
    
    public toggleFav(board_id: number) {
        return this.http.get(environment.apiUrl + '/board/board-toggle-fav/?board_id=' + board_id, {});
    }  
    
    createBoardTopic(params: any) {
        return this.http.post(`${environment.apiUrl}/board/create-board-topic`, params);
    }   

    createBoardAttr(params: {}, attr: string) {
        return this.http.post(`${environment.apiUrl}/board/create-board-${attr}`, params);
    }     
    
    deleteBoardTopic(board_id: number, id: number) {
        return this.http.get(`${environment.apiUrl}/board/delete-board-topic/?board_id=` + board_id + '&id=' + id, {});
    }   
    
    createBoardSprint(params: any) {
        return this.http.post(`${environment.apiUrl}/board/create-board-sprint`, params);
    }   
    
    deleteBoardSprint(board_id: number, id: number) {
        return this.http.get(`${environment.apiUrl}/board/delete-board-sprint/?board_id=` + board_id + '&id=' + id, {});
    }      
}