import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class ContractService {
    constructor(private http: HttpClient) { }

    initContracts() {
        return this.http.get(`${environment.apiUrl}/contract/init-contracts`, {});
    }


    getContractDetails(id) {
        return this.http.get(`${environment.apiUrl}/contract/contract-item/?contract_id=` + id, {});
    }
}