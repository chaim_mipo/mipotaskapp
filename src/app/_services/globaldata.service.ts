import { Injectable } from '@angular/core';


@Injectable()
export class GlobalDataService {
    public prop_cities: String = '';
    public prop_hoods: String = '';  
    public cancel_prop_cities: boolean = false;
    public current_user;
    public visited_properties = [];
    public last_visited_property = 0;
    public last_visited_customer = 0; 
    public last_visited_contract = 0; 
    public last_visited_pc_property = '';   
    public last_visited_pc_customer = '';   
    public last_visited_new_worker = '';   
    public press_back = false;  
    public grid_prop_ids = [];  
    public grid_cust_ids = []; 
    public grid_contract_ids = [];
    public productName = '';
    public productType = 'web';   
    public property_customer_all = []; //switch all
    public customer_property_all = [];
    public home_clever_exclusive_mode: boolean = true;
    public home_activity_selected_tab = 1;
    public home_activity_selected_tab_wp_1 = 1;
    public home_activity_selected_tab_wp_2 = 1;
    public home_activity_orderby = '';
    public home_activity_orderby_dir = '';
    public activity_mode_view = 'table'; //table | calendar
    public activity_calendar_selected_tab = 0;
    public app_not_active_time = 0;
    public active_timestamp = Math.floor(Date.now() / 1000);  
    public activity_page_date_selected_filter = 0;
    getActivityAfterSubmitAction(type) {
        let act = '';
        let t = String(type);
        if(t == '1') {
            act = 'loadTasks';
        } else if(t == '2' || t == '3' || t == '2,3') {
            act = 'loadCallsAndMeetings';
        } else {
            act = 'loadActivities';
        }

        return act;
    }
}