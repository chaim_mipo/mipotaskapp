import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class ActivityService {
    constructor(private http: HttpClient) { }

    getItem(params) {
        return this.http.post(`${environment.apiUrl}/activity/get-item`, params);
    }

    saveItem(data) {
        return this.http.post(`${environment.apiUrl}/activity/save-item`, data);
    }

    tableSetting() {
        return this.http.post(`${environment.apiUrl}/activity/table-setting`, {});
    }   
    
    public autoSave(act_id: number, params, settings) {
        return this.http.post(`${environment.apiUrl}/activity/autosave`, {act_id: act_id, params: params, settings: settings});
    }

    public addComment(act_id: number, text, tags) {
        return this.http.post(`${environment.apiUrl}/activity/add-comment`, {act_id: act_id, text: text, tags: tags});
    }         

    public saveComment(params) {
        return this.http.post(`${environment.apiUrl}/activity/save-comment`, params);
    }

    public removeComment(params) {
        return this.http.post(`${environment.apiUrl}/activity/remove-comment`, params);
    }  
    
    public getHistory(params) {
        return this.http.post(`${environment.apiUrl}/activity/get-activity-log`, params);
    }   
    
    public getComboList(params:any) {
        return this.http.get(`${environment.apiUrl}/activity/get-combo-list/?` + params, {});
    }   
    
    public getIdUrl(url: string, board_id: number) {
        return this.http.get(`${environment.apiUrl}/activity/get-id-url/?board_id=` + board_id + `&url=` + url, {});
    }  

    public removeEditorImage(params) {
        return this.http.post(`${environment.apiUrl}/editor/delete-editor-image`, params);
    }  

    public getSubtasks(id: number) {
        return this.http.get(`${environment.apiUrl}/activity/get-subtasks/?id=${id}`, {});
    }      
}