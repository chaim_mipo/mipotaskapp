import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class SiteService {
    constructor(private http: HttpClient) { }

    public getCurrentUser() {
        return this.http.get(`${environment.apiUrl}/site/current-user`, {});
    }

    public getServerInit(counter) {
        return this.http.get(`${environment.apiUrl}/site/server-init/?counter=` + counter, {});
    }  

    public getSwitchBranch(branch_id) {
        return this.http.get(`${environment.apiUrl}/site/switch-branch/?branch_id=` + branch_id, {});
    }     

    public globalSearch(term: string, mode?) {
        let params = 'short';
        if(mode !== undefined && mode != '') {
          params = mode;
        }
        return this.http.get(`${environment.apiUrl}/site/global-search/?term=` + term + '&mode=' + params, {});
      }    


    public autoSave(item_id: number, model: string, params: string) {
        return this.http.get(`${environment.apiUrl}/site/autosave/?item_id=`+item_id+'&model='+model+'&'+params, {});
    }   
    
    public deleteEditorImage(img) {
        let params = {img : img};
        let url = environment.apiUrl + '/site/delete-editor-image/';
        return this.http.post(url, params);            
      }    
}