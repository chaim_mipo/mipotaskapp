import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { toODataString } from '@progress/kendo-data-query';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map } from 'rxjs/operators/map';
import { tap } from 'rxjs/operators/tap';
import { CommonService, GlobalDataService } from '@app/_services';
import { environment } from '@environments/environment';
import { errors } from '@telerik/kendo-intl';
import { catchError } from 'rxjs/operators'
import { throwError } from 'rxjs';

export abstract class NorthwindService extends BehaviorSubject<GridDataResult> {
    public loading: boolean;

    constructor(
        private http: HttpClient,
        private tableName: string, 
        private commonService: CommonService,
        private globalDataService: GlobalDataService
    ) {
        super(null);
    }

    handleError(error: HttpErrorResponse){
        this.loading = false;
        return throwError(error);
       }    

    public query(state: any, table: string, filter: any): void {
        this.fetch(table, state, filter)
            .subscribe(x => super.next(x));
    }

    public addFavAttr(table, data) {
        if(table == 'Students') {
            data.forEach(function(obj) { obj.fav = 0;});
        }
        return data;
    }

    protected fetch(tableName: string, state: any, filter: any): Observable<GridDataResult> {

        if(tableName == 'activity/table' || tableName == 'lead/table' || tableName == 'time/table' || tableName == 'sprint/table' || tableName == 'release/table' || tableName == 'member/table') {

            this.loading = true;

            return this.http
                .post(`${environment.apiUrl}/${tableName}`, {state: state, filters: filter})
                .pipe(
                    map(response => (<GridDataResult>{
                        data: response['data'].rows,
                        total: parseInt(response['data'].count, 10)
                    })),
                    tap(
                        () => {
                            this.loading = false;
                            this.commonService.notifyOther({action: 'hideIndicator' + tableName});
                        },
                        () => {
                            this.loading = false;
                            this.commonService.notifyOther({action: 'hideIndicator' + tableName});
                        }
                    ),
                    catchError(this.handleError)
                    
                );
        } else {
            this.loading = true;

             let queryStr = `${toODataString(state)}&$count=true`;
          /*  let queryStr = '';

            if(state.sort && state.sort[0] && state.sort[0]['dir'] && state.sort[0]['field']) {
                let sort_dir = state.sort[0]['dir'] == 'desc' ? 'desc' : 'asc';
                let sort = '$orderby=' + state.sort[0]['field'] + ' ' + sort_dir;                
                queryStr += sort;
            }  */          

            let filter_length = Object.keys(filter).length,
                    filter_string = '';
    
            if (filter_length) {
              for (var key in filter) {
                  filter_string += '&' + key + '=' + filter[key];
              }
            }
    
            if  (filter_string) {
              queryStr += filter_string;
            }            


            let y = this.http
            .get(`${environment.apiUrl}/${tableName}?` + queryStr)
            .pipe(
                map(response => (
                    
                    {
                        data: response['data'].rows,
                        total: parseInt(response['data'].count, 10),
                        all_count: response['data']['all_count'],
                        all_not_deleted_count: response['data']['all_not_deleted_count'],
                        all_status_items: response['data']['all_status_items'],
                        mode: response['data']['mode']
                    }
                )),
                tap(
                    response => {
                        this.loading = false;
                        this.commonService.notifyOther({action: 'hideIndicator' + tableName});
                        if(tableName == 'customer/table' || tableName == 'property/table' || tableName == 'contract/table') {
                            let arr = [];
                            if(response['data'].length) {
                                response['data'].forEach( (element) => {
                                    arr.push(parseInt(element.id));
                                });
                            }

                            if(tableName == 'customer/table') {
                                this.globalDataService.grid_cust_ids = arr;
                            } else if(tableName == 'property/table') {
                                this.globalDataService.grid_prop_ids = arr;
                            } else if(tableName == 'contract/table' && response['mode'] == 'regular') {
                                this.globalDataService.grid_contract_ids = arr;
                            //  console.log(this.globalDataService.grid_contract_ids);
                            }
                        } else if(tableName == 'property-customer/table') {
                            // console.log(response);
                            this.commonService.notifyOther({action: 'propertyCustomerGlobalStat', all_count: response.all_count, all_not_deleted_count: response.all_not_deleted_count, all_status_items: response.all_status_items});

                        }

                        
                    },
                    () => {
                        this.loading = false;
                        this.commonService.notifyOther({action: 'hideIndicator' + tableName});                        
                    }
                )
            );



            return y;
        }

    }    
}


@Injectable()
export class CategoriesService extends NorthwindService {
    constructor(http: HttpClient, commonService: CommonService, globalDataService: GlobalDataService) { super(http, 'Categories', commonService, globalDataService); }
}
