export const environment = {
  production: true,
  name: 'MIPOTASK',
  apiUrl: 'https://tasks.mipo.co.il/app',
  domain: 'https://tasks.mipo.co.il/',
  appDomain: 'https://tasksapp.mipo.co.il/',
  uploadDir: 'uploads/activity_files/',
  userKey: 'currentMTAUser',
  version: '0.88',
  lang: 'en',
  dir: 'ltr'
};
