export const environment = {
    production: false,
    name: 'STASKS',
    apiUrl: 'https://stasksdev.mipo.co.il/app',
    domain: 'https://stasksdev.mipo.co.il/',
    appDomain: 'https://stasks.mipo.co.il/',
    uploadDir: 'uploads/stov/activity_files/',
    userKey: 'currentSTAUser',
    version: '0.88',
    lang: 'he', // en | he
    dir: 'rtl' //rtl | ltr
  };
  

  