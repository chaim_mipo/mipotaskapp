export const environment = {
    production: true,
    name: 'UTITASKS',
    apiUrl: 'https://utitasksdev.mipo.co.il/app',
    domain: 'https://utitasksdev.mipo.co.il/',
    appDomain: 'https://tasksuti.mipo.co.il/',
    uploadDir: 'uploads/uti/activity_files/',
    userKey: 'currentUTITAUser',
    version: '0.88',
    lang: 'he', // en | he
    dir: 'rtl' //rtl | ltr
  };