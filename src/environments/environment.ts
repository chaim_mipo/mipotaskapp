// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: '0.88',  
 /* name: 'STASKS',
  apiUrl: 'https://stasksdev.mipo.co.il/app',
  domain: 'https://stasksdev.mipo.co.il/',
  appDomain: 'https://stasks.mipo.co.il/',
  uploadDir: 'uploads/stov/activity_files/',
  userKey: 'currentSTAUser',
  lang: 'he', // en | he
  dir: 'rtl' //rtl | ltr*/
/*
  name: 'UTITASKS',
  apiUrl: 'https://utitasksdev.mipo.co.il/app',
  domain: 'https://utitasksdev.mipo.co.il/',
  appDomain: 'https://tasksuti.mipo.co.il/',
  uploadDir: 'uploads/uti/activity_files/',
  userKey: 'currentUTITAUser',
  lang: 'he', // en | he
  dir: 'rtl' //rtl | ltr*/


  name: 'MIPOTASK',
  apiUrl: 'https://tasks.mipo.co.il/app',
  domain: 'https://tasks.mipo.co.il/',
  appDomain: 'https://tasksapp.mipo.co.il/',
  uploadDir: 'uploads/activity_files/',
  userKey: 'currentMTAUser',
  lang: 'en', // en | he
  dir: 'ltr' //rtl | ltr    
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
